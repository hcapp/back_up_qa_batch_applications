package com.rssfeed.dao;

import java.util.ArrayList;

import com.rssfeed.exception.BatchProcessingCategoriesException;
import com.rssfeed.process.Category;

public interface CategoriesDao {

	public String insertImage(String categoryName, String categoryImagePath,
			String hubcitiId) throws BatchProcessingCategoriesException;

	public String categoryPorting() throws BatchProcessingCategoriesException;

	public void truncateImages() throws BatchProcessingCategoriesException;

	public ArrayList<Category> getHubCitiId()
			throws BatchProcessingCategoriesException;

}
