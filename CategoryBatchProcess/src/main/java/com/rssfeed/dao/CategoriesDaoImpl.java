package com.rssfeed.dao;

import java.util.ArrayList;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.rssfeed.process.ApplicationConstants;
import com.rssfeed.process.Category;
import com.rssfeed.process.CommonConstants;

import com.rssfeed.exception.BatchProcessingCategoriesException;

public class CategoriesDaoImpl implements CategoriesDao {

	private static Logger LOG = LoggerFactory.getLogger(CategoriesDaoImpl.class
			.getName());

	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcCall simpleJdbcCall;

	public void setDataSource(DataSource dataSource) {
		// this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public String insertImage(String categoryName, String categoryImagePath,
			String hubcitiId) throws BatchProcessingCategoriesException {
		LOG.info("Inside DaoImpl : insertImage ");

		int row = 0;
		String strResponse = null;

		String query = "INSERT INTO RssFeedCategoryStagingTable(CategoryName, ImagePath,HcHubCitiID) VALUES(?,?,?)";

		try {

			row = jdbcTemplate.update(query, new Object[] { categoryName,
					categoryImagePath, hubcitiId });

			if (row == 1) {
				strResponse = ApplicationConstants.SUCCESSTEXT;
				LOG.info("Category Inserted successfully" + "News type :"
						+ categoryName + "HubCitiID :" + hubcitiId);
			} else {
				strResponse = ApplicationConstants.FAILURETEXT;
				LOG.info("Category Insertion failed " + "News type :"
						+ categoryName + "HubCitiID :" + hubcitiId);

			}

		} catch (DataAccessException exception) {
			strResponse = ApplicationConstants.FAILURETEXT;
			LOG.info("Inside FeedsCategoryDAOImpl : Category Insertion Failed : "
					+ exception);
			throw new BatchProcessingCategoriesException(exception);
		}

		LOG.info("Exit DaoImpl : insertImage ");
		return strResponse;
	}

	public String categoryPorting() throws BatchProcessingCategoriesException {
		final String methodName = "CategoryPorting";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Boolean result;
		String response = null;
		String hubCitiId = null;
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WebRssFeedCategoryInsertion");
			MapSqlParameterSource feed = new MapSqlParameterSource();

			final Map<String, Object> resultFromProcedure = simpleJdbcCall
					.execute(feed);
			hubCitiId = (String) resultFromProcedure.get("HcHubCitiID");

			result = (Boolean) resultFromProcedure.get("Status");
			if (null != hubCitiId) {
				response = ApplicationConstants.SUCCESSTEXT;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

				LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + errorNum+ "errorMsg.." + errorMsg);
				response = ApplicationConstants.FAILURETEXT;
			}
		} catch (Exception exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName+ exception);
			response = ApplicationConstants.FAILURETEXT;
			throw new BatchProcessingCategoriesException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public void truncateImages() throws BatchProcessingCategoriesException {
		LOG.info("Inside DaoImpl : truncateImages ");
		final String sql = "TRUNCATE TABLE RssFeedCategoryStagingTable";

		try {

			jdbcTemplate.execute(sql);

		} catch (Exception e) {
			LOG.error("Inside DaoImpl : truncateImages : exception "
					+ e.getMessage());
			throw new BatchProcessingCategoriesException(e);
		}

		LOG.info("Exit CategoriesDaoImpl : truncateImages ");
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Category> getHubCitiId()
			throws BatchProcessingCategoriesException {
		final String methodName = "getHubCitiID";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<Category> hubCitiList = null;
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WebRssFeedHubCitiList");
			MapSqlParameterSource feed = new MapSqlParameterSource();
			simpleJdbcCall.returningResultSet("hubcitilst",
					new BeanPropertyRowMapper<Category>(Category.class));
			final Map<String, Object> resultFromProcedure = simpleJdbcCall
					.execute(feed);

			final Integer errorNum = (Integer) resultFromProcedure
					.get(CommonConstants.ERRORNUMBER);
			final String errorMsg = (String) resultFromProcedure
					.get(CommonConstants.ERRORMESSAGE);

			if (null != resultFromProcedure) {

				if (null == errorNum) {

					hubCitiList = (ArrayList<Category>) resultFromProcedure
							.get("hubcitilst");

				} else {
					LOG.info("Inside  : getHubCitiId : " + errorNum
							+ "errorMsg  .." + errorMsg);
				}
			}

		} catch (DataAccessException e) {

			LOG.error("Inside JdbcCustomerDAO : fetchAllFeeds : "
					+ e.getMessage());
			throw new BatchProcessingCategoriesException(e);

		}

		LOG.info("Exit getHubCitiId : BatchProcessingCategories ");
		return hubCitiList;

	}
}
