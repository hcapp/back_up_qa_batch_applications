package com.rssfeed.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONArray;

import org.json.JSONObject;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.rssfeed.exception.BatchProcessingCategoriesException;
import com.rssfeed.service.CategoriesService;
import com.rssfeed.service.CategoriesServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utility {

	/**
	 * Logger instance.
	 */
	private static Logger LOG = LoggerFactory
			.getLogger(CategoriesServiceImpl.class.getName());

	@SuppressWarnings("unused")
	public static void getRockwallValues(String hubCitiId)
			throws BatchProcessingCategoriesException {

		LOG.info("Inside Utility : getRockwallValues"
				+ CommonConstants.METHODSTART);
		final ApplicationContext context = new ClassPathXmlApplicationContext(
				"categories-service.xml");
		final CategoriesServiceImpl categoriesService = (CategoriesServiceImpl) context
				.getBean("categoriesService");

		try {
			String[] strNews = { "good times", "good cause", "good living",
					"good culture", "good people", "good pets", "good faith",
					"good thinking", "good business", "good sports",
					"good neighbours", "columnists" };

			String response = null;
			String portingResponse;

			for (int i = 0; i < strNews.length; i++) {
				LOG.info("START OF THE FEEDS BATCH PROCESS :->"
						+ Calendar.getInstance().getTime());
				LOG.info("========================================================================================");
				LOG.info("TYLER :START OF THE FEEDS BATCH PROCESS :->"
						+ Calendar.getInstance().getTime());
				LOG.info("Inside UTILITY GETTYLERVALUES", strNews[i]);
				Utility.getList(strNews[i], hubCitiId);

				LOG.info("END OF THE FEEDS BATCH PROCESS :->"
						+ Calendar.getInstance().getTime());
				LOG.info("========================================================================================");
				LOG.info("TYLER :END OF THE FEEDS BATCH PROCESS :->"
						+ Calendar.getInstance().getTime());

			}
		} catch (Exception e) {
			LOG.error("Inside Utility : getRockwallValues" + e.getMessage());
		}
		LOG.info("Inside Utility : getRockwallValues"
				+ CommonConstants.METHODEND);
	}

	@SuppressWarnings("unused")
	public static void getTylerValues(String hubcitiId)
			throws BatchProcessingCategoriesException {

		LOG.info("Inside Utility : getTylerValues"
				+ CommonConstants.METHODSTART);

		final ApplicationContext context = new ClassPathXmlApplicationContext(
				"categories-service.xml");
		final CategoriesServiceImpl categoriesService = (CategoriesServiceImpl) context
				.getBean("categoriesService");

		String response = null;
		String portingResponse;

		try {

			String[] strNew = { "sports", "health", "business", "top", "life",
					"opinion", "entertainment", "food", "all", "weather",
					"videos", "photos" };

			for (int i = 0; i < strNew.length; i++) {
				LOG.info("START OF THE FEEDS BATCH PROCESS :->"
						+ Calendar.getInstance().getTime());
				LOG.info("========================================================================================");
				LOG.info("TYLER :START OF THE FEEDS BATCH PROCESS :->"
						+ Calendar.getInstance().getTime());
				LOG.info("Inside UTILITY GETTYLERVALUES", strNew[i]);
				Utility.getList(strNew[i], hubcitiId);

				LOG.info("END OF THE FEEDS BATCH PROCESS :->"
						+ Calendar.getInstance().getTime());
				LOG.info("========================================================================================");
				LOG.info("TYLER :END OF THE FEEDS BATCH PROCESS :->"
						+ Calendar.getInstance().getTime());

			}
		} catch (Exception e) {
			LOG.error("Inside Utility : getTylerValues" + e.getMessage());
		}

		LOG.info(CommonConstants.METHODEND + "Inside Utility : getTylerValues");
	}

	public static JSONObject readJsonFromUrl(String url) {

		JSONObject json = null;
		InputStream is = null;

		try {
			is = new URL(url).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,
					Charset.forName("UTF-8")));
			String jsonText = readAll(rd);

			try {
				json = new JSONObject(jsonText);
			} catch (Exception e) {
				LOG.error("Inside Utility : readJsonFromUrl :", e.getMessage());
			}

		} catch (MalformedURLException e) {
			LOG.error("Inside Utility : readJsonFromUrl :", e.getMessage());
		} catch (IOException ex) {
			LOG.error("Inside Utility : readJsonFromUrl  : " + ex.getMessage());

		}

		finally {
			try {
				is.close();
			} catch (IOException e) {
				LOG.error("Inside Utility : readJsonFromUrl:", e.getMessage());
			}
		}
		return json;
	}

	private static String readAll(Reader rd) {
		StringBuilder sb = new StringBuilder();
		int cp;
		try {
			while ((cp = rd.read()) != -1) {
				sb.append((char) cp);
			}
		} catch (IOException e) {
			LOG.error("Inside Utility : readAll:", e.getMessage());
		}
		return sb.toString();
	}

	public static void getVideoList(String name, String hubCitiID) {
		LOG.info("Inside Utility : getVideoList");

		List<Category> items = new ArrayList<Category>();
		final ApplicationContext context = new ClassPathXmlApplicationContext(
				"categories-service.xml");
		final CategoriesServiceImpl categoriesService = (CategoriesServiceImpl) context
				.getBean("categoriesService");

		// URL url = null;

		String image = null;

		try {
			if (name.equalsIgnoreCase("videos")) {

				JSONObject json = readJsonFromUrl(CommonConstants.VIDEOS_NEWS_URL);

				JSONArray array = json.getJSONArray("items");

				for (int i = 0; i < array.length(); i++) {

					if (array.getJSONObject(i).getString("videoStillURL") != null) {
						image = array.getJSONObject(i).getString(
								"videoStillURL");

						break;

					}

				}
				Category item = new Category(name, image);
				items.add(item);
			}

			try {
				categoriesService.processCategoriesDatabaseOperation(items,
						hubCitiID);
			} catch (Exception ex) {
				LOG.error("Inside Utility : getVideoList : " + ex.getMessage());
			}

		} catch (Exception e) {
			LOG.error("Inside Utility : getVideoList : " + e.getMessage());

			Category item = new Category(name, null);
			items.add(item);

			try {
				categoriesService.processCategoriesDatabaseOperation(items,
						hubCitiID);
			} catch (Exception ex) {
				LOG.error("Inside Utility : getVideoList : " + ex.getMessage());
			}

		}

		LOG.info("Exit Utility : getVideoList ");

	}

	public static void getList(String name, String hubcitiId) {
		LOG.info("Inside Utility : getList" + CommonConstants.METHODSTART);

		List<Category> items = new ArrayList<Category>();

		final ApplicationContext context = new ClassPathXmlApplicationContext(
				"categories-service.xml");
		final CategoriesServiceImpl categoriesService = (CategoriesServiceImpl) context
				.getBean("categoriesService");

		URL url = null;
		String image = null;

		try {

			/*
			 * switch (name) { case "sports": url = new
			 * URL(CommonConstants.SPORTS_NEWS_URL); break;
			 * 
			 * case "health": url = new URL(CommonConstants.HEALTH_NEWS_URL);
			 * break;
			 * 
			 * case "business": url = new
			 * URL(CommonConstants.BUSINESS_NEWS_URL); break;
			 * 
			 * case "top": url = new URL(CommonConstants.TOP_NEWS_URL); break;
			 * 
			 * case "life": url = new URL(CommonConstants.LIFE_NEWS_URL); break;
			 * 
			 * case "opinion": url = new
			 * URL(CommonConstants.EDITORIAL_NEWS_URL); break;
			 * 
			 * case "entertainment": url = new
			 * URL(CommonConstants.ENTERTAINMENT_NEWS_URL); break;
			 * 
			 * case "food": url = new URL(CommonConstants.FOOD_NEWS_URL); break;
			 * 
			 * case "weather": url = new
			 * URL(CommonConstants.WEATHERTYLER_NEWS_URL); break;
			 * 
			 * case "all": url = new URL(CommonConstants.ALLARTICLES_NEWS_URL);
			 * break;
			 * 
			 * case "videos":
			 * getVideoList("videos",CommonConstants.VIDEOS_NEWS_URL,hubcitiId);
			 * break;
			 * 
			 * case "photos":
			 * PhotosAPI.readJsonFromUrl(CommonConstants.SMUGMUG_PHOTOS_URL,
			 * "photos", hubcitiId); break;
			 * 
			 * 
			 * 
			 * case "good times": url = new
			 * URL(CommonConstants.GOODEVENTS_NEWS_URL); break;
			 * 
			 * case "good cause": url = new
			 * URL(CommonConstants.GOODCAUSE_NEWS_URL); break;
			 * 
			 * case "good living": url = new
			 * URL(CommonConstants.GOODLIVING_NEWS_URL); break;
			 * 
			 * case "good culture": url = new
			 * URL(CommonConstants.GOODCULTURE_NEWS_URL); break;
			 * 
			 * case "good people": url = new
			 * URL(CommonConstants.GOODPEOPLE_NEWS_URL); break;
			 * 
			 * case "good pets": url = new
			 * URL(CommonConstants.GOODPETS_NEWS_URL); break;
			 * 
			 * case "good faith": url = new
			 * URL(CommonConstants.GOODFAITH_NEWS_URL); break;
			 * 
			 * case "good thinking": url = new
			 * URL(CommonConstants.GOODTHINKING_NEWS_URL); break;
			 * 
			 * case "good business": url = new
			 * URL(CommonConstants.GOODBUSINESS_NEWS_URL); break;
			 * 
			 * case "good neighbours": url = new
			 * URL(CommonConstants.GOODNEIGHBOURS_NEWS_URL); break;
			 * 
			 * case "good health": url = new
			 * URL(CommonConstants.GOODNEIGHBOURS_NEWS_URL); break;
			 * 
			 * case "good sports": url = new
			 * URL(CommonConstants.GOODSPORTS_NEWS_URL); break;
			 * 
			 * case "columnists": url = new
			 * URL(CommonConstants.GUESTCOLUMNS_NEWS_URL); break;
			 * 
			 * default: url=null; }
			 */

			if (name.equalsIgnoreCase("sports")) {
				url = new URL(CommonConstants.SPORTS_NEWS_URL);

			}

			else if (name.equalsIgnoreCase("health")) {
				url = new URL(CommonConstants.HEALTH_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("business")) {
				url = new URL(CommonConstants.BUSINESS_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("top")) {
				url = new URL(CommonConstants.TOP_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("life")) {
				url = new URL(CommonConstants.LIFE_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("opinion")) {
				url = new URL(CommonConstants.EDITORIAL_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("photos")) {

				PhotosAPI.readJsonFromUrl(CommonConstants.SMUGMUG_PHOTOS_URL,
						name, hubcitiId);
			}

			else if (name.equalsIgnoreCase("entertainment")) {
				url = new URL(CommonConstants.ENTERTAINMENT_NEWS_URL);
			} else if (name.equalsIgnoreCase("food")) {
				url = new URL(CommonConstants.FOOD_NEWS_URL);
			} else if (name.equalsIgnoreCase("weather")) {
				url = new URL(CommonConstants.WEATHERTYLER_NEWS_URL);
			} else if (name.equalsIgnoreCase("all")) {
				url = new URL(CommonConstants.ALLARTICLES_NEWS_URL);
			} else if (name.equalsIgnoreCase("videos")) {

				getVideoList(name, hubcitiId);
			}

			// adding new changes

			else if (name.equalsIgnoreCase("good times")) {
				url = new URL(CommonConstants.GOODEVENTS_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good cause")) {
				url = new URL(CommonConstants.GOODCAUSE_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good living")) {
				url = new URL(CommonConstants.GOODLIVING_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good culture")) {
				url = new URL(CommonConstants.GOODCULTURE_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good people")) {
				url = new URL(CommonConstants.GOODPEOPLE_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good pets")) {
				url = new URL(CommonConstants.GOODPETS_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good faith")) {
				url = new URL(CommonConstants.GOODFAITH_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good thinking")) {
				url = new URL(CommonConstants.GOODTHINKING_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good business")) {
				url = new URL(CommonConstants.GOODBUSINESS_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good sports")) {
				url = new URL(CommonConstants.GOODSPORTS_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good neighbours")) {
				url = new URL(CommonConstants.GOODNEIGHBOURS_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("columnists")) {
				url = new URL(CommonConstants.GUESTCOLUMNS_NEWS_URL);
			}

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setCoalescing(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			if (url.openStream().available() > 0) {
				Document doc = db.parse(new InputSource(url.openStream()));
				doc.getDocumentElement().normalize();

				NodeList nodeList = doc.getElementsByTagName("item");

				for (int i = 0; i < nodeList.getLength(); i++) {

					Node node = nodeList.item(i);
					Element fstElmnt = (Element) node;
					try {

						if (fstElmnt.getElementsByTagName("media:content") != null) {

							NodeList media = fstElmnt
									.getElementsByTagName("media:content");
							if (media.item(0) != null
									&& media.item(0).getAttributes() != null
									&& media.item(0).getAttributes()
											.getNamedItem("url") != null) {
								String mediaurl = media.item(0).getAttributes()
										.getNamedItem("url").getNodeValue();

								image = mediaurl;

							}

						}

						Category item = new Category(name, image);
						items.add(item);
						System.out.println("category" + name + image);

					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			}
			categoriesService.processCategoriesDatabaseOperation(items,
					hubcitiId);
		} catch (Exception e) {

			LOG.info("Inside Utility : getList : exception : " + e.getMessage());
			Category item = new Category(name, null, e.getMessage());
			items.add(item);
			try {
				categoriesService.processCategoriesDatabaseOperation(items,
						hubcitiId);
			} catch (Exception e1) {
				LOG.error("Inside Utility : getList  : " + e1.getMessage());
			}

		}

	}

}
