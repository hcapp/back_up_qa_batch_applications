package com.rssfeed.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.rssfeed.dao.CategoriesDao;
import com.rssfeed.exception.BatchProcessingCategoriesException;
import com.rssfeed.process.ApplicationConstants;
import com.rssfeed.process.Category;
import com.rssfeed.process.PropertiesReader;
import com.rssfeed.process.Utility;

public class CategoriesServiceImpl implements CategoriesService{
	
	private static Logger LOG = LoggerFactory
			.getLogger(CategoriesServiceImpl.class.getName());
	
	
	
	private CategoriesDao categoriesDao; 
	
	

	public void setCategoriesDao(CategoriesDao categoriesDao) {
		this.categoriesDao = categoriesDao;
	}

	//ApplicationContext context=new ClassPathXmlApplicationContext("database-config.xml");
	//CategoriesDaoImpl categoriesDao= (CategoriesDaoImpl) context.getBean("customerDAO");

	public void processTruncateOperation()throws BatchProcessingCategoriesException {
		String strMethodName = "processTruncateOperation";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		try {
				categoriesDao.truncateImages();

		} catch (BatchProcessingCategoriesException e) {
			LOG.error("Inside RssFeedServiceImpl : processDeletionOperation : "
					+ e.getMessage());
			throw new BatchProcessingCategoriesException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
	}
	
	public void getHubCitiId()throws BatchProcessingCategoriesException {
		String strMethodName = "getHubCitiId";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		ArrayList<Category> hubCitiList = null;
		
		
		

		try {

			
			hubCitiList = categoriesDao.getHubCitiId();
			
		    if(hubCitiList != null && !hubCitiList.isEmpty()){
			for (int i = 0; i < hubCitiList.size(); i++) {

				if (PropertiesReader.getPropertyValue("tyler_hubcitiName")
						.equals(hubCitiList.get(i).getHubCitiName())) {
					LOG.info("HubCitiId Tyler"
							+ hubCitiList.get(i).getHcHubCitiID());
					LOG.info("TYLER : START OF THE BATCH PROCESS FOR  CATEGORIES :->"
							+ Calendar.getInstance().getTime());
				
						
					
					Utility.getTylerValues(hubCitiList.get(i).getHcHubCitiID());
					
					
				
					LOG.info("TYLER :END OF THE BATCH PROCESS FOR TYLER CATEGORIES :->"
							+ Calendar.getInstance().getTime());
					LOG.info("========================================================================================");
				}

				else if (PropertiesReader.getPropertyValue(
						"rockwall_hubcitiName").equals(
						hubCitiList.get(i).getHubCitiName())) {
					LOG.info("HubCitiId Rockwall"
							+ hubCitiList.get(i).getHcHubCitiID());
					LOG.info("ROCKWALL :START OF THE BATCH PROCESS FOR ROCKWALL CATEGORIES :->"
							+ Calendar.getInstance().getTime());
					
					Utility.getRockwallValues(hubCitiList.get(i).getHcHubCitiID());
				
					

					LOG.info("ROCKWALL : END OF THE BATCH PROCESS FOR ROCKWALL CATEGORIES :->"
							+ Calendar.getInstance().getTime());
				}
			}
			}

				

			
		} catch (BatchProcessingCategoriesException e) {
			LOG.error("Inside RssFeedServiceImpl : getHubCitiId : "
					+ e.getMessage());
			throw new BatchProcessingCategoriesException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		
		
		
	}

	public void categoryPorting() throws BatchProcessingCategoriesException {
		
		String strMethodName = "categoryPorting";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		

		try {

			
			categoriesDao.categoryPorting();

				

			
		} catch (BatchProcessingCategoriesException e) {
			LOG.error("Inside RssFeedServiceImpl : getHubCitiId : "
					+ e.getMessage());
			throw new BatchProcessingCategoriesException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		
		
		
	}

	public String insertImage(String name, String image, String hubCitiId) throws BatchProcessingCategoriesException {
		
		String strMethodName = "insertImage";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
String response=null;
String portingResponse=null;
		

		try {

			
			response=categoriesDao.insertImage(name, image, hubCitiId);

			if (null != response && response.equals(ApplicationConstants.SUCCESSTEXT)) {
				LOG.info("Inside CategoriesServiceImpl: insertImage :  " + ApplicationConstants.SUCCESSTEXT);
				response= ApplicationConstants.SUCCESSTEXT;
			} else {
				LOG.info("Inside CategoriesServiceImpl: insertImage:  " + ApplicationConstants.FAILURETEXT);
				portingResponse = ApplicationConstants.FAILURETEXT;
			}

			
		} catch (BatchProcessingCategoriesException e) {
			LOG.error("Inside CategoriesServiceImpl : getHubCitiId : "
					+ e.getMessage());
			throw new BatchProcessingCategoriesException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return response;
		
		// TODO Auto-generated method stub
		
		
	}

	public void processCategoriesDatabaseOperation(List<Category> items, String hubcitiId){
		String strMethodName = "processCategoriesDatabaseOperation";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		
		String response=null;
		String portingResponse=null;
		
		if (null != items && !items.isEmpty()) {
			
			
			try{
			for (int j = 0; j < items.size(); j++) {
				
				
				String image=null;
				
				if (items.get(j).getCategoryImagePath() != null) {
					
					
					if(PropertiesReader.getPropertyValue("tyler_hubcitiId")
							.equals(hubcitiId)){
						image= items.get(j).getCategoryImagePath();
					//	System.out.println(image + items.get(j).getCategoryName() );
						
					}
					else{
						//Adding changes
						int dot=items.get(j).getCategoryImagePath().lastIndexOf(".");
				    	int index=items.get(j).getCategoryImagePath().lastIndexOf("-");
				    	image=items.get(j).getCategoryImagePath().substring(0, index)+items.get(j).getCategoryImagePath().substring(dot);
					}
					
					response=insertImage(items.get(j).getCategoryName() + " News", image, hubcitiId);
					if (null != response && response.equals(ApplicationConstants.SUCCESSTEXT)) {
						LOG.info("Inside Utility: getTylerValues :  " + ApplicationConstants.SUCCESSTEXT);
					} else {
						LOG.info("Inside Utility: getTylerValues:  " + ApplicationConstants.FAILURETEXT);
						portingResponse = ApplicationConstants.FAILURETEXT;
					}

					break;
				}
				
			
			
			
			}
			}catch (BatchProcessingCategoriesException e) {
				LOG.error("Inside CategoriesServiceImpl : getHubCitiId : "
						+ e.getMessage());
				
			}

			LOG.info(ApplicationConstants.METHODEND + strMethodName);
			
		}
		
		
	}

}
