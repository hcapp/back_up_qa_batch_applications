package com.hubciti.pushnotification.exception;

/**
 * Exception class.
 * 
 * @author dhruvanath_mm
 *
 */
public class PushNotificationException extends Exception
{

	/**
	 * generated serial version id.
	 */
	private static final long serialVersionUID = -1385703071543005434L;

	/**
	 * PushNotificationException default constructor.
	 */
	public PushNotificationException()
	{
		super();
	}

	/**
	 * Constructor PushNotificationException with message.
	 * 
	 * @param message
	 *            as parameter
	 */
	public PushNotificationException(String message)
	{
		super(message);
	}

	/**
	 * Constructor PushNotificationException with cause message.
	 * 
	 * @param cause
	 *            as parameter
	 */
	public PushNotificationException(Throwable cause)
	{
		super(cause);
	}

	/**
	 * Constructor PushNotificationException with message and cause.
	 * 
	 * @param message
	 *            as parameter
	 * @param cause
	 *            as parameter
	 */
	public PushNotificationException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
