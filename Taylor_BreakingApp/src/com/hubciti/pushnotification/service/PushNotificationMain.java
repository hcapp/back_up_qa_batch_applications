package com.hubciti.pushnotification.service;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hubciti.pushnotification.common.helper.BatchTime;
import com.hubciti.pushnotification.common.helper.PropertiesReader;
import com.hubciti.pushnotification.exception.PushNotificationException;

/*---------------------------------------------------------------
 * PushNotificationMain Initiating push notification process 
 * 
 * @Author : Kirankumar Garaddi
 * @written : 7/12/2015
 * 
 * Compilation : PushNotificationMain.java
 * Execution   : PushNotificationMain
 * 
 ----------------------------------------------------------------*/

public class PushNotificationMain {

	private static final Logger LOG = (Logger) LoggerFactory.getLogger(PushNotificationMain.class);

	public static void main(String[] args) {

		LOG.info("-----------------------------------------------------------------------------");
		Calendar startCalendar = Calendar.getInstance();
		LOG.info("HubCiti Push Notification Process Starts @:" + startCalendar.getTime().toString());
		LOG.info("-----------------------------------------------------------------------------");

		Integer HubCitiId = Integer.parseInt(PropertiesReader.getPropertyValue("HUBCITIID"));
		String feedURL = PropertiesReader.getPropertyValue("URL");
		String type = PropertiesReader.getPropertyValue("TYPE");
		int numbers= Integer.parseInt(PropertiesReader.getPropertyValue("NUMBER"));
		
		PushNotificationService pushNotificationService = new PushNotificationService();
		LOG.info("HubCiti Id :  " + HubCitiId);
		LOG.info("Feed URL : " + feedURL);
		LOG.info("Feed TYPE : " + type);
		LOG.info("Feed Numbers : " + numbers);
		try {

			if (null != HubCitiId && null != feedURL) {
				pushNotificationService.sendNotification(HubCitiId,feedURL,type,numbers);
			} else {
				LOG.info("HubCitiId required to process the push notification ");
			}

		} catch (PushNotificationException e) {

			LOG.error("Error in PushNotificationMain: " + e.getMessage());
		}

		LOG.info("*****************************************************************************");
		Calendar endCalendar = Calendar.getInstance();

		LOG.info("HubCiti Push Notification  Process Ends @:" + endCalendar.getTime().toString());

		BatchTime.printTime(startCalendar, endCalendar);
		LOG.info("*****************************************************************************");

	}

}
