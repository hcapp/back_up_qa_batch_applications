package com.hubciti.pushnotification.common.helper;

/**
 * PayLoad Generator for Android FCM Server
 * 
 * @author kirankumar.garaddi
 *
 */
public class PayLoadGenerator {

	private int badge;
	private String body = "";
	private String alertMessage = "";

	public void setBadge(int badge) {
		this.badge = badge;
	}

	public Integer getBadge() {
		return badge;
	}

	public void setBadge(Integer badge) {
		this.badge = badge;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getAlertMessage() {
		return alertMessage;
	}

	public void setAlertMessage(String alertMessage) {
		this.alertMessage = alertMessage;
	}

	public PayLoadGenerator(int badge, String body, String alertMessage) {
		super();
		this.badge = badge;
		this.body = body;
		this.alertMessage = alertMessage;
	}

	public String getPayload() {
		return "{\"aps\":{\"badge\":" + this.getBadge()
				+ ",\"content-available\":1,\"body\":" + this.getBody()
				+ ",\"alert\":{\"loc-key\":\"" + this.getAlertMessage()
				+ "\"}}}";
	}
}
