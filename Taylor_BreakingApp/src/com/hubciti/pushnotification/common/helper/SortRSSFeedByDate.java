package com.hubciti.pushnotification.common.helper;

import java.util.Comparator;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hubciti.pushnotification.common.pojo.RSSFeedMessage;

public class SortRSSFeedByDate implements Comparator<RSSFeedMessage> {

	private static final Logger LOG = LoggerFactory.getLogger(SortRSSFeedByDate.class);

	@Override
	public int compare(RSSFeedMessage msg1, RSSFeedMessage msg2) {
		Date date1, date2;
		Integer dateCompare = 0;
		try {
			date1 = Utility.getDateFromString(msg1.getPubDate());
			date2 = Utility.getDateFromString(msg2.getPubDate());
			if (null != date1 && null != date2) {
				dateCompare = date2.compareTo(date1);
			} else if (null == date1 && null != date2) {
				LOG.error("PubDate is empty for title: " + msg1.getTitle() + ", link: " + msg1.getLink());
				dateCompare = 1;
			} else if (null == date2 && null != date1) {
				dateCompare = -1;
				LOG.error("PubDate is empty for title: " + msg2.getTitle() + ", link: " + msg2.getLink());
			} else {
				dateCompare = 0;
				LOG.error("PubDate is empty for title: " + msg1.getTitle() + ", " + msg2.getTitle() + ", link: " + msg1.getLink() + ", " + msg2.getLink());
			}
		} catch (Exception e) {
			LOG.error("Exception in SortRSSFeedByDate: Message: " + e.getMessage());
		}
		return dateCompare;
	}
}
