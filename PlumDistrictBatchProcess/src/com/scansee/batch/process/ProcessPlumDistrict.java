package com.scansee.batch.process;

import java.util.ArrayList;
import java.util.Calendar;

import org.apache.log4j.Logger;

import com.scansee.batch.common.pojo.ExternalAPIVendor;
import com.scansee.batch.exception.PlumDistrictBatchProcessException;
import com.scansee.batch.service.PlumDistrictService;
import com.scansee.batch.service.PlumDistrictServiceImpl;

/**
 * This class is used to execute Plum District Batch process.
 * 
 * @author malathi_lr
 */
public class ProcessPlumDistrict
{

	/**
	 * To get logger instance.
	 */
	private static final Logger LOG = Logger.getLogger(ProcessPlumDistrict.class);

	public static void main(String[] args) throws PlumDistrictBatchProcessException
	{
		LOG.info("*********************************************************************");
		LOG.info("ProcessPlumDistrict Batch Process Start @:" + Calendar.getInstance().getTime());
		LOG.info("Inside the method ProcessPlumDistrict");
		PlumDistrictService plumDistrictService = null;

		ArrayList<ExternalAPIVendor> objExternalAPIVendorList = new ArrayList<ExternalAPIVendor>();
		String apiPartnerID = null;

		try
		{
			plumDistrictService = new PlumDistrictServiceImpl();
			objExternalAPIVendorList = plumDistrictService.getAPIList("HotDeals");
			for (int i = 0; i < objExternalAPIVendorList.size(); i++)
			{
				if (objExternalAPIVendorList.get(i).getVendorName().equalsIgnoreCase("Plum District"))
				{
					apiPartnerID = objExternalAPIVendorList.get(i).getApiPartnerID();
				}
			}
			plumDistrictService.processPlumDistrictData(apiPartnerID);

		}
		catch (PlumDistrictBatchProcessException e)
		{

			LOG.error("Exception Occured in PlumDistrictBatchProcess:\n" + e);
			plumDistrictService.insertBatchStatus(0, e.getMessage(), Calendar.getInstance().getTime(), apiPartnerID);
			plumDistrictService.sendBatchProcessStatus();
		}

		LOG.info("*********************************************************************");
		LOG.info("ProcessPlumDistrict Batch Process END @:" + Calendar.getInstance().getTime());

	}

}
