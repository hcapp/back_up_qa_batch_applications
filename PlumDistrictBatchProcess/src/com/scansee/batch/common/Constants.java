package com.scansee.batch.common;

/**
 * The class has Constants.
 * 
 * @author sowjanya_d
 */
public class Constants
{
	/**
	 * MethodStart declared as String for logger messages.
	 */
	public static final String METHODSTART = "In side method >>> ";
	/**
	 * MethodEnd declared as String for logger messages.
	 */
	public static final String METHODEND = " Exiting method >>> ";
	/**
	 * ExceptionOccurred declared as String for logger messages.
	 */
	public static final String EXCEPTIONOCCURRED = "Exception Occurred in  >>> ";
	/**
	 * ErrorOccurred declared as String for logger messages.
	 */
	public static final String ERROROCCURRED = "Error Occurred in  >>> ";

	/**
	 * TechnicalProblemErroCode declared as String for getting error code.
	 */
	public static final String TECHNICALPROBLEMERRORCODE = "10001";
	/**
	 * TechnicalProblemErrorText declared as String for getting error text.
	 */
	public static final String TECHNICALPROBLEMERRORTEXT = "ExternalAPI Information (Vendors and input search parameters) not available in the database.";

	/**
	 * This status represents the success execution of external API.
	 */
	public static final String SUCCESS_STATUSCODE = "200";

	/**
	 * This represents a task SUCCESS execution status.
	 */
	public static final String SUCCESS = "SUCCESS";

	/**
	 * This represents a task FAILURE execution status.
	 */
	public static final String FAILURE = "FAILURE";

	/**
	 * This will be used to form URL.
	 */
	public static final String QUESTIONARK = "?";

	/**
	 * This will be used to form URL.
	 */
	public static final String AMPERSAND = "&";
	/**
	 * API submodule name.
	 */
	public static final String HOTDEALS = "HotDeals";

	/**
	 * This constant for retrieving Database error message.
	 */
	public static final String ERRORMESSAGE = "ErrorMessage";
	/**
	 * This constant for retrieving Database error code.
	 */
	public static final String ERRORNUMBER = "ErrorNumber";
	/**
	 * SuccessCode declared as String for getting success response code.
	 */
	public static final String SUCCESSCODE = "10000";
	/**
	 * SuccessResponseText declared as String for getting success response text.
	 */
	public static final String SUCCESSRESPONSETEXT = "SUCCESS";
	/**
	 * KOALA declared as String for getting KOALA response text.
	 */
	public static final String KOALA = "KOALA";

	/**
	 * Constant for wishpond.
	 */
	public static final String WISHPOND = "wishpond";
	/**
	 * API submodule name.
	 */
	public static final String THISLOCATION = "This Location";
	/**
	 * Constant for NOT APPLICABLE.
	 */
	public static final String NOTAPPLICABLE = "N/A";
	/**
	 * Constant for NO LOCATION.
	 */
	public static final String NOLOCATION = "No Locations found";

	/**
	 * constructor for Constants.
	 */
	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String SMTPHOST = "SMTP_Host";

	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String SMTPPORT = "SMTP_Port";

	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String EMAIL = "BatchProcessEmailNotification";

	public static final String EMAILCONFIG = "Email";
	/**
	 * SCHEMANAME declared as String for database schema name.
	 */
	public static final String SCHEMANAME = "dbo";
	private Constants()
	{

	}
}
