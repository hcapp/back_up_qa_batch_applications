/**
 * Project     : Scan See
 * File        : Category.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 20th January 2011
 */
package com.scansee.batch.common.pojo;

import com.scansee.batch.common.Constants;

public class AppConfiguration
{

	/**
	 * stockImagePath variable declared as String
	 */
	private String screenContent;
	/**
	 * stockHeader variable declared as String
	 */
	private String screenName;

	/**
	 * Retrieve the value of screenContent.
	 * 
	 * @return the screenContent
	 */
	public String getScreenContent()
	{
		return screenContent;
	}

	/**
	 * Set the value of screenContent.
	 * 
	 * @param screenContent
	 *            the screenContent to set
	 */
	public void setScreenContent(String screenContent)
	{
		if (null == screenContent)
		{
			this.screenContent = Constants.NOTAPPLICABLE;
		}
		else
		{
			this.screenContent = screenContent;
		}

	}

	/**
	 * Retrieve the value of screenName.
	 * 
	 * @return the screenName
	 */
	public String getScreenName()
	{
		return screenName;
	}

	/**
	 * Set the value of screenName.
	 * 
	 * @param screenName
	 *            the screenName to set
	 */
	public void setScreenName(String screenName)
	{
		if (null == screenName)
		{
			this.screenName = Constants.NOTAPPLICABLE;
		}
		else
		{
			this.screenName = screenName;
		}

	}

}
