package com.scansee.batch.common.pojo;

public class ExternalAPIVendor {

	/**
	 * Variable apiURLPath declared as String.
	 */
	private String apiURLPath;
	/**
	 * Variable apiUsagePriority declared as Integer.
	 */
	private Integer apiUsagePriority;
	/**
	 * Variable apiKey declared as String.
	 */
	private String apiKey;
	/**
	 * Variable apiUsageID declared as String.
	 */
	private String apiUsageID;
	/**
	 * Variable apiPartnerID declared as String.
	 */
	private String apiPartnerID;
	/**
	 * Variable vendorName declared as String.
	 */
	private String vendorName;
	
	/**
	 * Variable excecutionOrder declared as integer.
	 */
	private Integer excecutionOrder;
	
	/**
	 * @return the apiURLPath
	 */
	public String getApiURLPath()
	{
		return apiURLPath;
	}
	/**
	 * @param apiURLPath the apiURLPath to set
	 */
	public void setApiURLPath(String apiURLPath)
	{
		this.apiURLPath = apiURLPath;
	}
	/**
	 * @return the apiKey
	 */
	public String getApiKey()
	{
		return apiKey;
	}
	/**
	 * @param apiKey the apiKey to set
	 */
	public void setApiKey(String apiKey)
	{
		this.apiKey = apiKey;
	}
	/**
	 * @return the apiUsageID
	 */
	public String getApiUsageID()
	{
		return apiUsageID;
	}
	/**
	 * @param apiUsageID the apiUsageID to set
	 */
	public void setApiUsageID(String apiUsageID)
	{
		this.apiUsageID = apiUsageID;
	}
	/**
	 * @return the apiPartnerID
	 */
	public String getApiPartnerID()
	{
		return apiPartnerID;
	}
	/**
	 * @param apiPartnerID the apiPartnerID to set
	 */
	public void setApiPartnerID(String apiPartnerID)
	{
		this.apiPartnerID = apiPartnerID;
	}
	/**
	 * @return the vendorName
	 */
	public String getVendorName()
	{
		return vendorName;
	}
	/**
	 * @param vendorName the vendorName to set
	 */
	public void setVendorName(String vendorName)
	{
		this.vendorName = vendorName;
	}
	/**
	 * @return the apiUsagePriority
	 */
	public Integer getApiUsagePriority()
	{
		return apiUsagePriority;
	}
	/**
	 * @param apiUsagePriority the apiUsagePriority to set
	 */
	public void setApiUsagePriority(Integer apiUsagePriority)
	{
		this.apiUsagePriority = apiUsagePriority;
	}
	
	/**
	 * @param excecutionOrder the excecutionOrder to set
	 */
	public void setExcecutionOrder(Integer excecutionOrder) {
		this.excecutionOrder = excecutionOrder;
	}
	
	/**
	 * @return the excecutionOrder
	 */
	public Integer getExcecutionOrder() {
		return excecutionOrder;
	}
	
}
