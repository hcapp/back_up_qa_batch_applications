package com.scansee.batch.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.mail.MessagingException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.LoggerFactory;

import com.scansee.batch.common.Constants;
import com.scansee.batch.common.PropertiesReader;
import com.scansee.batch.common.Utility;
import com.scansee.batch.common.pojo.AppConfiguration;
import com.scansee.batch.common.pojo.BatchProcessStatus;
import com.scansee.batch.common.pojo.EmailComponent;
import com.scansee.batch.common.pojo.ExternalAPIVendor;
import com.scansee.batch.common.pojo.HotDealDetail;
import com.scansee.batch.dao.PlumDistrictDAO;
import com.scansee.batch.dao.PlumDistrictDAOImpl;
import com.scansee.batch.exception.PlumDistrictBatchProcessException;

/**
 * This is a service implementation class for Plum District Batch process
 * 
 * @author malathi_lr
 */
public class PlumDistrictServiceImpl implements PlumDistrictService
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(PlumDistrictServiceImpl.class);

	PlumDistrictDAO plumDistrictDAO = new PlumDistrictDAOImpl();

	/**
	 * This method is used to insert data into stage table.
	 */
	@Override
	public String processPlumDistrictData(String apiPartnerID) throws PlumDistrictBatchProcessException
	{
		LOG.info("Inside processPlumDistrictData");
		String response = null;
		PlumDistrictService plumDistrictService = new PlumDistrictServiceImpl();
		try
		{
			processPlumDistrictAutomation(apiPartnerID);
			plumDistrictService.sendBatchProcessStatus();

		}
		catch (PlumDistrictBatchProcessException e)
		{
			LOG.error("Exception Occured in processPlumDistrictData:\n" + e);
			throw new PlumDistrictBatchProcessException(e.getMessage());
		}

		LOG.info("Exit processPlumDistrictData");
		// Email Notification

		return response;
	}

	/**
	 * This method is used to move data from staging table to production table.
	 */
	@Override
	public String movingPlumDistrictDataToProduction(String latestFile) throws PlumDistrictBatchProcessException
	{
		LOG.info("Inside movingPlumDistrictDataToProduction");
		String response = null;
		try
		{
			response = plumDistrictDAO.plumDistrictDataPorting(latestFile);
			if (response.equals("SUCCESS"))
			{
				response = plumDistrictDAO.plumDistrictLocDataPorting();
				if (response.equals("SUCCESS"))
				{
					response = plumDistrictDAO.plumDistrictCategoryXRefUpdation();
					LOG.info("Moving processed file:" + latestFile);
					// Move processed files
					moveProcessedFile(latestFile);
				}
			}
		}
		catch (PlumDistrictBatchProcessException e)
		{
			LOG.error("Exception Occured in movingPlumDistrictDataToProduction:\n" + e);
			throw new PlumDistrictBatchProcessException(e.getMessage());
		}
		LOG.info("Exit movingPlumDistrictDataToProduction");
		return response;
	}

	/**
	 * This method is used to update batch process.
	 */
	@Override
	public String updateBatchStatus(int statusMessage, String reason, Date date, String apiName, String fileName)
			throws PlumDistrictBatchProcessException
	{
		final String methodName = "updateBatchStatus";
		LOG.info(Constants.METHODSTART + methodName);
		String response = plumDistrictDAO.updateBatchStatus(statusMessage, reason, date, apiName, fileName);
		LOG.info(Constants.METHODEND + methodName);
		return response;

	}

	/**
	 * This method is used to insert batch process.
	 */
	@Override
	public String insertBatchStatus(int statusMessage, String reason, Date date, String apiName) throws PlumDistrictBatchProcessException
	{
		final String methodName = "insertBatchStatus";
		LOG.info(Constants.METHODSTART + methodName);
		String response = plumDistrictDAO.insertBatchStatus(statusMessage, reason, date, apiName);
		LOG.info(Constants.METHODEND + methodName);
		return response;

	}

	/**
	 * This method is used to get API Information.
	 */
	@Override
	public ArrayList<ExternalAPIVendor> getAPIList(String moduleName) throws PlumDistrictBatchProcessException
	{
		ArrayList<ExternalAPIVendor> objExternalAPIVendor = new ArrayList<ExternalAPIVendor>();
		objExternalAPIVendor = plumDistrictDAO.getExternalAPIList(moduleName);
		return objExternalAPIVendor;

	}

	/**
	 * This method is used to connect FTP server and down load latest commission
	 * junction file.
	 * 
	 * @return Ftp file
	 * @throws CommissionJunctionBatchProcessException
	 */
	@SuppressWarnings("deprecation")
	public String processPlumDistrictAutomation(String apiPartnerID) throws PlumDistrictBatchProcessException
	{

		// down loading file from FTP Server
		FTPClient client = new FTPClient();
		ArrayList<FTPFile> ftpFiles = new ArrayList<FTPFile>();
		String fileName = null;
		List<String> fileNameLst = new ArrayList<String>();
		int index = 0;
		String[] fileNameArr;
		String[] filePart;
		Date fileDate = null;
		Map<Date, List<String>> fileDateNameMap = new HashMap<Date, List<String>>();
		String fileToDownload = null;
		Map<Integer, String> fileDateNameSeqNoMap = new HashMap<Integer, String>();

		try
		{

			// Get FTP Connection
			client = getFTPConnection();

			// Change directory to Active files directory
			client.changeWorkingDirectory(PropertiesReader.getPropertyValue("plumdistrictpath"));

			// Get all the files in the active directory
			FTPFile[] files = client.listFiles();

			// Filter .xlsx files
			for (FTPFile ftpFile : files)
			{

				String filename = ftpFile.getName();

				if (FilenameUtils.isExtension(filename, "xlsx"))
				{

					ftpFiles.add(ftpFile);

				}

			}

			// Sort files based on Date and Sequence Number
			if (ftpFiles != null & !ftpFiles.isEmpty())
			{

				fileNameArr = new String[ftpFiles.size()];

				// Put file name into an array for sorting
				for (FTPFile ftpFile : ftpFiles)
				{
					fileNameArr[index++] = ftpFile.getName();

				}

				for (int i = 0; i < fileNameArr.length; i++)
				{
					fileNameLst = new ArrayList<String>();
					fileName = fileNameArr[i];
					LOG.info("File Name:" + fileName);
					if (null != fileName && !"".equals(fileName))
					{
						// If size of split result is 2 then there is only one
						// file present for the date

						int dotIndex = fileName.lastIndexOf(".");

						if (dotIndex != -1)
						{
							filePart = fileName.substring(0, dotIndex).split("_");

							if (filePart.length == 2)
							{

								try
								{
									if (filePart[1].length() == 8)
									{
										fileDate = Utility.formatDateNoDelimeter(filePart[1]);
										fileNameLst.add(fileName);
										fileDateNameMap.put(fileDate, fileNameLst);

									}
									else
									{

										LOG.error("Inavlid Date in the File Name::" + fileName);
										insertBatchStatus(0, "Inavlid File Name:" + fileName, Calendar.getInstance().getTime(), apiPartnerID);
									}

								}
								catch (PlumDistrictBatchProcessException e)
								{
									LOG.error("Inavlid Date in the File Name::" + fileName);
									insertBatchStatus(0, "Inavlid File Name:" + fileName, Calendar.getInstance().getTime(), apiPartnerID);
								}

							}
							else if (filePart.length == 3)
							{
								// If size of split result is 3 then multiple
								// files
								// are present for the date

								try
								{
									if (filePart[1].length() == 8)
									{
										fileDate = Utility.formatDateNoDelimeter(filePart[1]);
										if (fileDateNameMap.containsKey(fileDate))
										{
											List<String> fileNameLstNew = fileDateNameMap.get(fileDate);
											fileNameLstNew.add(fileName);
											fileDateNameMap.put(fileDate, fileNameLstNew);

										}
										else
										{
											fileNameLst.add(fileName);
											fileDateNameMap.put(fileDate, fileNameLst);

										}

									}
									else
									{

										LOG.error("Inavlid Date in the File Name::" + fileName);
										insertBatchStatus(0, "Inavlid File Name:" + fileName, Calendar.getInstance().getTime(), apiPartnerID);
									}

								}
								catch (PlumDistrictBatchProcessException e)
								{
									LOG.error("Inavlid Date::" + fileName);
									insertBatchStatus(0, "Inavlid File Name:" + fileName, Calendar.getInstance().getTime(), apiPartnerID);
								}

							}

						}

						else
						{

							LOG.error("Inavlid File::" + fileName);
							insertBatchStatus(0, "Inavlid File Name:" + fileName, Calendar.getInstance().getTime(), apiPartnerID);
						}

					}

				}

				// Sort map
				fileDateNameMap = new TreeMap<Date, List<String>>(fileDateNameMap);
				// Iterate Map
				@SuppressWarnings("rawtypes")
				Iterator iterator = fileDateNameMap.entrySet().iterator();
				while (iterator.hasNext())
				{
					@SuppressWarnings("unchecked")
					Map.Entry<Date, List<String>> entry = (Map.Entry<Date, List<String>>) iterator.next();
					List<String> fileNameListSort = (List<String>) entry.getValue();
					if (fileNameListSort.size() > 1)
					{
						for (String fileNm : fileNameListSort)
						{

							if (null != fileNm && !"".equals(fileNm))
							{
								int dotIndex = fileNm.lastIndexOf(".");
								if (dotIndex != -1)
								{
									String[] fileNameSeqNoSplit = fileNm.substring(0, dotIndex).split("_");

									if (fileNameSeqNoSplit.length == 3)
									{
										if (null != fileNameSeqNoSplit[2] && !"".equals(fileNameSeqNoSplit[2]))
										{
											fileDateNameSeqNoMap.put(Integer.valueOf(fileNameSeqNoSplit[2]), fileNm);

										}

									}

								}

							}

						}

						fileDateNameSeqNoMap = new TreeMap<Integer, String>(fileDateNameSeqNoMap);

						for (Map.Entry<Integer, String> mapEntry : fileDateNameSeqNoMap.entrySet())
						{
							processFile(mapEntry.getValue(), client, apiPartnerID);
						}
					}
					else
					{
						if (!fileNameListSort.isEmpty())
						{

							fileToDownload = fileNameListSort.get(0);
							processFile(fileToDownload, client, apiPartnerID);
						}

					}

				}

			}
			else
			{

				LOG.error("No Files to Process");
				insertBatchStatus(0, "No Files to Process", Calendar.getInstance().getTime(), apiPartnerID);
			}

		}
		catch (SocketException exception)
		{
			LOG.info("Inside PlumDistrictServiceImpl : getPlumDistrictFileFromFtp : " + exception);
			throw new PlumDistrictBatchProcessException(exception);
		}
		catch (IOException exception)
		{
			LOG.info("Inside PlumDistrictServiceImpl : getPlumDistrictFileFromFtp : " + exception);
			throw new PlumDistrictBatchProcessException(exception);
		}
		finally
		{
			// fos.close();
			try
			{
				client.disconnect();

			}
			catch (IOException exception)
			{
				LOG.info("Inside PlumDistrictServiceImpl : getPlumDistrictFileFromFtp : " + exception);
				throw new PlumDistrictBatchProcessException(exception);
			}
		}
		return null;

	}

	public static ArrayList<String> moveProcessedFile(String latestFile) throws PlumDistrictBatchProcessException
	{

		LOG.info("inside moveProcessedFile");
		FTPClient client = new FTPClient();
		String processedFilePath = null;
		String activeFilePath = null;
		Date date = new Date();
		try
		{

			client = getFTPConnection();
			processedFilePath = PropertiesReader.getPropertyValue("processedfilepath");
			activeFilePath = PropertiesReader.getPropertyValue("plumdistrictpath");
			// client.changeWorkingDirectory(PropertiesReader.getPropertyValue("plumdistrictpath"));
			if (null != latestFile && !"".equals(latestFile))
			{

				String renameFile = FilenameUtils.removeExtension(latestFile) + "_" + date.getTime() + ".xlsx";
				boolean status = client.rename(activeFilePath + latestFile, processedFilePath + renameFile);

				LOG.info("File movement status:" + status);
				if (status)
				{

					LOG.info("Successfully moved process file::" + latestFile);
				}

			}

		}
		catch (SocketException exception)
		{
			LOG.info("Inside PlumDistrictServiceImpl : moveProcessedFile : " + exception);
			throw new PlumDistrictBatchProcessException(exception);
		}
		catch (IOException exception)
		{
			LOG.info("Inside PlumDistrictServiceImpl : moveProcessedFile : " + exception);
			throw new PlumDistrictBatchProcessException(exception);
		}
		finally
		{
			// fos.close();
			try
			{
				client.disconnect();

			}
			catch (IOException exception)
			{
				LOG.info("Inside PlumDistrictServiceImpl : moveProcessedFile : " + exception);
				throw new PlumDistrictBatchProcessException(exception);
			}
		}
		LOG.info("Exit moveProcessedFile");
		return null;
	}

	public static FTPClient getFTPConnection() throws PlumDistrictBatchProcessException
	{

		FTPClient client = new FTPClient();
		int reply;
		try
		{
			client.connect(PropertiesReader.getPropertyValue("ftp_url"));
			client.login(PropertiesReader.getPropertyValue("ftp_username"), PropertiesReader.getPropertyValue("ftp_password"));
			// After connection attempt, you should check the reply code to
			// verify
			// success.
			reply = client.getReplyCode();

			if (!FTPReply.isPositiveCompletion(reply))
			{
				client.disconnect();
				LOG.error("FTP server refused connection.");
				throw new PlumDistrictBatchProcessException("FTP server refused connection");
			}
			else
			{
				LOG.info("***FTP connection Established*****");
				client.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
				client.setFileTransferMode(FTP.BINARY_FILE_TYPE);

			}
		}
		catch (SocketException exception)
		{
			LOG.info("Inside PlumDistrictServiceImpl : getFTPConnection : " + exception);
			throw new PlumDistrictBatchProcessException(exception);
		}
		catch (IOException exception)
		{
			LOG.info("Inside PlumDistrictServiceImpl : getFTPConnection : " + exception);
			throw new PlumDistrictBatchProcessException(exception);
		}
		return client;
	}

	@SuppressWarnings("static-access")
	@Override
	public String sendBatchProcessStatus() throws PlumDistrictBatchProcessException
	{
		LOG.info("Inside EmailNotificationServiceImpl : getBatchProcessStatus ");
		ArrayList<BatchProcessStatus> batchProcessStatusList = null;
		ArrayList<AppConfiguration> appConfigurations = null;
		EmailComponent emailComponent = new EmailComponent();
		String smtpHost = null;
		String smtpPort = null;
		String emailrecipients[];
		String response = null;
		try
		{
			batchProcessStatusList = plumDistrictDAO.getBatchProcessStatus("Plum District");
			appConfigurations = plumDistrictDAO.getAppConfig(Constants.EMAIL);
			String mailContent = Utility.formEmailBody(batchProcessStatusList);
			ArrayList<AppConfiguration> emailConf = plumDistrictDAO.getAppConfig(Constants.EMAILCONFIG);

			for (int j = 0; j < emailConf.size(); j++)
			{
				if (emailConf.get(j).getScreenName().equals(Constants.SMTPHOST))
				{
					smtpHost = emailConf.get(j).getScreenContent();
				}
				if (emailConf.get(j).getScreenName().equals(Constants.SMTPPORT))
				{
					smtpPort = emailConf.get(j).getScreenContent();
				}
			}

			if (appConfigurations != null && !appConfigurations.isEmpty())
			{
				emailrecipients = appConfigurations.get(0).getScreenContent().split(",");
				emailComponent.multipleUsersmailingComponent("support@scansee.com", emailrecipients, "Plum District Batch Process Status",
						mailContent, smtpHost, smtpPort);

			}
			else
			{
				response = "Email recipients are not available. Please Configure";
			}

		}

		catch (MessagingException e)
		{
			LOG.error("Exception Occurred::::::::::::::::::" + e.getMessage());
			response = Constants.FAILURE;
		}
		catch (PlumDistrictBatchProcessException e)
		{
			response = Constants.FAILURE;
			LOG.error("Exception Occurred::::::::::::::::::" + e.getMessage());
		}
		return response;
	}

	public String processFile(String fileName, FTPClient client, String apiPartnerID) throws PlumDistrictBatchProcessException
	{
		String downloadPath = null;
		FileOutputStream fos = null;
		InputStream inputStream = null;
		ArrayList<HotDealDetail> dealDetailList = null;
		int status;
		String response = null;
		PlumDistrictService plumDistrictService = new PlumDistrictServiceImpl();
		try
		{
			if (null != fileName && !"".equals(fileName))
			{

				downloadPath = PropertiesReader.getPropertyValue("plumdistrictdownloadpath");

				File obj = new File(downloadPath);
				if (!obj.exists())
				{
					obj.mkdir();
				}
				fos = new FileOutputStream(downloadPath + fileName);
				client.retrieveFile(fileName, fos);

				File file = new File(downloadPath + fileName);
				inputStream = new FileInputStream(file);
				dealDetailList = Utility.getHotDealDetailFromXLSXFile(inputStream);
				if (null != dealDetailList && !dealDetailList.isEmpty())
				{
					plumDistrictDAO.batchAPIPlumDistrictDataRefresh();
					response = plumDistrictDAO.insertPlumDistrictData(dealDetailList);
					response = plumDistrictService.movingPlumDistrictDataToProduction(fileName);

					if (null != response && response.equals("SUCCESS"))
					{
						status = 1;
					}
					else
					{
						status = 0;
					}
					plumDistrictService.updateBatchStatus(status, response, Calendar.getInstance().getTime(), apiPartnerID, fileName);
				}
				else
				{
					LOG.info("Empty file");
				}
			}
		}
		catch (FileNotFoundException exception)
		{
			LOG.info("Inside PlumDistrictServiceImpl : processFile : " + exception);
			throw new PlumDistrictBatchProcessException(exception);
		}
		catch (IOException exception)
		{
			LOG.info("Inside PlumDistrictServiceImpl : processFile : " + exception);
			throw new PlumDistrictBatchProcessException(exception);
		}
		return null;
	}
}
