package com.scansee.batch.service;

import java.util.ArrayList;
import java.util.Date;

import com.scansee.batch.common.pojo.ExternalAPIVendor;
import com.scansee.batch.exception.PlumDistrictBatchProcessException;

public interface PlumDistrictService
{

	public String processPlumDistrictData(String apiPartnerID) throws PlumDistrictBatchProcessException;

	public String movingPlumDistrictDataToProduction(String latestFile) throws PlumDistrictBatchProcessException;

	public String updateBatchStatus(int statusMessage, String reason, Date date, String apiName, String fileName)
			throws PlumDistrictBatchProcessException;

	public String insertBatchStatus(int statusMessage, String reason, Date date, String apiName) throws PlumDistrictBatchProcessException;

	public ArrayList<ExternalAPIVendor> getAPIList(String moduleName) throws PlumDistrictBatchProcessException;

	public String sendBatchProcessStatus() throws PlumDistrictBatchProcessException;
}
