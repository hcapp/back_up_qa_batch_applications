package com.scansee.batch.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.scansee.batch.common.Constants;
import com.scansee.batch.common.DbConnection;
import com.scansee.batch.common.Utility;
import com.scansee.batch.common.pojo.AppConfiguration;
import com.scansee.batch.common.pojo.BatchProcessStatus;
import com.scansee.batch.common.pojo.ExternalAPIVendor;
import com.scansee.batch.common.pojo.HotDealDetail;
import com.scansee.batch.exception.PlumDistrictBatchProcessException;

public class PlumDistrictDAOImpl implements PlumDistrictDAO
{

	/**
	 * Logger instance.
	 */
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(PlumDistrictDAOImpl.class);
	/**
	 * To get database connection.
	 */

	private DbConnection dbConnection = new DbConnection();

	/**
	 * Variable for jdbcTemplate.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	@Override
	public String insertPlumDistrictData(ArrayList<HotDealDetail> dealDetailList) throws PlumDistrictBatchProcessException
	{
		final String methodName = "insertPlumDistrictData";
		LOG.info(Constants.METHODSTART + methodName);
		String response = null;
		final List<Object[]> batch = new ArrayList<Object[]>();
		try
		{

			final SimpleJdbcTemplate simpleJdbcTemplate = dbConnection.getSimpleJdbcTemplate();
			for (int i = 0; i < dealDetailList.size(); i++)
			{
				{
					final Object[] values = new Object[] { dealDetailList.get(i).getHotdealId(), dealDetailList.get(i).getPartnerName(),
							dealDetailList.get(i).getProvider(), dealDetailList.get(i).getPrice(), dealDetailList.get(i).getDiscount(),
							dealDetailList.get(i).getSalePrice(), dealDetailList.get(i).getHotdealName(), dealDetailList.get(i).getShortDesc(),
							dealDetailList.get(i).getLongDesc(), dealDetailList.get(i).getImageUrl(), dealDetailList.get(i).getTermsAndCondtions(),
							dealDetailList.get(i).getUrl(), dealDetailList.get(i).getStartDate(), dealDetailList.get(i).getEndDate(),
							dealDetailList.get(i).getCategory(), dealDetailList.get(i).getCreatedDate(), dealDetailList.get(i).getModifiedDate(),
							dealDetailList.get(i).getCity(), dealDetailList.get(i).getState(), dealDetailList.get(i).getThirdPartyId() };
					batch.add(values);
				}
			}
			LOG.info("Plum District Batch processing -----" + batch);
			final int[] updateCounts = simpleJdbcTemplate.batchUpdate(
					"INSERT INTO APIPlumDistrictData VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", batch);

			if (updateCounts.length != 0)
			{
				response = Constants.SUCCESS;
			}
			else
			{
				response = Constants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new PlumDistrictBatchProcessException(exception);

		}

		LOG.info(Constants.METHODEND + methodName);
		return response;
	}

	@Override
	public String plumDistrictDataPorting(String fileName)
	{
		System.out.println("Inside plumDistrictDataPorting()");
		final String methodName = "plumDistrictDataPorting";
		LOG.info(Constants.METHODSTART + methodName);
		int result = 1;
		String response = null;
		try
		{
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchPlumDistrictDataPorting");
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("FileName", fileName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			result = (Integer) resultFromProcedure.get("Status");
			if (result == 0)
			{
				response = Constants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(Constants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				System.out.println(errorMsg);
				LOG.info(Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg);
				response = methodName + Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg;
			}
		}
		catch (Exception exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
		}
		return response;

	}

	@Override
	public String plumDistrictLocDataPorting()
	{
		System.out.println("Inside plumDistrictLocDataPorting()");
		final String methodName = "plumDistrictLocDataPorting";
		LOG.info(Constants.METHODSTART + methodName);
		int result = 1;
		String response = null;
		try
		{
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchPlumDistrictLocDataPorting");
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute();
			result = (Integer) resultFromProcedure.get("Status");
			if (result == 0)
			{
				response = Constants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(Constants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				System.out.println(errorMsg);
				LOG.info(Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg);
				response = methodName + Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg;
			}
		}
		catch (Exception exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
		}
		return response;

	}

	@Override
	public void batchAPIPlumDistrictDataRefresh()
	{
		System.out.println("Inside batchAPIPlumDistrictDataRefresh()");
		final String methodName = "batchAPIPlumDistrictDataRefresh";
		LOG.info(Constants.METHODSTART + methodName);
		try
		{
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchAPIPlumDistrictRefreshStagingTables");
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute();
		}
		catch (Exception exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
		}
	}

	@Override
	public String plumDistrictCategoryXRefUpdation()
	{
		System.out.println("Inside plumDistrictCategoryXRefUpdation()");
		final String methodName = "plumDistrictCategoryXRefUpdation";
		LOG.info(Constants.METHODSTART + methodName);
		int result = 1;
		String response = null;
		try
		{
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchPlumDistrictCategoryXRefUpdation");
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute();
			result = (Integer) resultFromProcedure.get("Status");
			if (result == 0)
			{
				response = Constants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(Constants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				System.out.println(errorMsg);
				LOG.info(Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg);
				response = methodName + Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg;
			}
		}
		catch (Exception exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
		}
		return response;

	}

	@Override
	public String updateBatchStatus(int status, String reason, Date date, String apiPartnerID, String fileName)
			throws PlumDistrictBatchProcessException
	{
		System.out.println("Inside updateBatchStatus()");
		final String methodName = "insertBatchStatus";
		LOG.info(Constants.METHODSTART + methodName);
		int result = 1;
		String response = null;
		Integer responseFromProc = null;
		try
		{

			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UpdateBatchLogFiles");
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("APIPartnerID", apiPartnerID);
			externalAPIListParameters.addValue("APIStatus", status);
			externalAPIListParameters.addValue("Reason", reason);
			externalAPIListParameters.addValue("ProcessedFileName", fileName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			responseFromProc = (Integer) resultFromProcedure.get("Status");

			if (responseFromProc == 0)
			{
				response = Constants.SUCCESS;
			}
			else
			{
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get(Constants.ERRORNUMBER));
				LOG.error("Error occurred in usp_UpdateBatchLog Store Procedure error number: {}" + errorNum + " and error message: {}");
				throw new PlumDistrictBatchProcessException(errorMsg);
			}
		}
		catch (EmptyResultDataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new PlumDistrictBatchProcessException(exception);
		}

		return response;
	}

	@Override
	public String insertBatchStatus(int status, String reason, Date date, String apiPartnerID) throws PlumDistrictBatchProcessException
	{
		System.out.println("Inside insertBatchStatus()");
		final String methodName = "insertBatchStatus";
		LOG.info(Constants.METHODSTART + methodName);
		int result = 1;
		String response = null;
		Integer responseFromProc = null;
		try
		{

			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_InsertBatchLog");
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("APIPartnerID", apiPartnerID);
			externalAPIListParameters.addValue("APIStatus", status);
			externalAPIListParameters.addValue("Reason", reason);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			responseFromProc = (Integer) resultFromProcedure.get("Status");

			if (responseFromProc == 0)
			{
				response = Constants.SUCCESS;
			}
			else
			{

				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get(Constants.ERRORNUMBER));
				LOG.error("Error occurred in usp_UpdateBatchLog Store Procedure error number: {}" + errorNum + " and error message: {}");
				throw new PlumDistrictBatchProcessException(errorMsg);

			}
		}
		catch (EmptyResultDataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new PlumDistrictBatchProcessException(exception);
		}

		return response;
	}

	@Override
	public ArrayList<ExternalAPIVendor> getExternalAPIList(String moduleName) throws PlumDistrictBatchProcessException
	{
		final String methodName = "getExternalAPIList";
		// LOG.info(Constants.METHODSTART + methodName);

		ArrayList<ExternalAPIVendor> externalAPIList = null;
		try
		{
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GetAPIList").returningResultSet("externalAPIListInfo",
					ParameterizedBeanPropertyRowMapper.newInstance(ExternalAPIVendor.class));
			final SqlParameterSource externalAPIListParameters = new MapSqlParameterSource().addValue("prSubModule", moduleName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			externalAPIList = (ArrayList<ExternalAPIVendor>) resultFromProcedure.get("externalAPIListInfo");
			if (null != resultFromProcedure.get(Constants.ERRORNUMBER))
			{
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get(Constants.ERRORNUMBER));
				LOG.error("Error occurred in usp_GetAPIList Store Procedure error number: {}" + errorNum + " and error message: {}");
				throw new PlumDistrictBatchProcessException(errorMsg);
			}
			else if (null != externalAPIList && !externalAPIList.isEmpty())
			{
				return externalAPIList;
			}

		}
		catch (EmptyResultDataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new PlumDistrictBatchProcessException(exception.getMessage());

		}
		catch (DataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new PlumDistrictBatchProcessException(exception.getMessage());

		}
		LOG.info(Constants.METHODEND + methodName);
		return externalAPIList;
	}

	@Override
	public ArrayList<BatchProcessStatus> getBatchProcessStatus(String apiPartnerName) throws PlumDistrictBatchProcessException
	{
		LOG.info("Inside EmailNotificationDAOImpl : getBatchProcessStatus ");
		ArrayList<BatchProcessStatus> batchProcessStatusList = null;
		try
		{

			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);

			simpleJdbcCall.withProcedureName("usp_BatchLogFiles").returningResultSet("BatchProcessStatusList",
					ParameterizedBeanPropertyRowMapper.newInstance(BatchProcessStatus.class));
			Date date = new Date();
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("Date", Utility.getFormattedDate());
			externalAPIListParameters.addValue("APIPartnerName", apiPartnerName);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			Integer status = (Integer) resultFromProcedure.get("Status");

			batchProcessStatusList = (ArrayList<BatchProcessStatus>) resultFromProcedure.get("BatchProcessStatusList");

		}
		catch (DataAccessException e)
		{
			LOG.error("Inside EmailNotificationDAOImpl : getBatchProcessStatus : " + e);
			throw new PlumDistrictBatchProcessException(e);
		}
		catch (ParseException e)
		{
			LOG.error("Inside EmailNotificationDAOImpl : getBatchProcessStatus : " + e);
			throw new PlumDistrictBatchProcessException(e);
		}
		// TODO Auto-generated method stub
		return batchProcessStatusList;
	}

	@Override
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws PlumDistrictBatchProcessException
	{
		LOG.info("Inside EmailNotificationDAOImpl : getAppConfig ");
		ArrayList<AppConfiguration> appConfigurationList = null;
		try
		{

			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);

			simpleJdbcCall.withProcedureName("usp_GetScreenContent").returningResultSet("AppConfigurationList",
					ParameterizedBeanPropertyRowMapper.newInstance(AppConfiguration.class));

			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("ConfigurationType", configType);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			Integer status = (Integer) resultFromProcedure.get("Status");

			appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");

		}
		catch (DataAccessException e)
		{
			LOG.error("Inside EmailNotificationDAOImpl : getAppConfig : " + e);
			throw new PlumDistrictBatchProcessException(e);
		}
		// TODO Auto-generated method stub
		return appConfigurationList;
	}
}
