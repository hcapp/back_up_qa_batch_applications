package com.scansee.batch.dao;

import java.util.ArrayList;
import java.util.Date;

import com.scansee.batch.common.pojo.AppConfiguration;
import com.scansee.batch.common.pojo.BatchProcessStatus;
import com.scansee.batch.common.pojo.ExternalAPIVendor;
import com.scansee.batch.common.pojo.HotDealDetail;
import com.scansee.batch.exception.PlumDistrictBatchProcessException;

public interface PlumDistrictDAO
{

	public String insertPlumDistrictData(ArrayList<HotDealDetail> dealDetailList) throws PlumDistrictBatchProcessException;

	public String plumDistrictDataPorting(String fileName) throws PlumDistrictBatchProcessException;

	public String plumDistrictLocDataPorting() throws PlumDistrictBatchProcessException;

	public void batchAPIPlumDistrictDataRefresh() throws PlumDistrictBatchProcessException;

	public String plumDistrictCategoryXRefUpdation() throws PlumDistrictBatchProcessException;

	public String updateBatchStatus(int status, String reason, Date date, String apiPartnerID,String fileName) throws PlumDistrictBatchProcessException;

	public String insertBatchStatus(int status, String reason, Date date, String apiPartnerID) throws PlumDistrictBatchProcessException;

	public ArrayList<ExternalAPIVendor> getExternalAPIList(String moduleName) throws PlumDistrictBatchProcessException;

	public ArrayList<BatchProcessStatus> getBatchProcessStatus(String apiPartnerName) throws PlumDistrictBatchProcessException;

	public ArrayList<AppConfiguration> getAppConfig(String configType) throws PlumDistrictBatchProcessException;
}
