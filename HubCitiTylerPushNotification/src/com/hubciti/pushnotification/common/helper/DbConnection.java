package com.hubciti.pushnotification.common.helper;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.hubciti.pushnotification.exception.PushNotificationException;

/**
 * The class for connecting database.
 * 
 * @author murali_pnvb
 */
public class DbConnection
{
	/**
	 * This is for getting JDBC template.
	 * 
	 * @return JdbcTemplate
	 * @throws PushNotificationException
	 *             The exceptions are caught and a Exception defined for the
	 *             application is thrown which is caught in the Controller
	 *             layer.
	 */
	public static JdbcTemplate getConnection() throws PushNotificationException
	{
		JdbcTemplate jdbcTemplate = null;
		final Logger log = Logger.getLogger(DbConnection.class);
		try
		{
			SingleConnectionDataSource singleConnectionDataSource = new SingleConnectionDataSource();
		
			 singleConnectionDataSource.setDriverClassName(PropertiesReader.getPropertyValue("driver_className"));
			 singleConnectionDataSource.setUrl(PropertiesReader.getPropertyValue("db_url"));
			 singleConnectionDataSource.setUsername(PropertiesReader.getPropertyValue("db_username"));
			 singleConnectionDataSource.setPassword(PropertiesReader.getPropertyValue("db_password"));
			 
			/*singleConnectionDataSource.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");
			singleConnectionDataSource.setUrl("jdbc:jtds:sqlserver://10.11.202.226:1433/Scansee_09thJan2012_QA;SelectMethod=cursor");
			singleConnectionDataSource.setUsername("scansee");
			singleConnectionDataSource.setPassword("span@1234");*/
			jdbcTemplate = new JdbcTemplate(singleConnectionDataSource);
		}
		catch (DataAccessException e)
		{
			log.error("Exception occured in getConnection.", e);
			throw new PushNotificationException(e);
		}
		catch (Exception e)
		{
			log.error("Exception occured in getConnection..", e);
			throw new PushNotificationException(e);
		}
		return jdbcTemplate;
	}

	/**
	 * To get database information to connect.
	 * 
	 * @return simpleJdbcTemplate
	 * @throws PushNotificationException
	 *             The exceptions are caught and a Exception defined for the
	 *             application is thrown which is caught in the Controller
	 *             layer.
	 */
	public SimpleJdbcTemplate getSimpleJdbcTemplate() throws PushNotificationException
	{
		SimpleJdbcTemplate simpleJdbcTemplate = null;
		final Logger log = Logger.getLogger(DbConnection.class);
		try
		{
			SingleConnectionDataSource singleConnectionDataSource = new SingleConnectionDataSource();
			singleConnectionDataSource.setDriverClassName(PropertiesReader.getPropertyValue("driver_className"));
			singleConnectionDataSource.setUrl(PropertiesReader.getPropertyValue("db_url"));
			singleConnectionDataSource.setUsername(PropertiesReader.getPropertyValue("db_username"));
			singleConnectionDataSource.setPassword(PropertiesReader.getPropertyValue("db_password"));
			simpleJdbcTemplate = new SimpleJdbcTemplate(singleConnectionDataSource);
		}
		catch (DataAccessException e)
		{
			log.error("Exception occured in getConnection" + e);
			throw new PushNotificationException(e);
		}
		catch (Exception e)
		{
			log.error("Exception occured in getConnection" + e);
			throw new PushNotificationException(e);
		}
		return simpleJdbcTemplate;
	}
}
