package com.hubciti.pushnotification.common.helper;

public class Constants {

	/*
	 * General News URL 
	 */
	public static final String TOP_NEWS_URL = "http://www.tylerpaper.com/rss/custom/type/trendingstories";
	
	public static final String TOP_NEWS = "Top News";
	
	public static final String TRENDING_NEWS = "Trending News";
	
	/*
	 * Sports News URL 
	 */
	public static final String SPORTS_NEWS_URL = "http://www.etfinalscore.com/rss/custom/type/trendingstories";
	
	/*
	 * Trending + Breaking news
	 */
	public static final String TREDING_BREAKING_NEWS ="Trending";
	
	public static final String TREDING_BREAKING_NEWS_URL = "http://www.tylerpaper.com/rss/custom/type/pushalerts";
	
	
	public static final String SPORTS_NEWS = "Sports News";
	
	public static final String APP_SCHEMA_NAME = "HubCitiApp2_3_2";
	
	public static final String WEB_SCHEMA_NAME = "HubCitiWeb";
	
	public static final String SUCCESS_CODE = "10000";
	
	public static final String SUCCESS_MESSAGE = "SUCCESS";
	
	public static final String PLATFORM_ANDROID = "Android";
	
	public static final String PLATFORM_IOS = "IOS";
	
	public static final String COLLAPSE_KEY = "HubCiti Notification";
	
	public static final String TEXT_GCM_API_KEY = "GCM API KEY";
	
	public static final String TEXT_GCM_URL = "GCM URL";
	
	public static final String TEXT_APNS_HOST = "APNS HOST";
	
	public static final String TEXT_APNS_PORT = "APNS PORT";
	
	public static final String TEXT_APNS_CERT_PASSWORD = "APNS Certificate Password";
	
	public static final String TEXT_APNS_CERT_PATH = "APNS Certificate Path";
	
	public static final String SPLIT_RSSFEED_NEWS = "|~~|";
	
}
