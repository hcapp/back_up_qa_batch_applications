package com.hubciti.pushnotification.common.helper;


import javapns.back.DeviceFactory;
import javapns.data.PayLoad;

import org.apache.log4j.Logger;

/**
 * This class is Extend form {@link PayLoad} class to extend data limit to 2kB from 256 Bytes.
 * 
 * @author dhruvanath_mm
 *
 */
public class ExtendedPayLoad extends PayLoad {
	
	protected static final Logger logger = Logger.getLogger( DeviceFactory.class );

        public byte[] getPayloadAsBytes() throws Exception {
                byte[] payload = null;
                try {
                        payload = toString().getBytes("UTF-8");
                } catch (Exception ex) {
                        payload = toString().getBytes();
                }
               
                if ( payload.length > 2048 ) {
                        throw new Exception( "Payload too large...[2048 Bytes is the limit]" );
                }
                return payload;
        }
}
