package com.hubciti.pushnotification.common.pojo;

import java.util.List;

public class NotificationDetails {
	
//	private List<RSSFeedMessage> rssFeedList;
	
//	private List<Deal> dealList;
	
//	private List<DeviceId> deviceIdList;
	
	private String collapse_key;
	
	private Data data;
	
	private List<String> registration_ids;
	
	private String link;

/*	public List<RSSFeedMessage> getRssFeedList() {
		return rssFeedList;
	}

	public void setRssFeedList(List<RSSFeedMessage> rssFeedList) {
		this.rssFeedList = rssFeedList;
	}

	public List<Deal> getDealList() {
		return dealList;
	}

	public void setDealList(List<Deal> dealList) {
		this.dealList = dealList;
	}

	public List<DeviceId> getDeviceIdList() {
		return deviceIdList;
	}

	public void setDeviceIdList(List<DeviceId> deviceIdList) {
		this.deviceIdList = deviceIdList;
	}*/
	
	public String getCollapse_key() {
		return collapse_key;
	}

	public void setCollapse_key(String collapse_key) {
		this.collapse_key = collapse_key;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}
	
	public List<String> getRegistration_ids() {
		return registration_ids;
	}

	public void setRegistration_ids(List<String> registration_ids) {
		this.registration_ids = registration_ids;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
}
