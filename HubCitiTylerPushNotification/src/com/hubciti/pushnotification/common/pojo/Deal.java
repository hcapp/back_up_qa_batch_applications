package com.hubciti.pushnotification.common.pojo;

public class Deal {

	private Integer dealId;

	private String dealName;

	private String dealDesc;

	private String type;

	private String splUrl;

	private String hcName;

	private String retailerId;

	private String retailLocationId;

	private String endDate;

	private String endTime;
	
	/*private String pushDate;

	public Deal() {
		super();
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
		pushDate = dateFormat.format(date);
	}

	public String getPushDate() {
		return pushDate;
	}

	public void setPushDate(String pushDate) {
		this.pushDate = pushDate;
	}*/
	
	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getRetailerId() {
		return retailerId;
	}

	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}

	public String getRetailLocationId() {
		return retailLocationId;
	}

	public void setRetailLocationId(String retailLocationId) {
		this.retailLocationId = retailLocationId;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public String getDealDesc() {
		return dealDesc;
	}

	public void setDealDesc(String dealDesc) {
		this.dealDesc = dealDesc;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSplUrl() {
		return splUrl;
	}

	public void setSplUrl(String splUrl) {
		this.splUrl = splUrl;
	}

	public String getHcName() {
		return hcName;
	}

	public void setHcName(String hcName) {
		this.hcName = hcName;
	}

}
