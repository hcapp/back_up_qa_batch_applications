package com.hubciti.pushnotification.thread;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsNotification;
import com.notnoop.apns.ApnsService;

public class Test {

	public static void main(String[] args) {

		try {
			ApnsService service = null;
			String payload = null;
			String certFullPath = null;
			File file = null;

			certFullPath = "Tyler.p12";

			file = new File(certFullPath);
			if (!file.exists()) {
				System.out.println("File not exit");
			} else {

				try {

					service = APNS.newService().withCert(certFullPath, "span1234").withSandboxDestination().build();

				} catch (Exception e) {

					System.out.println(e.getMessage());

				}
				String jsonData = "{\"dealList\":[{\"dealId\":73988,\"dealName\":\"1\",\"pushFlag\":false,\"type\":\"Coupons\"}]}";

				try {

					Map<String, Integer> gers = new HashMap<String, Integer>();

					//gers.put("content-available", 1);

					payload = APNS.newPayload().badge(1).alertBody(jsonData).localizedKey("Here is your daily deal.").customFields(gers).toString();
					/*payload = payload.replace("1}", "1");
					payload += "}}";*/

					// payload =
					// "{\"aps\":{\"alert\":{\"body\":\"{\"dealList\":[{\"dealId\":73988,\"dealName\":\"RaviKumar\",\"pushFlag\":false,\"type\":\"Coupons\"}]} \",\"loc-key\":\"here is ur deal!\"},\"badge\":1}}";

					ApnsNotification ser = service.push("d652faf0af8b39bb5694d416a4e1ebb51cac082b4981bdd32944d7a794528774", payload);
					System.out.println(ser.getExpiry());
					System.out.println(ser.getIdentifier());
					System.out.println(ser.getDeviceToken());
					System.out.println(ser.getPayload().length);

					System.out.println(payload);

				} catch (Exception e) {

					System.out.println(e.getMessage());
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
