package com.hubciti.pushnotification.service;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hubciti.pushnotification.common.helper.BatchTime;
import com.hubciti.pushnotification.common.helper.PropertiesReader;
import com.hubciti.pushnotification.exception.PushNotificationException;

/*---------------------------------------------------------------
 * PushNotificationMain Initiating push notification process 
 * 
 * @Author : Kirankumar Garaddi
 * @written : 7/12/2015
 * 
 * Compilation : PushNotificationMain.java
 * Execution   : PushNotificationMain
 * 
 ----------------------------------------------------------------*/


public class PushNotificationMain {

	private static final Logger LOG = (Logger) LoggerFactory.getLogger(PushNotificationMain.class);
			
	public static void main(String[] args) {

		LOG.info("-----------------------------------------------------------------------------");
		Calendar startCalendar = Calendar.getInstance();
		LOG.info("HubCiti Push Notification Process Starts @:" + startCalendar.getTime().toString());
		LOG.info("-----------------------------------------------------------------------------");
		
		Integer HubCitiId = Integer.parseInt(PropertiesReader.getPropertyValue("HUBCITIID"));
		PushNotificationService pushNotificationService = new PushNotificationService();
		
		try {
			
			if (null != HubCitiId) {
				pushNotificationService.sendNotification(HubCitiId);
			} else {
				LOG.info("HubCitiId required to process the push notification ");
			}
			
		} catch (PushNotificationException e) {
			
			LOG.error("Error in PushNotificationMain: " + e.getMessage());
		}
		
		LOG.info("*****************************************************************************");
		Calendar endCalendar = Calendar.getInstance();
		LOG.info("HubCiti Push Notification  Process Ends @:" + endCalendar.getTime().toString());
		BatchTime.printTime(startCalendar, endCalendar);
		LOG.info("*****************************************************************************");
		
	}

}
