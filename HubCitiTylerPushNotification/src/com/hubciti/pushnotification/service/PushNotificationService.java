package com.hubciti.pushnotification.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hubciti.pushnotification.common.helper.Constants;
import com.hubciti.pushnotification.common.pojo.Configuration;
import com.hubciti.pushnotification.common.pojo.Data;
import com.hubciti.pushnotification.common.pojo.DeviceId;
import com.hubciti.pushnotification.common.pojo.NotificationDetails;
import com.hubciti.pushnotification.common.pojo.RSSFeedMessage;
import com.hubciti.pushnotification.dao.PushNotificationDao;
import com.hubciti.pushnotification.dao.PushNotificationDaoImpl;
import com.hubciti.pushnotification.exception.PushNotificationException;
import com.hubciti.pushnotification.thread.AndroidThread;
import com.hubciti.pushnotification.thread.IOSThread;

/*---------------------------------------------------------------
 * PushNotificationService for Fetching Articles & storing into database 
 * 
 *  @written : 7/12/2015
 * 
 * Compilation : PushNotificationService.java
 * Execution   : PushNotificationService
 *     
 * 
 ----------------------------------------------------------------*/
public class PushNotificationService {

	private static final Logger LOG = LoggerFactory.getLogger(PushNotificationService.class);

	/**
	 * Method to send notification to IOS and Android.
	 * 
	 * @throws PushNotificationException
	 */
	public void sendNotification(Integer hubCitiId) throws PushNotificationException {
		LOG.info("Method Start: sendNotification(), Class: PushNotificationService");
		PushNotificationDao pushNotificationDao = new PushNotificationDaoImpl();

		/*
		 * Parsing RSS Feeds.
		 */
		RSSFeedMessage feedMessage = PushNotificationHelper.getRSSFeedNews();

		/*
		 * Inserting RSS Feeds to database.
		 */
		String strDaoResp = pushNotificationDao.insertRSSFeedToDataBase(feedMessage);

		if (null != strDaoResp && Constants.SUCCESS_MESSAGE.equals(strDaoResp)) {
			LOG.info("RSS feed message inserted to database successfully");
		} else {
			LOG.error("Error in inserting RSS feed message to database");
		}

		/*
		 * 
		 * Get Configuration and notification messages.
		 * 
		 */
		List<Configuration> pushNotiConfigs = pushNotificationDao.getPushNotificationConfiguration(hubCitiId);
		Data objNotiData = pushNotificationDao.getNotificationMessages(hubCitiId);

		if (null == objNotiData) {
			LOG.info("No messages or deviceIds to send Push Notification");
			return;
		}

		/*
		 * Get android and iOS device IDs separately.
		 */
		List<DeviceId> iOSIds = new ArrayList<DeviceId>();
		List<DeviceId> androidIds = new ArrayList<DeviceId>();
		for (DeviceId details : objNotiData.getDeviceIdList()) {
			if (Constants.PLATFORM_IOS.equalsIgnoreCase(details.getPlatform())) {
				details.setPlatform(null);
				iOSIds.add(details);
			} else {
				details.setPlatform(null);
				androidIds.add(details);
			}
		}

		/*
		 * 
		 * Remove device id list as we are having it separately.
		 */
		objNotiData.setDeviceIdList(null);

		NotificationDetails notiDetails = new NotificationDetails();
		notiDetails.setData(objNotiData);

		IOSThread iOSPushThread = null;
		
		AndroidThread androidThread = null;

		/*
		 * 
		 * Send notification to iOS
		 * 
		 */
		if (!iOSIds.isEmpty()) {

			iOSPushThread = new IOSThread(iOSIds, notiDetails, pushNotiConfigs);
		}

		/*
		 * 
		 * Send notification to android
		 * 
		 */
		if (!androidIds.isEmpty()) {
			androidThread = new AndroidThread(androidIds, notiDetails, pushNotiConfigs);
		}

		try {

			if (null != iOSPushThread) {

				iOSPushThread.getThread().join();

			}
			if (null != androidThread) {

				androidThread.getThread().join();

			}

		} catch (InterruptedException e) {

			LOG.info("Error occurrred in joining threads , Class: PushNotificationHelper" + e.getMessage());

		}

		LOG.info("Method End: sendNotification(), Class: PushNotificationService");

	}

}
