package com.hubciti.pushnotification.dao;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.hubciti.pushnotification.common.helper.Constants;
import com.hubciti.pushnotification.common.helper.DbConnection;
import com.hubciti.pushnotification.common.pojo.Configuration;
import com.hubciti.pushnotification.common.pojo.Data;
import com.hubciti.pushnotification.common.pojo.Deal;
import com.hubciti.pushnotification.common.pojo.DeviceId;
import com.hubciti.pushnotification.common.pojo.RSSFeedMessage;
import com.hubciti.pushnotification.exception.PushNotificationException;

public class PushNotificationDaoImpl implements PushNotificationDao	{
	
	private static final Logger LOG = LoggerFactory.getLogger(PushNotificationDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	
	private SimpleJdbcCall simpleJdbcCall;
	
	/**
	 * Method to insert RSS Fees Message to database.
	 * @param feedMsg
	 * @return
	 * @throws PushNotificationException
	 */
	@Override
	public String insertRSSFeedToDataBase(RSSFeedMessage feedMsg) throws PushNotificationException {
		LOG.info("Method Start: insertRSSFeedToDataBase(), Class: PushNotificationDaoImpl");
		String respString = null;

		try {
			jdbcTemplate = DbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.WEB_SCHEMA_NAME);
			simpleJdbcCall.withProcedureName("usp_WebHcNewsCreation");

			final MapSqlParameterSource feedMessageParameter = new MapSqlParameterSource();
			
			feedMessageParameter.addValue("Title", feedMsg.getTitle());
			feedMessageParameter.addValue("Link", feedMsg.getLink());
			feedMessageParameter.addValue("PublishedDate", feedMsg.getPubDate());
			feedMessageParameter.addValue("Type", feedMsg.getType());
			feedMessageParameter.addValue("Description", feedMsg.getMediaText());
			feedMessageParameter.addValue("ImagePath", feedMsg.getImgPath());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(feedMessageParameter);
			final Integer status = (Integer) resultFromProcedure.get("Status");
			if (status == 0) {
				respString = Constants.SUCCESS_MESSAGE;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get("ErrorNumber");
				final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				LOG.error("Error occurred in usp_WebHcNewsCreation Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
				throw new PushNotificationException(errorMsg);
			}
		} catch (DataAccessException exception) {
			LOG.error("Exception Occured in insertRSSFeedToDataBase " + exception.getMessage());
			throw new PushNotificationException(exception);
		}
		LOG.info("Method End: insertRSSFeedToDataBase(), Class: PushNotificationDaoImpl");
		return respString;
	}

	/**
	 * DAO method to get notification configuration.
	 * @return
	 * @throws PushNotificationException
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Configuration> getPushNotificationConfiguration(Integer hubCitiId) throws PushNotificationException {
		LOG.info("Method Start: getPushNotificationConfiguration(), Class: PushNotificationDaoImpl");
		List<Configuration> configList = null;
		try {
			jdbcTemplate = DbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.WEB_SCHEMA_NAME);
			simpleJdbcCall.withProcedureName("usp_WebHcIOSAndroidDealPushNotifyCertificateDetails");
			simpleJdbcCall.returningResultSet("notificationConfig", ParameterizedBeanPropertyRowMapper.newInstance(Configuration.class));

			final MapSqlParameterSource feedMessageParameter = new MapSqlParameterSource();
			feedMessageParameter.addValue("HcHubCitiID", hubCitiId);
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(feedMessageParameter);
			final Integer status = (Integer) resultFromProcedure.get("Status");
			if (status == 0) {
				configList = (List<Configuration>)resultFromProcedure.get("notificationConfig");
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get("ErrorNumber");
				final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				LOG.error("Error occurred in usp_WebHcIOSAndroidDetails Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
				throw new PushNotificationException(errorMsg);
			}
		} catch (DataAccessException exception) {
			LOG.error("Exception Occured in insertRSSFeedToDataBase " + exception.getMessage());
			throw new PushNotificationException(exception);
		}
		
		LOG.info("Method End: getPushNotificationConfiguration(), Class: PushNotificationDaoImpl");
		return configList;
	}

	/**
	 * DAO method to get notification data.
	 * @return
	 * @throws PushNotificationException
	 */
	@Override
	public Data getNotificationMessages(Integer hubCitiId) throws PushNotificationException {
		LOG.info("Method Start: getNotificationMessages(), Class: PushNotificationDaoImpl");
		List<RSSFeedMessage> rssfeedMsgList = null;
		List<Deal> dealList = null;
		List<DeviceId> devideIdsList = null;
		Data notiDetails = null;
		Boolean rssFeedEmpty = true;
		Boolean dealEmpty = true;
		Boolean devideIdEmpty = true;
		try {
			jdbcTemplate = DbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.WEB_SCHEMA_NAME);
			simpleJdbcCall.withProcedureName("usp_HcUserPushNotify");
			simpleJdbcCall.returningResultSet("rssNewsList", ParameterizedBeanPropertyRowMapper.newInstance(RSSFeedMessage.class));
			simpleJdbcCall.returningResultSet("dealOfTheDay", ParameterizedBeanPropertyRowMapper.newInstance(Deal.class));
			simpleJdbcCall.returningResultSet("devideIds", ParameterizedBeanPropertyRowMapper.newInstance(DeviceId.class));

			final MapSqlParameterSource feedMessageParameter = new MapSqlParameterSource();
			feedMessageParameter.addValue("HcHubCitiID", hubCitiId);
                      feedMessageParameter.addValue("Type","Breaking News");			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(feedMessageParameter);
			final Integer status = (Integer) resultFromProcedure.get("Status");
			if (status == 0) {

				rssfeedMsgList = (List<RSSFeedMessage>) resultFromProcedure.get("rssNewsList");
				// Breaking News Implementation.
					//dealList = (List<Deal>) resultFromProcedure.get("dealOfTheDay");
                                dealList =null;        
				
				//Getting TokenIDs or Registration IDs to send notification.
				devideIdsList = (List<DeviceId>) resultFromProcedure.get("devideIds");
  
				
				if(null != rssfeedMsgList && !rssfeedMsgList.isEmpty())	{
					rssFeedEmpty = false;
				}
				
				if(null != devideIdsList && !devideIdsList.isEmpty())	{
					devideIdEmpty = false;
				}

				
				if(!devideIdEmpty)	{
					if(!rssFeedEmpty || !dealEmpty)	{
						notiDetails = new Data();
						notiDetails.setDeviceIdList(devideIdsList);
						if(!rssFeedEmpty)	{
							notiDetails.setRssFeedList(rssfeedMsgList);
						}															
					}
				}
				
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get("ErrorNumber");
				final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				LOG.error("Error occurred in usp_HcUserPushNotify Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
				throw new PushNotificationException(errorMsg);
			}
		} catch (DataAccessException exception) {
			LOG.error("Data Access Exception Occured in getNotificationMessages" + exception.getMessage());
			throw new PushNotificationException(exception);
		}
		
		LOG.info("Method End: getNotificationMessages(), Class: PushNotificationDaoImpl");
		return notiDetails;
	}

}
