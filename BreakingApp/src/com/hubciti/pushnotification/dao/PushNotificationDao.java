package com.hubciti.pushnotification.dao;

import java.util.List;

import com.hubciti.pushnotification.common.pojo.Configuration;
import com.hubciti.pushnotification.common.pojo.Data;
import com.hubciti.pushnotification.common.pojo.NotificationDetails;
import com.hubciti.pushnotification.common.pojo.RSSFeedMessage;
import com.hubciti.pushnotification.exception.PushNotificationException;

public interface PushNotificationDao {

	/**
	 * Method to insert RSS Fees Message to database.
	 * @param feedMsg
	 * @return
	 * @throws PushNotificationException
	 */
	public String insertRSSFeedToDataBase(RSSFeedMessage feedMsg) throws PushNotificationException;
	
	/**
	 * DAO method to get notification configuration.
	 * @return
	 * @throws PushNotificationException
	 */
	public List<Configuration> getPushNotificationConfiguration(Integer HubCiti) throws PushNotificationException;
	
	/**
	 * DAO method to get notification data.
	 * @return
	 * @throws PushNotificationException
	 */
	public Data getNotificationMessages(Integer hubCitiId) throws PushNotificationException;
}
