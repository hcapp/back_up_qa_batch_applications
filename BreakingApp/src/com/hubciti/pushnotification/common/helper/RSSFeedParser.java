package com.hubciti.pushnotification.common.helper;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

import com.hubciti.pushnotification.common.pojo.RSSFeed;

/**
 * Class to parse RSS feed for news.
 * 
 * @author dhruvanath_mm
 *
 */
public class RSSFeedParser {

	static final String TITLE = "title";
	static final String LINK = "link";
	static final String PUB_DATE = "pubDate";
	static final String DESCRIPTION = "description";
	static final String ITEM = "item";
	static final String MEDIA_TEXT = "text";
	static final String MEDIA_CONTENT = "content";

	private URL url;

	public RSSFeedParser(String feedUrl) {
		try {
			this.url = new URL(feedUrl);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	public RSSFeed readFeed() {
		RSSFeed feed = null;
		/*try {
			boolean isFeedHeader = true;
			// Set header values intial to the empty string
			String description = "";
			String title = "";
			String link = "";
			String pubdate = "";
			String mediaContent = "";
			String text = "";

			// First create a new XMLInputFactory
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			// Setup a new eventReader
			InputStream in = read();
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
			// read the XML document
			while (eventReader.hasNext()) {
				XMLEvent event = eventReader.nextEvent();
				if (event.isStartElement()) {
					String localPart = event.asStartElement().getName().getLocalPart();
					switch (localPart) {
					case ITEM:
						if (isFeedHeader) {
							isFeedHeader = false;
							feed = new RSSFeed(title, link, description);
						}
						event = eventReader.nextEvent();
						break;
					case TITLE:
						title = getCharacterData(event, eventReader);
						break;
					case DESCRIPTION:
						description = getCharacterData(event, eventReader);
						break;
					case LINK:
						link = getCharacterData(event, eventReader);
						break;
					case PUB_DATE:
						pubdate = getCharacterData(event, eventReader);
						break;
					case MEDIA_CONTENT:
						mediaContent = getCharacterData(event, eventReader);
						break;
					case MEDIA_TEXT:
						text = getCharacterData(event, eventReader);
						break;
					}
				} else if (event.isEndElement()) {
					if (event.asEndElement().getName().getLocalPart() == (ITEM)) {
						RSSFeedMessage message = new RSSFeedMessage();
						message.setTitle(title);
						message.setLink(link);
						message.setPubDate(pubdate);
						message.setText(text);
						message.setMediaContent(mediaContent);
						feed.getRssFeedMsg().add(message);
						event = eventReader.nextEvent();
						continue;
					}
				}
			}
		} catch (XMLStreamException e) {
			throw new RuntimeException(e);
		}*/
		return feed;
	}
	
	private String getCharacterData(XMLEvent event, XMLEventReader eventReader) throws XMLStreamException {
		String result = "";
		event = eventReader.nextEvent();
		if (event instanceof Characters) {
			result = event.asCharacters().getData();
		}
		return result;
	}

	private InputStream read() {
		try {
			return url.openStream();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
