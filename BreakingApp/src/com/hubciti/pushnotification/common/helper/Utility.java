package com.hubciti.pushnotification.common.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utility {

	private static final Logger LOG = LoggerFactory.getLogger(Utility.class);
	
	/**
	 * The constant for &#x0.
	 */
	private static final char[] NULL = "&#x0;".toCharArray();

	/**
	 * The constant for &amp.
	 */
	private static final char[] AMP = "&amp;".toCharArray();

	/**
	 * The constant for &lt.
	 */
	private static final char[] LT = "&lt;".toCharArray();

	/**
	 * The constant for &gt.
	 */
	private static final char[] GT = "&gt;".toCharArray();

	/**
	 * The constant for &quot.
	 */
	private static final char[] QUOT = "&quot;".toCharArray();

	/**
	 * The constant for &apos.
	 */
	private static final char[] APOS = "&apos;".toCharArray();

	/**
	 * Method Parse date string in format - Sat, 03 Jan 2015 20:27:16 -0600 and
	 * return date object.
	 * 
	 * @param date
	 * @return
	 */
	public static Date getDateFromString(String strDate) {
		Date objDate = null;
		if (null != strDate && !"".equals(strDate)) {
			try {
				objDate = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z").parse(strDate);
			} catch (ParseException e) {
				LOG.error("Error in parsing date. Error message: " + e.getMessage());
				return null;
			}
		}
		return objDate;
	}
	
	/**
	 * The method to remove the special characters.
	 * 
	 * @param inPutXml
	 *            The input xml.
	 * @return the xml with special charcters removed.
	 */

	public static String removeSpecialChars(String inPutXml) {

		/*
		 * final String methodName = "removeSpecialChars";
		 * LOG.info(HubCitiConstants.METHODSTART + methodName);
		 */

		String outPutxML = "";
		if (null != inPutXml) {
			outPutxML = null;
			outPutxML = inPutXml.replaceAll(new String(AMP), "&");
			outPutxML = outPutxML.replaceAll(new String(GT), ">");
			outPutxML = outPutxML.replaceAll(new String(LT), "<");
			outPutxML = outPutxML.replaceAll(new String(NULL), "null");
			// outPutxML = outPutxML.replaceAll(new String(QUOT), "\"\"");
			outPutxML = outPutxML.replaceAll(new String(QUOT), "\"");
			outPutxML = outPutxML.replaceAll(new String(APOS), "'");
			outPutxML = outPutxML.replaceAll("\n", "");
			outPutxML = outPutxML.replaceAll("&#xd;", "\r");
			outPutxML = outPutxML.replaceAll("&#39;", "'");
		}
		// LOG.info(HubCitiConstants.METHODEND + methodName);
		return outPutxML;
	}
}
