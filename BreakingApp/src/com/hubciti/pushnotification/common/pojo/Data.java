package com.hubciti.pushnotification.common.pojo;

import java.util.List;

public class Data {
	
	private List<RSSFeedMessage> rssFeedList;
	
	private List<Deal> dealList;
	
	private String link;
	
	private List<DeviceId> deviceIdList;
	
	private String notiMgs;
	
	public List<RSSFeedMessage> getRssFeedList() {
		return rssFeedList;
	}

	public void setRssFeedList(List<RSSFeedMessage> rssFeedList) {
		this.rssFeedList = rssFeedList;
	}

	public List<Deal> getDealList() {
		return dealList;
	}

	public void setDealList(List<Deal> dealList) {
		this.dealList = dealList;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
	
	public List<DeviceId> getDeviceIdList() {
		return deviceIdList;
	}

	public void setDeviceIdList(List<DeviceId> deviceIdList) {
		this.deviceIdList = deviceIdList;
	}

	public String getNotiMgs() {
		return notiMgs;
	}

	public void setNotiMgs(String notiMgs) {
		this.notiMgs = notiMgs;
	}

}
