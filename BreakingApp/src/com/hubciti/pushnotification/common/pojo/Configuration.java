package com.hubciti.pushnotification.common.pojo;

public class Configuration {

	private String configurationType;
	
	private String screenContent;

	public String getConfigurationType() {
		return configurationType;
	}

	public void setConfigurationType(String configurationType) {
		this.configurationType = configurationType;
	}

	public String getScreenContent() {
		return screenContent;
	}

	public void setScreenContent(String screenContent) {
		this.screenContent = screenContent;
	}
	
}
