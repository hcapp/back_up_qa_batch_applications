package com.hubciti.pushnotification.thread;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hubciti.pushnotification.common.pojo.Configuration;
import com.hubciti.pushnotification.common.pojo.DeviceId;
import com.hubciti.pushnotification.common.pojo.NotificationDetails;
import com.hubciti.pushnotification.service.PushNotificationHelper;
import com.hubciti.pushnotification.service.PushNotificationService;
/*---------------------------------------------------------------
 * AndroidThread for Configuring sending push notification to android devices
 * 
 * @Author : Kirankumar Garaddi
 * @written : 25/08/2015
 * 
 * Compilation : AndroidThread.java
 * Execution   : Java AndroidThread
 *     
 * 
 ----------------------------------------------------------------*/
public class AndroidThread implements Runnable {

	/**
	 * 
	 * Instance varibale for Logging information
	 * 
	 */
	private static final Logger LOG = LoggerFactory.getLogger(PushNotificationService.class);

	/**
	 * instance for storing list of android devices related information
	 */
	private List<DeviceId> androidIds;

	/**
	 * 
	 * Instance for notification details
	 * 
	 */
	private NotificationDetails notiDetails;

	/**
	 * 
	 * Instance for notification configurations
	 * 
	 */
	private List<Configuration> pushNotiConfigs;

	/**
	 * Thread for running process for sending pushnotification to GCM Server
	 * 
	 */
	private Thread thread;

	public Thread getThread() {
		return thread;
	}

	/**
	 * Constructor for Initalizing instaces
	 * @param androidIds
	 * @param notiDetails
	 * @param pushNotiConfigs
	 */
	public AndroidThread(List<DeviceId> androidIds, NotificationDetails notiDetails, List<Configuration> pushNotiConfigs) {
		super();
		this.androidIds = androidIds;
		this.notiDetails = notiDetails;
		this.pushNotiConfigs = pushNotiConfigs;
		this.thread = new Thread(this);
		this.thread.start();
	}

	/**
	 * 
	 * Thread run method for sending push notification to android devices
	 * 
	 */
	@Override
	public void run() {
		try {
			PushNotificationHelper.sendAndroidNotification(androidIds, notiDetails, pushNotiConfigs);
		} catch (IOException e) {
			LOG.error("Error Occured in sending android notification: " + e.getMessage());
		} catch (Exception e) {
			LOG.error("Error Occured in sending android notification: " + e.getMessage());
		}
	}

}
