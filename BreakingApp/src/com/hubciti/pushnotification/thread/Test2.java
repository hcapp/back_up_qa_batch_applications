package com.hubciti.pushnotification.thread;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hubciti.pushnotification.common.pojo.Deal;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;

public class Test2 {

	public static void main(String[] args) {

		try {
			ApnsService service = null;
			String payload = null;
			String certFullPath = null;
			File file = null;
			
			
			for(int badgeValue =1 ; badgeValue<10;badgeValue++){
				certFullPath = "Tyler.p12";
	
				file = new File(certFullPath);
				if (!file.exists()) {
					System.out.println("File not exit");
				} else {
	
					try {
	
						service = APNS.newService().withCert(certFullPath, "span1234").withSandboxDestination().build();
	
					} catch (Exception e) {
	
						System.out.println(e.getMessage());
	
					}
					List<Deal> deallist = new ArrayList<Deal>();
					
					Deal deal = new Deal();
					
					deal.setDealId(73988);
					deal.setDealName("2");
					deal.setType("coupons");
					deallist.add(deal);
					
					Gson gson = new Gson();
					String jsonData = gson.toJson(deallist);
					System.out.println(jsonData);
					
					try {
	
						Map<String, Integer> gers = new HashMap<String, Integer>();
	
						payload = APNS.newPayload().badge(badgeValue).alertBody(jsonData).localizedKey("Here is your daily deal.").customFields(gers).toString();
						System.out.println(payload);
						System.out.println("\"badge\":"+badgeValue+"}}");
						payload = payload.replace("\"badge\":"+badgeValue+"}}","\"badge\":"+badgeValue+",\"content-available\":1}}");
						System.out.println(payload);
						System.out.println();
					} catch (Exception e) {
	
						System.out.println(e.getMessage());
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
