package com.scansee.batch.service;

import java.util.ArrayList;
import java.util.Date;

import com.scansee.batch.common.pojos.CommissionJuncBatchStatus;
import com.scansee.batch.exception.CommissionJunctionBatchProcessException;
import com.scansee.externalapi.common.pojos.ExternalAPIVendor;

/**
 * This interface contains methods to dao layer for commission junction batch
 * process automation.
 * 
 * @author shyamsundara_hm
 */
public interface CommissionBatchService
{

	/**
	 * This method is used to insert commission junction data to staging table.
	 * 
	 * @return CommissionJuncBatchStatus containing response message,no of
	 *         record inserted etc.
	 * @throws CommissionJunctionBatchProcessException
	 */
	public String commissionBatchProcess(String apiPartnerID) throws CommissionJunctionBatchProcessException;

	/**
	 * This method is used to move commission junction data from staging table
	 * to production table.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	public String movingCJDataToProduction(String apiPartnerID,int batchNumber) throws CommissionJunctionBatchProcessException;

	/**
	 * This method is used to update commission junction batch process.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	public String updateCJBatchStatus(int statusMessage, String reason, Date date, String apiName, String fileName)
			throws CommissionJunctionBatchProcessException;

	/**
	 * This method is used to insert commission junction batch status.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	public String insertCJBatchStatus(int statusMessage, String reason, Date date, String apiName) throws CommissionJunctionBatchProcessException;

	/**
	 * This method is used to fetch commission junction api patner id.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	public ArrayList<ExternalAPIVendor> getAPIList(String moduleName) throws CommissionJunctionBatchProcessException;

	/**
	 * This method is used to send commission junction batch status
	 */
	public String sendCJBatchProcessStatus(String attachFilePath,int numberFilesToProcess, int numberOfFilesProcessed,String timeLeft, String timeTaken,int batchNumber) throws CommissionJunctionBatchProcessException;
}
