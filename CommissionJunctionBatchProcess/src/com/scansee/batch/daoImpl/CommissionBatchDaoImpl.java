package com.scansee.batch.daoImpl;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.scansee.batch.common.Constants;
import com.scansee.batch.common.DbConnection;
import com.scansee.batch.common.Utility;
import com.scansee.batch.common.pojos.AppConfiguration;
import com.scansee.batch.common.pojos.BatchProcessStatus;
import com.scansee.batch.common.pojos.CommissionJuncBatchStatus;
import com.scansee.batch.common.pojos.OnlineRetailerInfo;
import com.scansee.batch.dao.CommissionBatchDao;
import com.scansee.batch.exception.CommissionJunctionBatchProcessException;
import com.scansee.externalapi.common.pojos.ExternalAPIVendor;

public class CommissionBatchDaoImpl implements CommissionBatchDao
{
	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(CommissionBatchDaoImpl.class);

	/**
	 * To get database connection.
	 */

	private DbConnection dbConnection = new DbConnection();
	/**
	 * To get database connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * The method for inserting commission junction retailers and returned the
	 * status of the Batch process.This method calls deleteDuplicateDeals and
	 * insertIntoProductHotdeals methods.
	 * 
	 * @param dealMapBatchResponseParameters
	 *            request parameter
	 * @param dealMapBatchStatus
	 *            request parameter
	 * @return DealMapBatchStatus.
	 * @throws DealMapBatchException
	 *             The exceptions are caught and a Exception defined for the
	 *             application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public CommissionJuncBatchStatus insertCommissionDataToStagingTable(ArrayList<OnlineRetailerInfo> onlineRetailerlst, String fileName)
			throws CommissionJunctionBatchProcessException
	{

		final String methodName = "insertCommissionDataToStagingTable";
		LOG.info(Constants.METHODSTART + methodName);
		String response = null;
		int batchSize = 10000;
		int count = 0;
		CommissionJuncBatchStatus commissionJuncBatchStatusObj = null;
		List<Object[]> batch = new ArrayList<Object[]>();
		try
		{
			LOG.info("Inserting File Data:::::" + fileName);
			Object[] values = null;
			SimpleJdbcTemplate simpleJdbcTemplate = dbConnection.getSimpleJdbcTemplate();
			for (OnlineRetailerInfo onlineRetailerInfoObj : onlineRetailerlst)
			{

				values = new Object[] { onlineRetailerInfoObj.getPrgName(), onlineRetailerInfoObj.getPrgUrl(),
						onlineRetailerInfoObj.getRetStoreName(), onlineRetailerInfoObj.getKeyword(), onlineRetailerInfoObj.getDescription(),
						onlineRetailerInfoObj.getSku(), onlineRetailerInfoObj.getManufacturer(), onlineRetailerInfoObj.getUpc(),
						onlineRetailerInfoObj.getIsbn(), onlineRetailerInfoObj.getSalePrice(), onlineRetailerInfoObj.getPrice(),
						onlineRetailerInfoObj.getRetPrice(), onlineRetailerInfoObj.getBuyUrl(), onlineRetailerInfoObj.getImpressionUrl(),
						onlineRetailerInfoObj.getImageUrl(), onlineRetailerInfoObj.getPromotionalText(), onlineRetailerInfoObj.getStartDate(),
						onlineRetailerInfoObj.getEndDate(), onlineRetailerInfoObj.getInstock(), onlineRetailerInfoObj.getWarranty(),
						onlineRetailerInfoObj.getShippingCost(), onlineRetailerInfoObj.getCategory(), onlineRetailerInfoObj.getSubCategory() };
				batch.add(values);

				if (++count % batchSize == 0)
				{
					int[] updateCounts = simpleJdbcTemplate.batchUpdate("INSERT INTO APICommissionJunctionData  VALUES (?,?,?,?,?" + ",?,?,?,?,?"
							+ ",?,?,?,?,?" + ",?,?,?,?,?" + ",?,?,?)", batch);
					batch.clear();
				}

			}
			LOG.info("Commission Junction Batch processing -----" + batch);
			int[] updateCounts = simpleJdbcTemplate.batchUpdate("INSERT INTO APICommissionJunctionData  VALUES (?,?,?,?,?" + ",?,?,?,?,?"
					+ ",?,?,?,?,?" + ",?,?,?,?,?" + ",?,?,?)", batch);
			commissionJuncBatchStatusObj = new CommissionJuncBatchStatus();

			if (updateCounts.length > 0)
			{
				response = Constants.SUCCESS;

			}
			else
			{
				response = Constants.FAILURE;

			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new CommissionJunctionBatchProcessException(exception);

		}
		catch (Exception exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new CommissionJunctionBatchProcessException(exception);

		}

		LOG.info(Constants.METHODEND + methodName);
		return commissionJuncBatchStatusObj;
	}

	/**
	 * This method is used to move commission junction data from staging table
	 * to production table.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	@Override
	public String CJDataPortingUPC(String fileName,int batchNumber) throws CommissionJunctionBatchProcessException
	{

		final String methodName = "CJDataPortingUPC";
		LOG.info(Constants.METHODSTART + methodName);
		int result = 1;
		String response = null;
		try
		{
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchCommissionJunctionDataPortingUPC");
			final MapSqlParameterSource portingUPCParameter = new MapSqlParameterSource();
			portingUPCParameter.addValue("FileName", fileName);
			portingUPCParameter.addValue("BatchNumber", batchNumber);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(portingUPCParameter);
			result = (Integer) resultFromProcedure.get("Status");
			if (result == 0)
			{
				response = Constants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(Constants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				System.out.println(errorMsg);
				LOG.info(Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg);
				response = methodName + Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg;
			}
		}
		catch (Exception exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
			throw new CommissionJunctionBatchProcessException(exception);
		}
		return response;
	}

	/**
	 * This method is used to move commission junction data from staging table
	 * to production table.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	@Override
	public String CJDataPortingISBN(String fileName) throws CommissionJunctionBatchProcessException
	{

		final String methodName = "CJDataPortingISBN";
		LOG.info(Constants.METHODSTART + methodName);
		int result = 1;
		String response = null;
		try
		{
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchCommissionJunctionDataPortingISBN");
			final MapSqlParameterSource portingUPCParameters = new MapSqlParameterSource();
			portingUPCParameters.addValue("FileName", fileName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(portingUPCParameters);
			result = (Integer) resultFromProcedure.get("Status");
			if (result == 0)
			{
				response = Constants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(Constants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				System.out.println(errorMsg);
				LOG.info(Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg);
				response = methodName + Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg;
			}

		}
		catch (Exception exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
			throw new CommissionJunctionBatchProcessException(exception);
		}
		return response;
	}

	/**
	 * This method is used to move commission junction data from staging table
	 * to production table.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	@Override
	public String CJDataPortingProductName(String fileName) throws CommissionJunctionBatchProcessException
	{

		final String methodName = "CJDataPortingProductName";
		LOG.info(Constants.METHODSTART + methodName);
		int result = 1;
		String response = null;
		try
		{

			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchCommissionJunctionDataPortingProductName");
			final MapSqlParameterSource portingUPCParameters = new MapSqlParameterSource();
			portingUPCParameters.addValue("FileName", fileName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(portingUPCParameters);
			result = (Integer) resultFromProcedure.get("Status");
			if (result == 0)
			{
				response = Constants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(Constants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				System.out.println(errorMsg);
				LOG.info(Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg);
				response = methodName + Constants.ERROROCCURRED + errorNum + "errorMsg.." + errorMsg;
			}
		}
		catch (Exception exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
			throw new CommissionJunctionBatchProcessException(exception);
		}
		return response;
	}

	/**
	 * This method is used to fetch commission junction api patner id.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	@Override
	public String updateCJBatchStatus(int status, String reason, Date date, String apiPartnerID, String fileName)
			throws CommissionJunctionBatchProcessException
	{

		final String methodName = "updateCJBatchStatus";
		LOG.info(Constants.METHODSTART + methodName);
		String response = null;
		Integer responseFromProc = null;
		try
		{

			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UpdateBatchLogFiles");
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("APIPartnerID", apiPartnerID);
			externalAPIListParameters.addValue("APIStatus", status);
			externalAPIListParameters.addValue("Reason", reason);
			externalAPIListParameters.addValue("ProcessedFileName", fileName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			responseFromProc = (Integer) resultFromProcedure.get("Status");

			if (responseFromProc == 0)
			{
				response = Constants.SUCCESS;
			}
			else
			{
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get(Constants.ERRORNUMBER));
				LOG.error("Error occurred in usp_UpdateBatchLog Store Procedure error number: {}" + errorNum + " and error message: {}");
				throw new CommissionJunctionBatchProcessException(errorMsg);
			}
		}
		catch (EmptyResultDataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new CommissionJunctionBatchProcessException(exception);
		}

		return response;
	}

	/**
	 * This method is used to insert commission junction batch status.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	@Override
	public String insertCJBatchStatus(int status, String reason, Date date, String apiPartnerID) throws CommissionJunctionBatchProcessException
	{

		final String methodName = "insertCJBatchStatus";
		LOG.info(Constants.METHODSTART + methodName);
		String response = null;
		Integer responseFromProc = null;
		try
		{

			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_InsertBatchLog");
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("APIPartnerID", apiPartnerID);
			externalAPIListParameters.addValue("APIStatus", status);
			externalAPIListParameters.addValue("Reason", reason);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			responseFromProc = (Integer) resultFromProcedure.get("Status");

			if (responseFromProc == 0)
			{
				response = Constants.SUCCESS;
			}
			else
			{

				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get(Constants.ERRORNUMBER));
				LOG.error("Error occurred in usp_UpdateBatchLog Store Procedure error number: {}" + errorNum + " and error message: {}");
				throw new CommissionJunctionBatchProcessException(errorMsg);

			}
		}
		catch (EmptyResultDataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new CommissionJunctionBatchProcessException(exception);
		}

		return response;
	}

	@Override
	public ArrayList<ExternalAPIVendor> getExternalAPIList(String moduleName) throws CommissionJunctionBatchProcessException
	{
		final String methodName = "Commission junction :getExternalAPIList";
		LOG.info(Constants.METHODSTART + methodName);

		ArrayList<ExternalAPIVendor> externalAPIList = null;
		try
		{
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GetAPIList").returningResultSet("externalAPIListInfo",
					ParameterizedBeanPropertyRowMapper.newInstance(ExternalAPIVendor.class));
			final SqlParameterSource externalAPIListParameters = new MapSqlParameterSource().addValue("prSubModule", moduleName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			externalAPIList = (ArrayList<ExternalAPIVendor>) resultFromProcedure.get("externalAPIListInfo");
			if (null != resultFromProcedure.get(Constants.ERRORNUMBER))
			{
				final String errorMsg = (String) resultFromProcedure.get(Constants.ERRORMESSAGE);
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get(Constants.ERRORNUMBER));
				LOG.error("Error occurred in usp_GetAPIList Store Procedure error number: {}" + errorNum + " and error message: {}");
				throw new CommissionJunctionBatchProcessException(errorMsg);
			}
			else if (null != externalAPIList && !externalAPIList.isEmpty())
			{
				return externalAPIList;
			}

		}
		catch (EmptyResultDataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new CommissionJunctionBatchProcessException(exception);

		}
		catch (DataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new CommissionJunctionBatchProcessException(exception);

		}
		LOG.info(Constants.METHODEND + methodName);
		return externalAPIList;
	}

	/**
	 * This method is used to fetch commission junction api patner id.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */

	@Override
	public void RefreshCJData() throws CommissionJunctionBatchProcessException
	{

		final String methodName = "RefreshCJData";
		LOG.info(Constants.METHODSTART + methodName);
		try
		{
			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_BatchAPICommissionJunctionDataRefreshStagingTables");
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute();
		}
		catch (Exception exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName + exception);
		}

	}

	@Override
	public ArrayList<BatchProcessStatus> getBatchProcessStatus(String apiPartnerName,int batchNumber) throws CommissionJunctionBatchProcessException
	{

		final String methodName = "Commission junction :getBatchProcessStatus";
		LOG.info(Constants.METHODSTART + methodName);

		ArrayList<BatchProcessStatus> batchProcessStatusList = null;
		try
		{

			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);

			simpleJdbcCall.withProcedureName("usp_CommissionJunctionBatchLogFiles").returningResultSet("BatchProcessStatusList",
					ParameterizedBeanPropertyRowMapper.newInstance(BatchProcessStatus.class));
			Date date = new Date();
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("Date", Utility.getFormattedDate());
			externalAPIListParameters.addValue("APIPartnerName", apiPartnerName);
			externalAPIListParameters.addValue("BatchNumber", batchNumber);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			Integer status = (Integer) resultFromProcedure.get("Status");

			batchProcessStatusList = (ArrayList<BatchProcessStatus>) resultFromProcedure.get("BatchProcessStatusList");

		}
		catch (DataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new CommissionJunctionBatchProcessException(exception);
		}
		catch (ParseException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new CommissionJunctionBatchProcessException(exception);
		}
		// TODO Auto-generated method stub
		return batchProcessStatusList;
	}

	@Override
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws CommissionJunctionBatchProcessException
	{
		final String methodName = "Commission junction :getAppConfig";
		LOG.info(Constants.METHODSTART + methodName);
		ArrayList<AppConfiguration> appConfigurationList = null;
		try
		{

			jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(Constants.SCHEMANAME);

			simpleJdbcCall.withProcedureName("usp_GetScreenContent").returningResultSet("AppConfigurationList",
					ParameterizedBeanPropertyRowMapper.newInstance(AppConfiguration.class));

			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("ConfigurationType", configType);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			Integer status = (Integer) resultFromProcedure.get("Status");

			appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");

		}
		catch (DataAccessException exception)
		{
			LOG.error(Constants.EXCEPTIONOCCURRED + methodName, exception);
			throw new CommissionJunctionBatchProcessException(exception);

		}
		// TODO Auto-generated method stub
		return appConfigurationList;
	}
}
