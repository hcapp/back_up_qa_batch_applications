package com.scansee.batch.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import com.scansee.batch.common.pojos.AppConfiguration;
import com.scansee.batch.common.pojos.BatchProcessStatus;
import com.scansee.batch.common.pojos.CommissionJuncBatchStatus;
import com.scansee.batch.common.pojos.OnlineRetailerInfo;
import com.scansee.batch.exception.CommissionJunctionBatchProcessException;
import com.scansee.externalapi.common.pojos.ExternalAPIVendor;

/**
 * This class contains commission junction batch process automation methods.
 * 
 * @author shyamsundara_hm
 */
public interface CommissionBatchDao
{
	/**
	 * This method is used to insert commission junction data to staging table.
	 * 
	 * @return CommissionJuncBatchStatus containing response message,no of
	 *         record inserted etc.
	 * @throws CommissionJunctionBatchProcessException
	 */
	public CommissionJuncBatchStatus insertCommissionDataToStagingTable(ArrayList<OnlineRetailerInfo> onlineRetailerlst, String filename)
			throws CommissionJunctionBatchProcessException;

	/**
	 * This method is used to move commission junction data from staging table
	 * to production table.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	public String CJDataPortingUPC(String apiPartnerID,int batchNumber) throws CommissionJunctionBatchProcessException;

	/**
	 * This method is used to move commission junction data from staging table
	 * to production table.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	public String CJDataPortingISBN(String fileName) throws CommissionJunctionBatchProcessException;

	/**
	 * This method is used to move commission junction data from staging table
	 * to production table.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	public String CJDataPortingProductName(String fileName) throws CommissionJunctionBatchProcessException;

	/**
	 * This method is used to fetch commission junction api patner id.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	public String updateCJBatchStatus(int status, String reason, Date date, String apiPartnerID,String fileName) throws CommissionJunctionBatchProcessException;

	/**
	 * This method is used to insert commission junction batch status.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	public String insertCJBatchStatus(int status, String reason, Date date, String apiPartnerID) throws CommissionJunctionBatchProcessException;

	/**
	 * This method is used to fetch commission junction api patner id.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	public ArrayList<ExternalAPIVendor> getExternalAPIList(String moduleName) throws CommissionJunctionBatchProcessException;

	/**
	 * This method is used to truncate staging table data.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	public void RefreshCJData() throws CommissionJunctionBatchProcessException;
	
	/**
	 * This method is used to fetch smtp details
	 * 
	 */
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws CommissionJunctionBatchProcessException;
	/**
	 * This method is used to get commission junction batch process status
	 * 
	 */
	public ArrayList<BatchProcessStatus> getBatchProcessStatus(String apiPartnerName,int batchNumber) throws CommissionJunctionBatchProcessException;
}
