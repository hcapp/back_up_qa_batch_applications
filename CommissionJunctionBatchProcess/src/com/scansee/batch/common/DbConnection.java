package com.scansee.batch.common;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.scansee.batch.exception.CommissionJunctionBatchProcessException;

/**
 * This class is used for database connection.
 * 
 * @author shyamsundara_hm
 */

public class DbConnection
{

	/**
	 * @return JdbcTemplate
	 * @throws DealMapBatchException
	 *             The exceptions are caught and a Exception defined for the
	 *             application is thrown which is caught in the Controller
	 *             layer.
	 */
	public JdbcTemplate getConnection() throws CommissionJunctionBatchProcessException
	{
		JdbcTemplate jdbcTemplate = null;
		final Logger log = Logger.getLogger(DbConnection.class);
		try
		{
			SingleConnectionDataSource singleConnectionDataSource = new SingleConnectionDataSource();
			singleConnectionDataSource.setDriverClassName(PropertiesReader.getPropertyValue("driver_className"));
			singleConnectionDataSource.setUrl(PropertiesReader.getPropertyValue("db_url"));
			singleConnectionDataSource.setUsername(PropertiesReader.getPropertyValue("db_username"));
			singleConnectionDataSource.setPassword(PropertiesReader.getPropertyValue("db_password"));
			jdbcTemplate = new JdbcTemplate(singleConnectionDataSource);
		}
		catch (DataAccessException e)
		{
			log.error("Exception occured in getConnection." + e);
			throw new CommissionJunctionBatchProcessException(e);
		}
		catch (Exception e)
		{
			log.error("Exception occured in getConnection.." + e);
			throw new CommissionJunctionBatchProcessException(e);
		}
		return jdbcTemplate;
	}

	/**
	 * To get database information to connect.
	 * 
	 * @return simpleJdbcTemplate
	 * @throws DealMapBatchException
	 *             The exceptions are caught and a Exception defined for the
	 *             application is thrown which is caught in the Controller
	 *             layer.
	 */
	public SimpleJdbcTemplate getSimpleJdbcTemplate() throws CommissionJunctionBatchProcessException
	{
		SimpleJdbcTemplate simpleJdbcTemplate = null;
		final Logger log = Logger.getLogger(DbConnection.class);
		try
		{
			SingleConnectionDataSource singleConnectionDataSource = new SingleConnectionDataSource();
			singleConnectionDataSource.setDriverClassName(PropertiesReader.getPropertyValue("driver_className"));
			singleConnectionDataSource.setUrl(PropertiesReader.getPropertyValue("db_url"));
			singleConnectionDataSource.setUsername(PropertiesReader.getPropertyValue("db_username"));
			singleConnectionDataSource.setPassword(PropertiesReader.getPropertyValue("db_password"));
			simpleJdbcTemplate = new SimpleJdbcTemplate(singleConnectionDataSource);
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occured in getConnection" + exception);
			throw new CommissionJunctionBatchProcessException(exception);
		}
		catch (Exception exception)
		{
			log.error("Exception occured in getConnection" + exception);
			throw new CommissionJunctionBatchProcessException(exception);
		}
		return simpleJdbcTemplate;
	}
}
