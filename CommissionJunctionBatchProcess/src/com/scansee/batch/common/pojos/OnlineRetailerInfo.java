package com.scansee.batch.common.pojos;

import com.scansee.batch.common.Utility;

/**
 * this class is used to contain commission junction retailer information.
 * 
 * @author shyamsundara_hm
 */
public class OnlineRetailerInfo
{
	/**
	 * Created for program name.
	 */
	private String prgName;
	/**
	 * Created for Program url
	 */
	private String prgUrl;
	/**
	 * Created for catelog name
	 */
	private String catLogName;
	/**
	 * Created for last update date
	 */
	private String lastUpdateDate;
	/**
	 * Created for online store name
	 */
	private String retStoreName;
	/**
	 * Created for online store keywords
	 */
	private String keyword;
	/**
	 * Created for description
	 */
	private String description;
	/**
	 * Created for sku
	 */
	private String sku;
	/**
	 * Created for manufacturer
	 */
	private String manufacturer;
	/**
	 * Created for manufacturer id
	 */
	private String manufacturerId;
	/**
	 * Created for upc
	 */
	private String upc;
	/**
	 * Created for isbn
	 */
	private String isbn;
	/**
	 * Created for currency
	 */
	private String currency;
	/**
	 * Created for SALEPRICE
	 */
	private String salePrice;
	/**
	 * Created for price
	 */
	private String price;
	/**
	 * Created for retailer price
	 */
	private String retPrice;
	/**
	 * Created for from Price
	 */
	private String frmPrice;
	/**
	 * Created for buy url
	 */
	private String buyUrl;
	/**
	 * Created for impression url
	 */
	private String impressionUrl;
	/**
	 * Created for image url
	 */
	private String imageUrl;
	/**
	 * Created for Advertiser category
	 */
	private String advCategory;
	/**
	 * Created for third party Id
	 */
	private String thirdPartyId;
	/**
	 * Created for third party category
	 */
	private String thirdPartyCat;
	/**
	 * Created for author
	 */
	private String author;
	/**
	 * Created for artist
	 */
	private String artist;
	/**
	 * Created for title
	 */
	private String title;
	/**
	 * Created for publisher
	 */
	private String publisher;
	/**
	 * Created for label
	 */
	private String label;
	/**
	 * Created for format
	 */
	private String format;
	/**
	 * Created for special
	 */
	private String special;
	/**
	 * Created for gift
	 */
	private String gift;
	/**
	 * Created for promotional text
	 */
	private String promotionalText;
	/**
	 * Created for start date
	 */
	private String startDate;
	/**
	 * Created for end date
	 */
	private String endDate;
	/**
	 * Created for online
	 */
	private String online;
	/**
	 * Created for off line
	 */
	private String offline;
	/**
	 * Created for instock
	 */
	private String instock;
	/**
	 * created for conditions
	 */
	private String condition;
	/**
	 * Created for warranty
	 */
	private String warranty;
	/**
	 * Created for shipping cost
	 */
	private String shippingCost;
	/**
	 * Created for Category
	 */
	private String category;
	/**
	 * Created for sub category
	 */
	private String subCategory;

	public String getPrgName()
	{
		return prgName;
	}

	public void setPrgName(String prgName)
	{

		this.prgName = prgName;

	}

	public String getPrgUrl()
	{
		return prgUrl;
	}

	public void setPrgUrl(String prgUrl)
	{

		this.prgUrl = prgUrl;

	}

	public String getCatLogName()
	{
		return catLogName;
	}

	public void setCatLogName(String catLogName)
	{

		this.catLogName = catLogName;

	}

	public String getLastUpdateDate()
	{
		return lastUpdateDate;
	}

	public void setLastUpdateDate(String lastUpdateDate)
	{

		this.lastUpdateDate = lastUpdateDate;

	}

	public String getRetStoreName()
	{
		return retStoreName;
	}

	public void setRetStoreName(String retStoreName)
	{

		this.retStoreName = retStoreName;

	}

	public String getKeyword()
	{
		return keyword;
	}

	public void setKeyword(String keyword)
	{

		this.keyword = keyword;

	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{

		this.description = description;

	}

	public String getSku()
	{
		return sku;
	}

	public void setSku(String sku)
	{

		this.sku = sku;

	}

	public String getManufacturer()
	{
		return manufacturer;
	}

	public void setManufacturer(String manufacturer)
	{

		this.manufacturer = manufacturer;

	}

	public String getManufacturerId()
	{
		return manufacturerId;
	}

	public void setManufacturerId(String manufacturerId)
	{

		this.manufacturerId = manufacturerId;

	}

	public String getUpc()
	{
		return upc;
	}

	public void setUpc(String upc)
	{

		this.upc = upc;

	}

	public String getIsbn()
	{
		return isbn;
	}

	public void setIsbn(String isbn)
	{

		this.isbn = isbn;

	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(String currency)
	{

		this.currency = currency;

	}

	public String getSalePrice()
	{
		return salePrice;
	}

	public void setSalePrice(String salePrice)
	{

		this.salePrice = salePrice;

	}

	public String getPrice()
	{
		return price;
	}

	public void setPrice(String price)
	{

		this.price = price;

	}

	public String getRetPrice()
	{
		return retPrice;
	}

	public void setRetPrice(String retPrice)
	{

		this.retPrice = retPrice;

	}

	public String getFrmPrice()
	{
		return frmPrice;
	}

	public void setFrmPrice(String frmPrice)
	{

		this.frmPrice = frmPrice;

	}

	public String getBuyUrl()
	{
		return buyUrl;
	}

	public void setBuyUrl(String buyUrl)
	{

		this.buyUrl = buyUrl;

	}

	public String getImpressionUrl()
	{
		return impressionUrl;
	}

	public void setImpressionUrl(String impressionUrl)
	{

		this.impressionUrl = impressionUrl;

	}

	public String getImageUrl()
	{
		return imageUrl;
	}

	public void setImageUrl(String imageUrl)
	{
		this.imageUrl = imageUrl;

	}

	public String getAdvCategory()
	{
		return advCategory;
	}

	public void setAdvCategory(String advCategory)
	{

		this.advCategory = advCategory;

	}

	public String getThirdPartyId()
	{
		return thirdPartyId;
	}

	public void setThirdPartyId(String thirdPartyId)
	{

		this.thirdPartyId = thirdPartyId;

	}

	public String getThirdPartyCat()
	{
		return thirdPartyCat;
	}

	public void setThirdPartyCat(String thirdPartyCat)
	{

		this.thirdPartyCat = thirdPartyCat;

	}

	public String getAuthor()
	{
		return author;
	}

	public void setAuthor(String author)
	{

		this.author = author;

	}

	public String getArtist()
	{
		return artist;
	}

	public void setArtist(String artist)
	{

		this.artist = artist;

	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{

		this.title = title;

	}

	public String getPublisher()
	{
		return publisher;
	}

	public void setPublisher(String publisher)
	{

		this.publisher = publisher;

	}

	public String getLabel()
	{
		return label;
	}

	public void setLabel(String label)
	{

		this.label = label;

	}

	public String getFormat()
	{
		return format;
	}

	public void setFormat(String format)
	{

		this.format = format;

	}

	public String getSpecial()
	{
		return special;
	}

	public void setSpecial(String special)
	{

		this.special = special;

	}

	public String getGift()
	{
		return gift;
	}

	public void setGift(String gift)
	{

		this.gift = gift;

	}

	public String getPromotionalText()
	{
		return promotionalText;
	}

	public void setPromotionalText(String promotionalText)
	{

		this.promotionalText = promotionalText;

	}

	public String getStartDate()
	{
		return startDate;
	}

	public void setStartDate(String startDate)
	{

		this.startDate = startDate;

	}

	public String getEndDate()
	{
		return endDate;
	}

	public void setEndDate(String endDate)
	{

		this.endDate = endDate;

	}

	public String getOnline()
	{
		return online;
	}

	public void setOnline(String online)
	{

		this.online = online;

	}

	public String getOffline()
	{
		return offline;
	}

	public void setOffline(String offline)
	{

		this.offline = offline;

	}

	public String getInstock()
	{
		return instock;
	}

	public void setInstock(String instock)
	{

		this.instock = instock;

	}

	public String getCondition()
	{
		return condition;
	}

	public void setCondition(String condition)
	{

		this.condition = condition;

	}

	public String getWarranty()
	{
		return warranty;
	}

	public void setWarranty(String warranty)
	{

		this.warranty = warranty;

	}

	public String getShippingCost()
	{
		return shippingCost;
	}

	public void setShippingCost(String shippingCost)
	{

		this.shippingCost = shippingCost;

	}

	public String getCategory()
	{
		return category;
	}

	public void setCategory(String category)
	{

		this.category = category;

	}

	public String getSubCategory()
	{
		return subCategory;
	}

	public void setSubCategory(String subCategory)
	{

		this.subCategory = subCategory;

	}

}
