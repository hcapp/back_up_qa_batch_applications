package com.scansee.batch.common.pojos;

public class BatchProcessStatus
{

	private String BatchProcessName;
	private String executionDate;
	private String endTime;
	private String rowsRecieved;
	private String rowsProcessed;
	private String reason;
	private String status;
	private String processedFileName;
	private String duplicatesCount;
	private String expiredCount;
	private String mandatoryFieldsMissingCount;
	private String existingRecordCount;
	private String uPCLengthCount;
	private String invalidRecordsCount;
	public String getBatchProcessName()
	{
		return BatchProcessName;
	}
	public void setBatchProcessName(String batchProcessName)
	{
		BatchProcessName = batchProcessName;
	}

	public String getExecutionDate()
	{
		return executionDate;
	}
	public void setExecutionDate(String executionDate)
	{
		this.executionDate = executionDate;
	}
	public String getEndTime()
	{
		return endTime;
	}
	public void setEndTime(String endTime)
	{
		this.endTime = endTime;
	}
	public String getRowsRecieved()
	{
		return rowsRecieved;
	}
	public void setRowsRecieved(String rowsRecieved)
	{
		this.rowsRecieved = rowsRecieved;
	}
	public String getRowsProcessed()
	{
		return rowsProcessed;
	}
	public void setRowsProcessed(String rowsProcessed)
	{
		this.rowsProcessed = rowsProcessed;
	}
	
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public String getReason()
	{
		return reason;
	}
	public void setReason(String reason)
	{
		this.reason = reason;
	}
	public String getProcessedFileName()
	{
		return processedFileName;
	}
	public void setProcessedFileName(String processedFileName)
	{
		this.processedFileName = processedFileName;
	}
	public String getDuplicatesCount()
	{
		return duplicatesCount;
	}
	public void setDuplicatesCount(String duplicatesCount)
	{
		this.duplicatesCount = duplicatesCount;
	}
	public String getExpiredCount()
	{
		return expiredCount;
	}
	public void setExpiredCount(String expiredCount)
	{
		this.expiredCount = expiredCount;
	}
	public String getMandatoryFieldsMissingCount()
	{
		return mandatoryFieldsMissingCount;
	}
	public void setMandatoryFieldsMissingCount(String mandatoryFieldsMissingCount)
	{
		this.mandatoryFieldsMissingCount = mandatoryFieldsMissingCount;
	}
	public String getExistingRecordCount()
	{
		return existingRecordCount;
	}
	public void setExistingRecordCount(String existingRecordCount)
	{
		this.existingRecordCount = existingRecordCount;
	}
	public String getuPCLengthCount()
	{
		return uPCLengthCount;
	}
	public void setuPCLengthCount(String uPCLengthCount)
	{
		this.uPCLengthCount = uPCLengthCount;
	}
	public String getInvalidRecordsCount()
	{
		return invalidRecordsCount;
	}
	public void setInvalidRecordsCount(String invalidRecordsCount)
	{
		this.invalidRecordsCount = invalidRecordsCount;
	}
	
	
}
