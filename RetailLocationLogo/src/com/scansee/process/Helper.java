package com.scansee.process;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Helper
{
	public Properties test() {
		Properties props = null;
		try {
			InputStream is = this.getClass().getClassLoader().getResourceAsStream("application.properties");
			props = new Properties();
			props.load(is);
			System.out.println(props.getProperty("retailer.destination"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return props;
	}
	
	
	public String createFolderStructure(Properties props)
	{
		String filePath = props.getProperty("retailer.source");
		XSSFSheet sheet = null;
		String strRetImgName = null;
		try {
			FileInputStream fis = new FileInputStream(filePath);
			XSSFWorkbook wb_xssf = new XSSFWorkbook(fis);
			sheet = wb_xssf.getSheetAt(0);
			@SuppressWarnings("rawtypes")
			Iterator rowItr = sheet.rowIterator();
			while (rowItr.hasNext()) {
				XSSFRow row = (XSSFRow) rowItr.next();
				int rowNum = row.getRowNum();
				if(rowNum != 0) 
				{
					System.out.println("row number " + rowNum);
					String retailerId =  row.getCell(0).toString();
					XSSFCell retName = row.getCell(1);
					XSSFCell retImgName = row.getCell(2);
					long longRetailId = new Double(Double.parseDouble(retailerId)).longValue();
					String strRetailId = Long.toString(longRetailId);
					strRetImgName = retImgName.toString().substring(retImgName.toString().lastIndexOf("/") + 1);
					System.out.println(" Retailer name  retailer ID retailer Image name "+retName + strRetailId +retImgName);
					copyFile(strRetailId, retName.toString(), strRetImgName);
				}
			}
		}  catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	// method to create copy files

	public void copyFile(String retailerID, String name, String imageName)
	{
		Properties props = test();
		String destinationPath = props.getProperty("retailer.destination");
		String imagePath = props.getProperty("imagePath");
		String fileSeparator = System.getProperty("file.separator");
		StringBuilder mediaPathBuilder = new StringBuilder();
		mediaPathBuilder.append(destinationPath);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(retailerID);
		
		File obj = new File(mediaPathBuilder.toString());
		
		if (!obj.exists()) {
			System.out.println(" ::: Creating Retailer Directory ::"+mediaPathBuilder);
			obj.mkdir();
		} 

		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append("locationlogo");
		obj = new File(mediaPathBuilder.toString());
		
		if (!obj.exists()) {
			System.out.println(" ::: Creating Retailer Location Directory ::"+mediaPathBuilder);
			obj.mkdir();
		}
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(imageName);
		wrtieFile(imagePath, mediaPathBuilder);
	}
	

	public void wrtieFile(String imagePath, StringBuilder mediaPathBuilder)
	{
		File imageFile = new File(imagePath);
		FileInputStream imageStream = null;
		FileOutputStream imageOStream = null;
		try {
			imageStream = new FileInputStream(imageFile);
			imageOStream = new FileOutputStream(mediaPathBuilder.toString());
			byte[] buffer = new byte[32];
			while (imageStream.read(buffer) != -1) {
				imageOStream.write(buffer);
			}
			imageOStream.close();
			imageStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				imageOStream.close();
				imageStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
