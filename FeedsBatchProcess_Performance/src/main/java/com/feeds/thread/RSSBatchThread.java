package com.feeds.thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.feeds.common.Utility;
import com.feeds.service.FeedsServiceImpl;

public class RSSBatchThread implements Runnable{

	private String category;

	private String hubCitiId;

	private Thread thread;
	
	private int hubCitiType;
	private static Logger LOG = LoggerFactory.getLogger(FeedsServiceImpl.class.getName());
	
	public RSSBatchThread() {
		super();
	}

	public RSSBatchThread(String category, String hubCitiId,int hubCitiType) {
		super();
		this.category = category;
		this.hubCitiId = hubCitiId;
		this.hubCitiType = hubCitiType;
		this.thread = new Thread(this);
		this.thread.start();
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getHubCitiId() {
		return hubCitiId;
	}

	public void setHubCitiId(String hubCitiId) {
		this.hubCitiId = hubCitiId;
	}

	public Thread getThread() {
		return thread;
	}

	public void setThread(Thread thread) {
		this.thread = thread;
	}

	@Override
	public void run() {
		LOG.info("Thread Started Category >>>> : " + category + "    HubCitiId : " + hubCitiId);
		if(hubCitiType ==1){
			Utility.getTylerList(category,hubCitiId);
		}else if(hubCitiType ==2){
			Utility.getRockwallList(category,hubCitiId);
		}else if(hubCitiType ==3){
			Utility.getAustinList(category,hubCitiId);
		}else if(hubCitiType ==4){
			Utility.getKilleenList(category,hubCitiId);
		}
		LOG.info("Thread Ended Category   <<<< : " + category + "    HubCitiId : " + hubCitiId);
		
	}

}
