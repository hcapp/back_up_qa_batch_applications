package com.feeds.common;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.feeds.exception.RssFeedBatchProcessException;
import com.feeds.pojo.Item;
import com.feeds.service.FeedsServiceImpl;

//import org.apache.commons.lang.StringEscapeUtils;

public class Utility {

	private static Logger LOG = LoggerFactory
			.getLogger(Utility.class.getName());

	public static void getLatestClassifieldFromFtp(String categoryName,
			String hubcitiId) {

		LOG.info("Inside Utility getLatestClassifieldFromFtp START");
		FTPClient client = new FTPClient();

		ArrayList<FTPFile> ftpFiles = new ArrayList<FTPFile>();
		String downloadPath = null;
		FTPFile latestFile = null;
		String fileName = null;
		String server = PropertiesReader.getPropertyValue("ftpservername");
		int port = 21;
		String user = PropertiesReader.getPropertyValue("ftpuser");
		String pass = PropertiesReader.getPropertyValue("ftppassword");
		try {

			client.connect(server, port);
			client.login(user, pass);
			client.enterLocalPassiveMode();
			client.setFileType(FTP.BINARY_FILE_TYPE);
			client.changeWorkingDirectory("Classifields");
			System.out.println("22222222");

			FTPFile[] files = client.listFiles();

			for (FTPFile ftpFile : files) {

				String filename = ftpFile.getName();

				if (FilenameUtils.isExtension(filename, "xml")) {

					ftpFiles.add(ftpFile);
					System.out.println("333333333");

				}

			}

			// all the latest XML file
			if (ftpFiles != null & !ftpFiles.isEmpty()) {

				Date lastMod = ftpFiles.get(0).getTimestamp().getTime();
				latestFile = ftpFiles.get(0);

				for (FTPFile ftpFile : ftpFiles) {

					if (ftpFile.getTimestamp().getTime().after(lastMod)) {
						latestFile = ftpFile;

						lastMod = ftpFile.getTimestamp().getTime();
						System.out.println("4444444444");
					}
				}
				// Download the latest file local
				if (null != latestFile) {

					downloadPath = PropertiesReader
							.getPropertyValue("downloadpath");
					fileName = latestFile.getName();
					try {
						File downloadFile1 = new File(
								PropertiesReader
										.getPropertyValue("downloadpath")
										+ fileName);
						OutputStream outputStream1 = new BufferedOutputStream(
								new FileOutputStream(downloadFile1));

						String name = latestFile.getName();

						client.retrieveFile(name, outputStream1);

						outputStream1.close();

					} catch (IOException ex) {
						LOG.error("Inside Utility : getLatestClassifieldFromFTP : "
								+ ex.getMessage());

					} finally {
						try {
							if (client.isConnected()) {
								client.logout();
								client.disconnect();
							}
						} catch (IOException ex) {
							LOG.error("Inside Utility finally: getLatestClassifieldFromFTP : "
									+ ex.getMessage());
						}
						parseClassifield(downloadPath + fileName, categoryName,
								hubcitiId);
					}

				}

			}

		} catch (SocketException exception) {

			LOG.error("Inside Utility : getLatestClassifieldFromFTP : "
					+ exception.getMessage());

		} catch (IOException exception) {

			LOG.error("Inside Utility : getLatestClassifieldFromFTP : "
					+ exception.getMessage());

		} finally {
			try {
				client.disconnect();

			} catch (IOException exception) {

				LOG.error("Inside Utility : getLatestClassifieldFromFTP : "
						+ exception.getMessage());

			}
		}

		LOG.info("Exit Method getLatestClassifieldFromFTP ");

	}

	private static void parseClassifield(String filePath, String categoryname,
			String hubcitiId) {
		LOG.info("Inside Utility : parseClassifield START");

		List<Item> items = new ArrayList<Item>();
		final ApplicationContext context = new ClassPathXmlApplicationContext(
				"feeds-service.xml");
		final FeedsServiceImpl feedsService = (FeedsServiceImpl) context
				.getBean("feedsService");

		try {

			File file = new File(filePath);

			DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();

			Document doc = dBuilder.parse(file);

			doc.getDocumentElement().normalize();

			NodeList parentList = doc.getElementsByTagName("segment");

			for (int i = 0; i < parentList.getLength(); i++) {

				String adcopy = null;
				String classification = null;
				String section = null;

				Node nNode = parentList.item(i);

				Element eElement = (Element) nNode;

				try {

					if (eElement.getElementsByTagName("record") != null) {
						NodeList nList = eElement
								.getElementsByTagName("record");

						for (int temp = 0; temp < nList.getLength(); temp++) {

							Node nNodes = nList.item(temp);
							System.out.println("66666666666");
							Element eElements = (Element) nNodes;

							if (eElements.getElementsByTagName("field") != null) {

								NodeList media2 = eElements
										.getElementsByTagName("field");
								try {
									if (media2.item(7).getTextContent() != null) {
										classification = media2.item(7)
												.getTextContent();
									} else {
										classification = null;
									}
								} catch (Exception e) {
									LOG.error(
											"INSIDE UTILITY getLatestClassifieldFromFTP CLASSIFICATION: ",
											e.getMessage());
								}
								try {
									if (media2.item(6).getTextContent() != null) {
										section = media2.item(6)
												.getTextContent();
									} else {
										section = null;
									}
								} catch (Exception e) {
									LOG.error(
											"INSIDE UTILITY getLatestClassifieldFromFTP SECTION: ",
											e.getMessage());
								}

							}

						}

					}
				} catch (Exception e) {
					LOG.error(
							"INSIDE UTILITY getLatestClassifieldFromFTP RECORD : ",
							e.getMessage());
				}

				try {
					if (eElement.getElementsByTagName("adcopy") != null) {
						NodeList media1 = eElement
								.getElementsByTagName("adcopy");
						if (media1.item(0) != null
								&& media1.item(0).getTextContent() != null) {
							adcopy = eElement.getElementsByTagName("adcopy")
									.item(0).getTextContent();
						}
					}

					else {
						adcopy = null;
					}

				} catch (Exception e) {
					LOG.error("Inside Utility : parseClassifield ADCOPY: "
							+ e.getMessage());

				}

				Item item = new Item(null, null, null, null, null, null, null,
						adcopy, section, classification);
				item.setId(i + 1);
				items.add(item);

			}

			feedsService.processDatabaseOperation(items, categoryname,
					hubcitiId);

		} catch (Exception e) {
			LOG.error("Inside Utility : parseClassifield :  " + e.getMessage());
			Item item = new Item(null, null, null, null, null, null,
					e.getMessage(), null, null, null);
			items.add(item);

			try {
				feedsService.processDatabaseOperation(items, categoryname,
						hubcitiId);
			} catch (RssFeedBatchProcessException e1) {
				LOG.error("Inside Utility : parseClassifield :  "
						+ e.getMessage());
			}
		}

		LOG.info("Inside Utility : parseClassifield END");

	}

	/**
	 * Method to Check if the String object is null
	 * 
	 * @param strValue
	 *            String
	 * @return strValue String
	 */
	/*
	 * public static String checkNull(String strValue) { if (null == strValue ||
	 * "null".equals(strValue) || "".equals(strValue) ||
	 * "undefined".equals(strValue)) { return ""; } else { return
	 * strValue.trim(); } }
	 */

	/*
	 * public static ArrayList<Item> getEmployment(String name) {
	 * LOG.info("Inside Utility : getEmployment ");
	 * 
	 * ArrayList<Item> empItems = new ArrayList<Item>(); RssFeedServiceImpl
	 * service = new RssFeedServiceImpl(); URL url = null;
	 * 
	 * try {
	 * 
	 * if (name.equalsIgnoreCase("employment")) { url = new
	 * URL(CommonConstants.employment); }
	 * 
	 * DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	 * DocumentBuilder db = dbf.newDocumentBuilder(); if
	 * (url.openStream().available() > 0) { Document doc = db.parse(new
	 * InputSource(url.openStream())); doc.getDocumentElement().normalize();
	 * NodeList nodeList = doc.getElementsByTagName("item");
	 * 
	 * for (int i = 0; i < nodeList.getLength(); i++) {
	 * 
	 * String tit = null; String longDesc = null; String category = null; String
	 * link = null;
	 * 
	 * Node node = nodeList.item(i);
	 * 
	 * Element fstElmnt = (Element) node;
	 * 
	 * NodeList linkList = fstElmnt.getElementsByTagName("link"); Element
	 * linkElement = (Element) linkList.item(0); linkList =
	 * linkElement.getChildNodes(); if (validateURL(((Node)
	 * linkList.item(0)).getNodeValue())) { link = ((Node)
	 * linkList.item(0)).getNodeValue(); }
	 * 
	 * NodeList titleList = fstElmnt.getElementsByTagName("title"); Element
	 * titleElement = (Element) titleList.item(0); titleList =
	 * titleElement.getChildNodes();
	 * 
	 * tit = ((Node) titleList.item(0)).getNodeValue()
	 * .replaceAll("[^\\w\\s\'.,&:\"]", "") .replaceAll("&rsquo", "'")
	 * .replaceAll("&lsquo", "'") .replaceAll("&ldquo", "\"")
	 * .replaceAll("&rdquo", "\"") .replaceAll("&mdash", "--")
	 * .replaceAll("&ndash", "-").replaceAll(":", "");
	 * 
	 * NodeList websiteList = fstElmnt .getElementsByTagName("description");
	 * 
	 * Element websiteElement = (Element) websiteList.item(0); websiteList =
	 * websiteElement.getChildNodes(); longDesc = ((Node)
	 * websiteList.item(0)).getNodeValue() .replaceAll("&rsquo", "'")
	 * .replaceAll("&lsquo", "'") .replaceAll("&ldquo", "\"")
	 * .replaceAll("&rdquo", "\"") .replaceAll("&mdash", "--")
	 * .replaceAll("&ndash", "-") .replaceAll("([^\\w\\s\'.,-:&\"])", "");
	 *//**
	 * longDesc = websiteList.item(0).toString(); if (null != longDesc &&
	 * !"".equals(longDesc)) { longDesc = longDesc.replace("[#text:", "");
	 * longDesc = longDesc.substring(0, longDesc.length() - 1).trim(); }
	 * 
	 * @param hubCitiID
	 */
	/*
	 * 
	 * Item item = new Item(tit, longDesc, null, link, category, null);
	 * item.setId(i + 1); empItems.add(item); } }
	 * 
	 * service.processDatabaseOperation(empItems, name,
	 * CommonConstants.TylerHubCitiId); }
	 * 
	 * catch (Exception e) { LOG.info("Inside Utility : getEmployment  : " + e);
	 * }
	 * 
	 * LOG.info("Exit Utility : getEmployment "); return empItems; }
	 */

	public static void getPhotosList() {
		LOG.info("Inside Utility : getPhotosList");

		// URL url = null;

		String title = null;

		try {

			JSONObject json = readJsonFromUrl(CommonConstants.SMUGMUG_PHOTOS_URL);
			JSONObject json1 = json.getJSONObject("Response");

			JSONArray array = json1.getJSONArray("Album");
			for (int i = 0; i < array.length(); i++) {

				if (array.getJSONObject(i).getString("Title") != null) {
					title = array.getJSONObject(i).getString("Title")
							.replaceAll("([^\\w\\s\'.,-:&\"<>])", "")
							.replaceAll("&rsquo", "'")
							.replaceAll("&lsquo", "'")
							.replaceAll("&ldquo", "\"")
							.replaceAll("&rdquo", "\"")
							.replaceAll("&mdash", "--")
							.replaceAll("&ndash", "-").replaceAll(":", "");
					System.out.println("Title for photos" + title);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void getVideoList(String name, String hubCitiID) {
		LOG.info("Inside Utility : getVideoList");

		List<Item> videoItems = new ArrayList<Item>();
		final ApplicationContext context = new ClassPathXmlApplicationContext(
				"feeds-service.xml");
		final FeedsServiceImpl feedsService = (FeedsServiceImpl) context
				.getBean("feedsService");

		// URL url = null;

		String tit = null;
		String image = null;
		String shortDesc = null;
		try {
			if (name.equalsIgnoreCase("videos")) {

				JSONObject json = readJsonFromUrl(CommonConstants.VIDEOS_NEWS_URL);

				JSONArray array = json.getJSONArray("items");

				for (int i = 0; i < array.length(); i++) {
					if (array.getJSONObject(i).getString("name") != null) {
						tit = array.getJSONObject(i).getString("name")
								.replaceAll("([^\\w\\s\'.,-:&\"<>])", "")
								.replaceAll("&rsquo", "'")
								.replaceAll("&lsquo", "'")
								.replaceAll("&ldquo", "\"")
								.replaceAll("&rdquo", "\"")
								.replaceAll("&mdash", "--")
								.replaceAll("&ndash", "-").replaceAll(":", "");
					}

					if (array.getJSONObject(i).getString("videoStillURL") != null) {
						image = array.getJSONObject(i).getString(
								"videoStillURL");

					}

					String videoId = array.getJSONObject(i).getString("id");
					BigDecimal bd = new BigDecimal(videoId);

					if (!array.getJSONObject(i).getString("shortDescription")
							.equalsIgnoreCase("null")) {
						shortDesc = array.getJSONObject(i)
								.getString("shortDescription")
								.replaceAll("([^\\w\\s\'.,-:&\"<>])", "")
								.replaceAll("&rsquo", "'")
								.replaceAll("&lsquo", "'")
								.replaceAll("&ldquo", "\"")
								.replaceAll("&rdquo", "\"")
								.replaceAll("&mdash", "--")
								.replaceAll("&ndash", "-")
								.replaceAll("&39", "");

					} else {
						shortDesc = "";
					}

					// link="http://link.brightcove.com/services/player/bcpid3742235943001?bckey=AQ~~,AAACHKYdcFk~,IGxDpm7wjyHsFkDSEiehCuhZ0IVXD7vt"+"&bctid="+bd.longValue();
					final String link = getMP4RssFeedVideo(bd.longValue());

					Item item = new Item(tit, null, image, link, null,
							shortDesc,null, null);

					item.setId(i + 1);
					videoItems.add(item);
				}
			}

			feedsService.processDatabaseOperation(videoItems, name, hubCitiID);

		} catch (Exception e) {
			LOG.error("Inside Utility : getVideoList : " + e.getMessage());

			Item item = new Item(tit, null, null, null, null, null,null,
					e.getMessage());
			videoItems.add(item);

			try {
				feedsService.processDatabaseOperation(videoItems, name,
						CommonConstants.TYLERHUBCITIID);
			} catch (RssFeedBatchProcessException e1) {
				LOG.error("Inside Utility : getVideoList : " + e1.getMessage());
			}

		}

		LOG.info("Exit Utility : getVideoList ");

	}

	public static void getFeedList(String name, String hubCitiID) {
		LOG.info("Inside Utility : getFeedList ");

		List<Item> items = new ArrayList<Item>();
		final ApplicationContext context = new ClassPathXmlApplicationContext(
				"feeds-service.xml");
		final FeedsServiceImpl feedsService = (FeedsServiceImpl) context
				.getBean("feedsService");

		URL url = null;
		URL url1 = null;
		int iCount = 0;

		try {

			if (name.equalsIgnoreCase("top")) {

				url = new URL(CommonConstants.TOP_NEWS_URL);

				DocumentBuilderFactory dbf = DocumentBuilderFactory
						.newInstance();
				dbf.setCoalescing(true);
				DocumentBuilder db = dbf.newDocumentBuilder();

				if (url.openStream().available() > 0) {

					Document doc = db.parse(new InputSource(url.openStream()));
					doc.getDocumentElement().normalize();
					NodeList nodeList = doc.getElementsByTagName("item");

					for (int i = 0; i < nodeList.getLength(); i++) {

						String lDescription = null;
						String image = null;
						String link = null;
						String date = null;
						String sDescription = null;
						String title = null;

						Node node = nodeList.item(i);
						Element fstElmnt = (Element) node;

						try {

							if (fstElmnt.getElementsByTagName("media:content") != null) {

								NodeList media = fstElmnt
										.getElementsByTagName("media:content");
								if (media.item(0) != null
										&& media.item(0).getAttributes() != null
										&& media.item(0).getAttributes()
												.getNamedItem("url") != null) {
									String mediaurl = media.item(0)
											.getAttributes()
											.getNamedItem("url").getNodeValue();

									image = mediaurl;
								}

							} else {
								image = null;
							}

						} catch (Exception e) {
							LOG.error("Inside Utility : getFeedList  : "
									+ e.getMessage());

						}

						try {
							if (fstElmnt.getElementsByTagName("link") != null) {
								NodeList linkList = fstElmnt
										.getElementsByTagName("link");
								Element linkElement = (Element) linkList
										.item(0);
								linkList = linkElement.getChildNodes();

								link = ((Node) linkList.item(0)).getNodeValue();
							} else {
								link = null;
							}
						} catch (Exception e) {
							LOG.error("Inside Utility : getFeedList  : "
									+ e.getMessage());

						}
						try {
							if (fstElmnt.getElementsByTagName("title") != null) {
								NodeList titleList = fstElmnt
										.getElementsByTagName("title");
								Element titleElement = (Element) titleList
										.item(0);
								titleList = titleElement.getChildNodes();

								title = ((Node) titleList.item(0))
										.getNodeValue()
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"")
										.replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replaceAll("&mdash", "--")
										.replaceAll("&8211", "-")
										.replaceAll("&8216", "'")
										// left single
										// quotation mark
										.replaceAll("&8217", "'")
										// right single
										// quotation mark
										.replaceAll("&8230", "...")
										// horizontal
										// ellipsis
										.replaceAll("&ndash", "-")
										.replaceAll(":", "")
								        .replaceAll("&hellip", "...");
							} else {
								title = null;
							}
						} catch (Exception e) {
							LOG.error("Inside Utility : getFeedList  : "
									+ e.getMessage());

						}
						try {
							if (fstElmnt.getElementsByTagName("pubDate") != null) {
								NodeList dateList = fstElmnt
										.getElementsByTagName("pubDate");
								Element dateElement = (Element) dateList
										.item(0);
								dateList = dateElement.getChildNodes();
								String datee = ((Node) dateList.item(0))
										.getNodeValue();

								String dates[] = datee.split(" ");
								date = dates[2] + " " + dates[1] + " " + " , "
										+ dates[3];
							} else {
								date = null;
							}
						} catch (Exception e) {
							LOG.error("Inside Utility : getFeedList  : "
									+ e.getMessage());

						}
						try {
							if (fstElmnt.getElementsByTagName("media:text") != null) {
								NodeList websiteList = fstElmnt
										.getElementsByTagName("media:text");
								Element websiteElement = (Element) websiteList
										.item(0);
								websiteList = websiteElement.getChildNodes();

								if (null != websiteList.item(0)) {
									if (websiteList.item(0).toString() != null) {

										if (((Node) websiteList.item(0))
												.getNodeValue().length() > 75) {
											sDescription = ((Node) websiteList
													.item(0))
													.getNodeValue()
													.replaceAll(
															"([^\\w\\s\'.,-:&\"<>])",
															"")
													.replaceAll("&rsquo", "'")
													.replaceAll("&lsquo", "'")
													.replaceAll("&ldquo", "\"")
													.replaceAll("&rdquo", "\"")
													.replaceAll("&mdash", "--")
													.replaceAll("&ndash", "-")
													.replaceAll("&8211", "-")
													.replaceAll("&8216", "'")
													// left single
													// quotation
													// mark
													.replaceAll("&8217", "'")
													// right
													// single
													// quotation
													// mark
													.replaceAll("&8230", "...")
													// horizontal
													// ellipsis
													.replaceAll("&39", "")
													.replaceAll("&hellip", "...")
													.substring(0, 75)
													+ "...";
										} else {

											sDescription = ((Node) websiteList
													.item(0))
													.getNodeValue()
													.replaceAll(
															"([^\\w\\s\'.,-:&\"<>])",
															"")
													.replaceAll("&rsquo", "'")
													.replaceAll("&lsquo", "'")
													.replaceAll("&ldquo", "\"")
													.replaceAll("&rdquo", "\"")
													.replaceAll("&mdash", "--")
													.replaceAll("&ndash", "-")
													.replaceAll("&8211", "-")
													.replaceAll("&8216", "'") // left
																				// single
													// quotation
													// mark
													.replaceAll("&8217", "'") // right
													// single
													// quotation
													// mark
													.replaceAll("&8230", "...") // horizontal
													// ellipsis
													.replaceAll("&39", "")
													 .replaceAll("&hellip", "...")
													+ "...";
										}

										// lDescription =
										// websiteList.item(0).toString();
										lDescription = websiteList.item(0)
												.getNodeValue().toString();

										if (null != lDescription
												&& !"".equals(lDescription)) {
											lDescription = lDescription
													.replace("[#text:", "");
											lDescription = lDescription
													.substring(
															0,
															lDescription
																	.length() - 1)
													.trim()
													.replaceAll(
															"([^\\w\\s\'.,-:&\"<>])",
															"")
													.replaceAll("&rsquo", "'")
													.replaceAll("&lsquo", "'")
													.replaceAll("&ldquo", "\"")
													.replaceAll("&rdquo", "\"")
													.replace("&39", "'")
													.replaceAll("&8211", "-")
													.replaceAll("&8216", "'")
													// left single quotation
													// mark
													.replaceAll("&8217", "'")
													// right single quotation
													// mark
													.replaceAll("&8230", "...")
													// horizontal ellipsis
													.replaceAll("&mdash", "--")
													 .replaceAll("&hellip", "...")
													.replaceAll("&ndash", "-");
											
										}
									}

									Item item = new Item(title, lDescription,
											image, link, date, sDescription,null,
											null);

									item.setId(iCount++);
									items.add(item);
								}

							} else {
								lDescription = "";
								sDescription = "";
							}

						} catch (Exception e) {
							LOG.error("Inside Utility : getFeedList  : "
									+ e.getMessage());

						}
					}
				}
				
				
			} else if (name.equalsIgnoreCase("breaking")) {
				
				url1 = new URL(CommonConstants.BREAKING_NEWS_URL);

				DocumentBuilderFactory dbf1 = DocumentBuilderFactory
						.newInstance();
				dbf1.setCoalescing(true);
				DocumentBuilder db1 = dbf1.newDocumentBuilder();

				if (url1.openStream().available() > 0) {

					Document doc = db1
							.parse(new InputSource(url1.openStream()));
					doc.getDocumentElement().normalize();
					NodeList nodeList = doc.getElementsByTagName("item");

					for (int i = 0; i < nodeList.getLength(); i++) {

						String title = null;
						String lDescription = null;
						String image = null;
						String link = null;
						String date = null;
						String sDescription = null;

						Node node = nodeList.item(i);

						Element fstElmnt = (Element) node;

						try {

							if (fstElmnt.getElementsByTagName("media:content") != null) {

								NodeList media = fstElmnt
										.getElementsByTagName("media:content");
								if (media.item(0) != null
										&& media.item(0).getAttributes() != null
										&& media.item(0).getAttributes()
												.getNamedItem("url") != null) {
									String mediaurl = media.item(0)
											.getAttributes()
											.getNamedItem("url").getNodeValue();

									image = mediaurl;
								}

							} else {
								image = null;
							}

						} catch (Exception e) {
							LOG.error("Inside Utility : getFeedList  : "
									+ e.getMessage());

						}
						try {

							if (fstElmnt.getElementsByTagName("link") != null) {

								NodeList linkList = fstElmnt
										.getElementsByTagName("link");
								Element linkElement = (Element) linkList
										.item(0);
								linkList = linkElement.getChildNodes();

								link = ((Node) linkList.item(0)).getNodeValue();
							} else {
								link = null;
							}
						} catch (Exception e) {
							LOG.error("Inside Utility : getFeedList  : "
									+ e.getMessage());

						}
						try {

							if (fstElmnt.getElementsByTagName("title") != null) {

								NodeList titleList = fstElmnt
										.getElementsByTagName("title");
								Element titleElement = (Element) titleList
										.item(0);
								titleList = titleElement.getChildNodes();

								title = ((Node) titleList.item(0))
										.getNodeValue()
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"")
										.replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replaceAll("&mdash", "--")
										.replaceAll("&8211", "-")
										.replaceAll("&8216", "'")
										// left single
										// quotation mark
										.replaceAll("&8217", "'")
										// right single
										// quotation mark
										.replaceAll("&8230", "...")
										// horizontal
										// ellipsis
										.replaceAll("&ndash", "-")
										.replaceAll(":", "")
										 .replaceAll("&hellip", "...");
							} else {
								title = null;
							}
						} catch (Exception e) {
							LOG.error("Inside Utility : getFeedList  : "
									+ e.getMessage());

						}
						try {
							if (fstElmnt.getElementsByTagName("pubDate") != null) {
								NodeList dateList = fstElmnt
										.getElementsByTagName("pubDate");
								Element dateElement = (Element) dateList
										.item(0);
								dateList = dateElement.getChildNodes();
								String datee = ((Node) dateList.item(0))
										.getNodeValue();

								String dates[] = datee.split(" ");
								date = dates[2] + " " + dates[1] + " " + " , "
										+ dates[3];
							} else {
								date = null;
							}
						} catch (Exception e) {
							LOG.error("Inside Utility : getFeedList  : "
									+ e.getMessage());

						}
						try {
							if (fstElmnt.getElementsByTagName("media:text") != null) {
								NodeList websiteList = fstElmnt
										.getElementsByTagName("media:text");
								Element websiteElement = (Element) websiteList
										.item(0);
								websiteList = websiteElement.getChildNodes();

								if (null != websiteList.item(0)) {
									if (websiteList.item(0).toString() != null) {

										if (((Node) websiteList.item(0))
												.getNodeValue().length() > 75) {
											sDescription = ((Node) websiteList
													.item(0))
													.getNodeValue()
													.replaceAll(
															"([^\\w\\s\'.,-:&\"<>])",
															"")
													.replaceAll("&rsquo", "'")
													.replaceAll("&lsquo", "'")
													.replaceAll("&ldquo", "\"")
													.replaceAll("&rdquo", "\"")
													.replaceAll("&mdash", "--")
													.replaceAll("&ndash", "-")
													.replaceAll("&8211", "-")
													.replaceAll("&8216", "'")
													// left single
													// quotation
													// mark
													.replaceAll("&8217", "'")
													// right
													// single
													// quotation
													// mark
													.replaceAll("&8230", "...")
													// horizontal
													// ellipsis
													.replaceAll("&39", "")
													 .replaceAll("&hellip", "...")
													.substring(0, 75)
													+ "...";
										} else {

											sDescription = ((Node) websiteList
													.item(0))
													.getNodeValue()
													.replaceAll(
															"([^\\w\\s\'.,-:&\"<>])",
															"")
													.replaceAll("&rsquo", "'")
													.replaceAll("&lsquo", "'")
													.replaceAll("&ldquo", "\"")
													.replaceAll("&rdquo", "\"")
													.replaceAll("&mdash", "--")
													.replaceAll("&ndash", "-")
													.replaceAll("&8211", "-")
													.replaceAll("&8216", "'") // left
																				// single
													// quotation
													// mark
													.replaceAll("&8217", "'") // right
													// single
													// quotation
													// mark
													.replaceAll("&8230", "...") // horizontal
													// ellipsis
													.replaceAll("&39", "")
													 .replaceAll("&hellip", "...")
													+ "...";
										}

										// lDescription =
										// websiteList.item(0).toString();
										lDescription = websiteList.item(0)
												.getNodeValue().toString();

										if (null != lDescription
												&& !"".equals(lDescription)) {
											lDescription = lDescription
													.replace("[#text:", "");
											lDescription = lDescription
													.substring(
															0,
															lDescription
																	.length() - 1)
													.trim()
													.replaceAll(
															"([^\\w\\s\'.,-:&\"<>])",
															"")
													.replaceAll("&rsquo", "'")
													.replaceAll("&lsquo", "'")
													.replaceAll("&ldquo", "\"")
													.replaceAll("&rdquo", "\"")
													.replace("&39", "'")
													.replaceAll("&8211", "-")
													.replaceAll("&8216", "'")
													// left single quotation
													// mark
													.replaceAll("&8217", "'")
													// right single quotation
													// mark
													.replaceAll("&8230", "...")
													// horizontal ellipsis
													.replaceAll("&mdash", "--")
													.replaceAll("&ndash", "-")
													 .replaceAll("&hellip", "...");
										}

										Item item = new Item(title,
												lDescription, image, link,
												date, sDescription,null, null);

										item.setId(iCount++);
										items.add(item);
									}

								} else {
									lDescription = "";
									sDescription = "";
								}
							}
						} catch (Exception e) {
							LOG.error("Inside Utility : getFeedList  : "
									+ e.getMessage());

						}
					}
				}
				
				
				

			}

			feedsService.processDatabaseOperation(items, name, hubCitiID);

		} catch (Exception e) {
			LOG.error("Inside Utility : getFeedList  : " + e.getMessage());
			Item item = new Item(null, null, null, null, null, null,null,
					e.getMessage());
			items.add(item);
			try {
				feedsService.processDatabaseOperation(items, name, hubCitiID);
			} catch (RssFeedBatchProcessException e1) {
				LOG.error("Inside Utility : getFeedList  : " + e1.getMessage());
			}
		}

		LOG.info("Exit Utility : getFeedList ");

	}

	// Added for Rockwall

	public static void getRockwallList(String name, String hubCitiID) {
		LOG.info("Inside Utility : getRockwallList ");

		List<Item> items = new ArrayList<Item>();
		final ApplicationContext context = new ClassPathXmlApplicationContext(
				"feeds-service.xml");
		final FeedsServiceImpl feedsService = (FeedsServiceImpl) context
				.getBean("feedsService");

		URL url = null;

		try {

			/*
			 * switch (name) { case "good times": url = new
			 * URL(CommonConstants.GOODEVENTS_NEWS_URL); break;
			 * 
			 * case "good cause": url = new
			 * URL(CommonConstants.GOODCAUSE_NEWS_URL); break;
			 * 
			 * case "good living": url = new
			 * URL(CommonConstants.GOODLIVING_NEWS_URL); break;
			 * 
			 * case "good culture": url = new
			 * URL(CommonConstants.GOODCULTURE_NEWS_URL); break;
			 * 
			 * case "good people": url = new
			 * URL(CommonConstants.GOODPEOPLE_NEWS_URL); break;
			 * 
			 * case "good pets": url = new
			 * URL(CommonConstants.GOODPETS_NEWS_URL); break;
			 * 
			 * case "good faith": url = new
			 * URL(CommonConstants.GOODFAITH_NEWS_URL); break;
			 * 
			 * case "good thinking": url = new
			 * URL(CommonConstants.GOODTHINKING_NEWS_URL); break;
			 * 
			 * case "good business": url = new
			 * URL(CommonConstants.GOODBUSINESS_NEWS_URL); break;
			 * 
			 * case "good neighbours": url = new
			 * URL(CommonConstants.GOODNEIGHBOURS_NEWS_URL); break;
			 * 
			 * case "good health": url = new
			 * URL(CommonConstants.GOODNEIGHBOURS_NEWS_URL); break;
			 * 
			 * case "columnists": url = new
			 * URL(CommonConstants.GUESTCOLUMNS_NEWS_URL); break;
			 * 
			 * case "good sports": url = new
			 * URL(CommonConstants.GOODSPORTS_NEWS_URL); break;
			 * 
			 * default: url = null; }
			 */

			if (name.equalsIgnoreCase("good times")) {
				url = new URL(CommonConstants.GOODEVENTS_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good cause")) {
				url = new URL(CommonConstants.GOODCAUSE_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good living")) {
				url = new URL(CommonConstants.GOODLIVING_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good business")) {
				url = new URL(CommonConstants.GOODBUSINESS_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good people")) {
				url = new URL(CommonConstants.GOODPEOPLE_NEWS_URL);

			}

			else if (name.equalsIgnoreCase("good pets")) {
				url = new URL(CommonConstants.GOODPETS_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good faith")) {
				url = new URL(CommonConstants.GOODFAITH_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good thinking")) {
				url = new URL(CommonConstants.GOODTHINKING_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good health")) {
				url = new URL(CommonConstants.GOODNEIGHBOURS_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good culture")) {
				url = new URL(CommonConstants.GOODCULTURE_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("good sports")) {
				url = new URL(CommonConstants.GOODSPORTS_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("columnists")) {
				url = new URL(CommonConstants.GUESTCOLUMNS_NEWS_URL);
			}

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setCoalescing(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			if (url.openStream().available() > 0) {
				Document doc = db.parse(new InputSource(url.openStream()));
				doc.getDocumentElement().normalize();
				NodeList nodeList = doc.getElementsByTagName("item");

				for (int i = 0; i < nodeList.getLength(); i++) {

					String title = null;
					String lDescription = null;
					String image = null;
					String link = null;
					String date = null;
					String sDescription = null;

					Node node = nodeList.item(i);

					Element fstElmnt = (Element) node;

					try {

						if (fstElmnt.getElementsByTagName("media:content") != null) {

							NodeList media = fstElmnt
									.getElementsByTagName("media:content");
							if (media.item(0) != null
									&& media.item(0).getAttributes() != null
									&& media.item(0).getAttributes()
											.getNamedItem("url") != null) {
								String mediaurl = media.item(0).getAttributes()
										.getNamedItem("url").getNodeValue();

								// Adding changes
								int dot = mediaurl.lastIndexOf(".");
								int index = mediaurl.lastIndexOf("-");
								image = mediaurl.substring(0, index)
										+ mediaurl.substring(dot);
								// image = mediaurl;
							}

						} else {
							image = null;
						}

					} catch (Exception e) {
						LOG.error("Inside Utility : getRockwallList :  "
								+ e.getMessage());

					}

					try {

						if (fstElmnt.getElementsByTagName("link") != null) {

							NodeList linkList = fstElmnt
									.getElementsByTagName("link");
							Element linkElement = (Element) linkList.item(0);
							linkList = linkElement.getChildNodes();

							link = ((Node) linkList.item(0)).getNodeValue();

						} else {
							link = null;
						}

					} catch (Exception e) {
						LOG.error("Inside Utility : getRockwallList :  "
								+ e.getMessage());

					}
					try {
						if (fstElmnt.getElementsByTagName("title") != null) {

							NodeList titleList = fstElmnt
									.getElementsByTagName("title");
							Element titleElement = (Element) titleList.item(0);
							titleList = titleElement.getChildNodes();

							title = ((Node) titleList.item(0)).getNodeValue()

							.replaceAll("([^\\w\\s\'.,-:&\"<>])", "")
									.replaceAll("&rsquo", "'")
									.replaceAll("&lsquo", "'")
									.replaceAll("&ldquo", "\"")
									.replaceAll("&rdquo", "\"")
									.replaceAll("&mdash", "--")
									.replaceAll("&ndash", "-")
									.replaceAll("&8211", "-")
									.replaceAll("&8216", "'") // left single
																// quotation
																// mark
									.replaceAll("&8217", "'") // right single
																// quotation
																// mark
									.replaceAll("&8230", "...") // horizontal
																// ellipsis
									.replaceAll("&38", "&") // for ampersand.
									.replaceAll("&8220", "\"")// left double
																// quotation
																// mark
									.replaceAll("&8221", "\"") // right double
																// quotation
																// mark
                                     .replaceAll("&hellip", "...")
									.replaceAll(":", "");

							// title = StringEscapeUtils.unescapeHtml(title);
						} else {
							title = null;
						}

					} catch (Exception e) {
						LOG.error("Inside Utility : getRockwallList :  "
								+ e.getMessage());

					}

					try {
						if (fstElmnt.getElementsByTagName("pubDate") != null) {

							NodeList dateList = fstElmnt
									.getElementsByTagName("pubDate");
							Element dateElement = (Element) dateList.item(0);
							dateList = dateElement.getChildNodes();
							String datee = ((Node) dateList.item(0))
									.getNodeValue();
							// date=getDate(datee);
							String dates[] = datee.split(" ");
							date = dates[2] + " " + dates[1] + " " + " , "
									+ dates[3];

						} else {
							date = null;
						}

					} catch (Exception e) {
						LOG.error("Inside Utility : getRockwallList :  "
								+ e.getMessage());

					}

					NodeList websiteList = fstElmnt
							.getElementsByTagName("description");

					Element websiteElement = (Element) websiteList.item(0);
					websiteList = websiteElement.getChildNodes();
					if (null != websiteList.item(0)) {

						if (websiteList.item(0).toString() != null) {
							if (((Node) websiteList.item(0)).getNodeValue()
									.length() > 75) {
								sDescription = ((Node) websiteList.item(0))
										.getNodeValue()
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"").replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replaceAll("&mdash", "--")
										.replaceAll("&ndash", "-")
										.replaceAll("&39", "")
										.replaceAll("&8211", ".")
										.replaceAll("&8216", "'") // left single
																	// quotation
																	// mark
										.replaceAll("&8217", "'") // right
																	// single
																	// quotation
																	// mark
										.replaceAll("&8230", "...") // horizontal
																	// ellipsis
										.replaceAll("&38", "&") // for
																// ampersand.
										.replaceAll("&8220", "\"")// left double
																	// quotation
																	// mark
										.replaceAll("&8221", "\"")
										 .replaceAll("&hellip", "...")
										
										// right
																	// double
																	// quotation
																	// mark
										.substring(0, 75)
										+ "...";
								// sDescription = StringEscapeUtils
								// .unescapeHtml(sDescription);
							} else {

								sDescription = ((Node) websiteList.item(0))
										.getNodeValue()
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"").replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replaceAll("&mdash", "--")
										.replaceAll("&ndash", "-")
										.replaceAll("&8211", ".")
										.replaceAll("&8216", "'")
										// left single quotation mark
										.replaceAll("&8217", "'")
										// right single quotation mark
										.replaceAll("&8230", "...")
										// horizontal ellipsis
										.replaceAll("&39", "")
										.replaceAll("&38", "&") // for
																// ampersand.
										.replaceAll("&8220", "\"")// left double
																	// quotation
																	// mark
										.replaceAll("&8221", "\"") // right
																	// double
																	// quotation
																	// mark
										 .replaceAll("&hellip", "...")
										+ "...";
								// sDescription = StringEscapeUtils
								// .unescapeHtml(sDescription);
							}
							lDescription = websiteList.item(0).toString();

							if (null != lDescription
									&& !"".equals(lDescription)) {
								lDescription = lDescription.replace("[#text:",
										"");
								lDescription = lDescription
										.substring(0, lDescription.length() - 1)
										.trim()
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"")
										.replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replace("&39", "'")
										.replaceAll("&8211", ".")
										.replaceAll("&8216", "'")
										// left single quotation mark
										.replaceAll("&8217", "'")
										// right single quotation mark
										.replaceAll("&8230", "...")
										// horizontal ellipsis
										.replaceAll("&mdash", "--")
										.replaceAll("&ndash", "-")
										.replaceAll("&38", "&") // for
																// ampersand.
										.replaceAll("&8220", "\"")// left double
																	// quotation
																	// mark
										.replaceAll("&8221", "\"")
								 .replaceAll("&hellip", "...");
								// right
																	// double
																	// quotation
																	// mark
								// lDescription = StringEscapeUtils
								// .unescapeHtml(lDescription);
							}

							Item item = new Item(title, lDescription, image,
									link, date, sDescription,null, null);

							item.setId(i + 1);
							items.add(item);

						}
					}
				}

				feedsService.processDatabaseOperation(items, name, hubCitiID);
			}

		} catch (Exception e) {
			LOG.error("Inside Utility : getRockwallList :  " + e.getMessage());
			Item item = new Item(null, null, null, null, null, null,null,
					e.getMessage());
			items.add(item);

			try {
				feedsService.processDatabaseOperation(items, name, hubCitiID);
			} catch (RssFeedBatchProcessException e1) {
				LOG.error("Inside Utility : getRockwallList :  "
						+ e.getMessage());
			}
		}

		LOG.info("Exit Utility : getRockwallList ");

	}

	public static void getTylerList(String name, String hubCitiID) {
		LOG.info("Inside Utility : getTylerList ");

		List<Item> items = new ArrayList<Item>();
		final ApplicationContext context = new ClassPathXmlApplicationContext(
				"feeds-service.xml");
		final FeedsServiceImpl feedsService = (FeedsServiceImpl) context
				.getBean("feedsService");

		URL url = null;

		try {

			/*
			 * switch (name) { case "sports": url = new
			 * URL(CommonConstants.SPORTS_NEWS_URL); break;
			 * 
			 * case "health": url = new URL(CommonConstants.HEALTH_NEWS_URL);
			 * break;
			 * 
			 * case "business": url = new
			 * URL(CommonConstants.BUSINESS_NEWS_URL); break;
			 * 
			 * case "top": url = new URL(CommonConstants.TOP_NEWS_URL); break;
			 * 
			 * case "life": url = new URL(CommonConstants.LIFE_NEWS_URL); break;
			 * 
			 * case "opinion": url = new
			 * URL(CommonConstants.EDITORIAL_NEWS_URL); break;
			 * 
			 * case "entertainment": url = new
			 * URL(CommonConstants.ENTERTAINMENT_NEWS_URL); break;
			 * 
			 * case "food": url = new URL(CommonConstants.FOOD_NEWS_URL); break;
			 * 
			 * case "weather": url = new
			 * URL(CommonConstants.WEATHERTYLER_NEWS_URL); break;
			 * 
			 * case "all": url = new URL(CommonConstants.ALLARTICLES_NEWS_URL);
			 * break;
			 * 
			 * case "videos": getVideoList("videos", hubCitiID); break;
			 * 
			 * case "photos":
			 * PhotosAPI.readJsonFromUrl(CommonConstants.SMUGMUG_PHOTOS_URL,
			 * "photos", hubCitiID); break;
			 * 
			 * default: url = null; }
			 */

			if (name.equalsIgnoreCase("sports")) {
				url = new URL(CommonConstants.SPORTS_NEWS_URL);
			} else if (name.equalsIgnoreCase("food")) {
				url = new URL(CommonConstants.FOOD_NEWS_URL);
			} else if (name.equalsIgnoreCase("health")) {
				url = new URL(CommonConstants.HEALTH_NEWS_URL);
			} else if (name.equalsIgnoreCase("business")) {
				url = new URL(CommonConstants.BUSINESS_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("top")) {
				getFeedList("top", hubCitiID);
			} else if (name.equalsIgnoreCase("breaking")) {
				getFeedList("breaking", hubCitiID);
			}

			else if (name.equalsIgnoreCase("life")) {
				url = new URL(CommonConstants.LIFE_NEWS_URL);
			} else if (name.equalsIgnoreCase("opinion")) {
				url = new URL(CommonConstants.EDITORIAL_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("photos")) {
				PhotosAPI.readJsonFromUrl(CommonConstants.SMUGMUG_PHOTOS_URL,
						name, hubCitiID);
			}

			else if (name.equalsIgnoreCase("entertainment")) {
				url = new URL(CommonConstants.ENTERTAINMENT_NEWS_URL);
			} else if (name.equalsIgnoreCase("weather")) {
				url = new URL(CommonConstants.WEATHERTYLER_NEWS_URL);
			} else if (name.equalsIgnoreCase("food")) {
				url = new URL(CommonConstants.FOOD_NEWS_URL);
			} else if (name.equalsIgnoreCase("all")) {
				url = new URL(CommonConstants.ALLARTICLES_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("videos")) {
				getVideoList(name, hubCitiID);
			}

			else if (name.equalsIgnoreCase("Classifields")) {
				Utility.getLatestClassifieldFromFtp("Classifields", hubCitiID);
			}

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setCoalescing(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			if (url.openStream().available() > 0) {
				Document doc = db.parse(new InputSource(url.openStream()));
				doc.getDocumentElement().normalize();
				NodeList nodeList = doc.getElementsByTagName("item");

				for (int i = 0; i < nodeList.getLength(); i++) {

					String title = null;
					String lDescription = null;
					String image = null;
					String link = null;
					String date = null;
					String sDescription = null;
					String mediaLink=null;

					Node node = nodeList.item(i);

					Element fstElmnt = (Element) node;

					try {

						if (fstElmnt.getElementsByTagName("media:content") != null) {

							NodeList media = fstElmnt
									.getElementsByTagName("media:content");
							if (media.item(0) != null
									&& media.item(0).getAttributes() != null
									&& media.item(0).getAttributes()
											.getNamedItem("url") != null) {
								String mediaurl = media.item(0).getAttributes()
										.getNamedItem("url").getNodeValue();

								image = mediaurl;
							}

						} else {
							image = null;
						}

					} catch (Exception e) {
						LOG.error("imageurl exception", image);
						e.printStackTrace();
					}

					try {

						if (fstElmnt.getElementsByTagName("link") != null) {
							NodeList linkList = fstElmnt
									.getElementsByTagName("link");
							Element linkElement = (Element) linkList.item(0);
							linkList = linkElement.getChildNodes();

							link = ((Node) linkList.item(0)).getNodeValue();

							NodeList titleList = fstElmnt
									.getElementsByTagName("title");
							Element titleElement = (Element) titleList.item(0);
							titleList = titleElement.getChildNodes();

							title = ((Node) titleList.item(0)).getNodeValue()
									.replaceAll("([^\\w\\s\'.,-:&\"<>])", "")
									.replaceAll("&rsquo", "'")
									.replaceAll("&lsquo", "'")
									.replaceAll("&ldquo", "\"")
									.replaceAll("&rdquo", "\"")
									.replaceAll("&mdash", "--")
									.replaceAll("&ndash", "-")
									.replaceAll("&8211", "-")
									.replaceAll("&8216", "'") // left
																// single
																// quotation
																// mark
									.replaceAll("&8217", "'") // right single
																// quotation
																// mark
									.replaceAll("&8230", "...")
									 .replaceAll("&hellip", "...")
									
									// horizontal
																// ellipsis
									.replaceAll(":", "");
						} else {
							title = null;
						}

					} catch (Exception e) {
						LOG.error("Inside Utility : getTylerList  : "
								+ e.getMessage());

					}
					try {
						if (fstElmnt.getElementsByTagName("pubDate") != null) {

							NodeList dateList = fstElmnt
									.getElementsByTagName("pubDate");
							Element dateElement = (Element) dateList.item(0);
							dateList = dateElement.getChildNodes();
							String datee = ((Node) dateList.item(0))
									.getNodeValue();
							// date=getDate(datee);
							String dates[] = datee.split(" ");
							date = dates[2] + " " + dates[1] + " " + " , "
									+ dates[3];
						} else {
							date = null;
						}
					} catch (Exception e) {
						LOG.error("Inside Utility : getTylerList  : "
								+ e.getMessage());

					}
					
					try{
						
						Node videoLink= fstElmnt.getElementsByTagName("media:text").item(1);
						if(videoLink!= null){
							if (videoLink!= null
									&& videoLink.getAttributes() != null
									&& videoLink.getAttributes()
											.getNamedItem("type") != null) {
								String value = videoLink.getAttributes()
										.getNamedItem("type").getNodeValue();
								if(value.equalsIgnoreCase("video")){

									NodeList linkList = fstElmnt
											.getElementsByTagName("media:text");
									Element linkElement = (Element) linkList.item(1);
									linkList = linkElement.getChildNodes();

									String nodeValue = ((Node) linkList.item(0)).getNodeValue();
									
	                              
									if (null != nodeValue
											&& !"".equals(nodeValue)) {
										mediaLink = nodeValue.replace(
												"[#text:", "");
								
									/*try{
										String pattern=".*videoPlayer\"\\s+value=\"(\\d+).*";
										
			Pattern ptn = Pattern.compile(pattern);
			Matcher matcher=ptn.matcher(media);
			 while(matcher.find()) {
		        String videoId=  matcher.group(1);
		        BigDecimal bd = new BigDecimal(videoId);
		        mediaLink = getMP4RssFeedVideo(bd.longValue());
		        
		        //mediaLink=CommonConstants.BRIGHTCOVE_VIDEOS_URL+videoId;
		       
			 }
		        
		         
		        
			
									}catch(Exception e){
										LOG.error("Inside Utility : getTylerList  : "
												+ e.getMessage());
									}*/
										
										
		  
										
									}
								
								}

								
							}
						}
					}catch(Exception e){
						LOG.error("Inside Utility : getTylerList  : "
								+ e.getMessage());
					}

					try {

						if (fstElmnt.getElementsByTagName("media:text") != null) {

							NodeList websiteList = fstElmnt
									.getElementsByTagName("media:text");
							Element websiteElement = (Element) websiteList
									.item(0);
							websiteList = websiteElement.getChildNodes();

							if (null != websiteList.item(0)) {

								if (websiteList.item(0).toString() != null) {
									if (((Node) websiteList.item(0))
											.getNodeValue().length() > 75) {
										sDescription = ((Node) websiteList
												.item(0))
												.getNodeValue()
												.replaceAll(
														"([^\\w\\s\'.,-:&\"<>])",
														"")
												.replaceAll("&rsquo", "'")
												.replaceAll("&lsquo", "'")
												.replaceAll("&ldquo", "\"")
												.replaceAll("&rdquo", "\"")
												.replaceAll("&mdash", "--")
												.replaceAll("&ndash", "-")
												.replaceAll("&8211", "-")
												.replaceAll("&8216", "'")
												// left single
												// quotation
												// mark
												.replaceAll("&8217", "'")
												// right
												// single
												// quotation
												// mark
												.replaceAll("&8230", "...")
												 .replaceAll("&hellip", "...")
												// horizontal
												// ellipsis
												.replaceAll("&39", "")
												.substring(0, 75)
												+ "...";
									} else {

										sDescription = ((Node) websiteList
												.item(0))
												.getNodeValue()
												.replaceAll(
														"([^\\w\\s\'.,-:&\"<>])",
														"")
												.replaceAll("&rsquo", "'")
												.replaceAll("&lsquo", "'")
												.replaceAll("&ldquo", "\"")
												.replaceAll("&rdquo", "\"")
												.replaceAll("&mdash", "--")
												.replaceAll("&ndash", "-")
												.replaceAll("&8211", "-")
												.replaceAll("&8216", "'") // left
																			// single
																			// quotation
																			// mark
												.replaceAll("&8217", "'") // right
																			// single
																			// quotation
																			// mark
												.replaceAll("&8230", "...") // horizontal
																			// ellipsis
												.replaceAll("&39", "")
												 .replaceAll("&hellip", "...")
												+ "...";
									}
									lDescription = websiteList.item(0)
											.toString();

									if (null != lDescription
											&& !"".equals(lDescription)) {
										lDescription = lDescription.replace(
												"[#text:", "");
										lDescription = lDescription
												.substring(
														0,
														lDescription.length() - 1)
												.trim()
												.replaceAll(
														"([^\\w\\s\'.,-:&=?()\"<>])",
														"")
												.replaceAll("&rsquo", "'")
												.replaceAll("&lsquo", "'")
												.replaceAll("&ldquo", "\"")
												.replaceAll("&rdquo", "\"")
												.replace("&39", "'")
												.replaceAll("&8211", "-")
												.replaceAll("&8216", "'")
												// left singgetle quotation mark
												.replaceAll("&8217", "'")
												// right single quotation mark
												.replaceAll("&8230", "...")
												 .replaceAll("&hellip", "...")
												// horizontal ellipsis
												.replaceAll("&mdash", "--")
												.replaceAll("&ndash", "-");
									}

									Item item = new Item(title, lDescription,
											image, link, date, sDescription,mediaLink,
											null);
									item.setId(i + 1);
									items.add(item);
								}

							} else {
								lDescription = null;
								sDescription = null;
							}
						}
					} catch (Exception e) {
						LOG.error("Inside Utility : getTylerList  : "
								+ e.getMessage());

					}

				}
			}

			feedsService.processDatabaseOperation(items, name, hubCitiID);

		} catch (Exception e) {
			LOG.error("Inside Utility : getTylerList :  " + e.getMessage());
			Item item = new Item(null, null, null, null, null, null,null,
					e.getMessage());
			items.add(item);
			try {
				feedsService.processDatabaseOperation(items, name, hubCitiID);
			} catch (RssFeedBatchProcessException e1) {
				LOG.error("Inside Utility : getTylerList :  " + e.getMessage());
			}
		}

		LOG.info("Exit Utility : getTylerList ");

	}

	public static String stripNonValidXMLCharacters(String in) {
		StringBuilder out = new StringBuilder();
		char current;

		if (in == null || ("".equals(in)))
			return "";
		for (int i = 0; i < in.length(); i++) {
			current = in.charAt(i);
			if ((current == 0x9) || (current == 0xA) || (current == 0xD)
					|| ((current >= 0x20) && (current <= 0xD7FF))
					|| ((current >= 0xE000) && (current <= 0xFFFD))
					|| ((current >= 0x10000) && (current <= 0x10FFFF)))
				out.append(current);
		}
		return out.toString();
	}

	public static JSONObject readJsonFromUrl(String url) {

		JSONObject json = null;
		InputStream is = null;

		try {
			is = new URL(url).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,
					Charset.forName("UTF-8")));
			String jsonText = readAll(rd);

			json = new JSONObject(jsonText);

		} catch (MalformedURLException e) {
			LOG.error("Inside Utility : readJsonFromUrl :", e.getMessage());
		} catch (IOException ex) {
			LOG.error("Inside Utility : readJsonFromUrl  : " + ex.getMessage());

		} catch (ParseException ex) {
			LOG.error("Inside Utility : readJsonFromUrl  : " + ex.getMessage());

		}

		finally {
			try {
				is.close();
			} catch (IOException e) {
				LOG.error("Inside Utility : readJsonFromUrl:", e.getMessage());
			}
		}
		return json;
	}

	private static String readAll(Reader rd) {
		StringBuilder sb = new StringBuilder();
		int cp;
		try {
			while ((cp = rd.read()) != -1) {
				sb.append((char) cp);
			}
		} catch (IOException e) {

			LOG.error("Inside READALL method :", e.getMessage());

		}
		return sb.toString();
	}

	/*
	 * public static String formatDate( Map<String, List<Item>>
	 * citiesByCountrySorted) {
	 * 
	 * String newstring = null;
	 * 
	 * for (Map.Entry<String, List<Item>> entry : citiesByCountrySorted
	 * .entrySet()) {
	 * 
	 * String key = entry.getKey();
	 * 
	 * try { Date date = new SimpleDateFormat("dd/mmm YYYY").parse(key);
	 * newstring = new SimpleDateFormat("MM-dd-YYYY").format(date); //
	 * 2011-01-18 } catch (ParseException e) {
	 * LOG.error("Exception in formatDate method" + e.getMessage()); }
	 * 
	 * } return newstring; }
	 * 
	 * public static java.sql.Timestamp getFormattedDate() throws
	 * java.text.ParseException {
	 * 
	 * final Date date = new Date(); java.sql.Timestamp sqltDate = null;
	 * 
	 * formatting the current date.
	 * 
	 * // final DateFormat formater = new SimpleDateFormat("yyyy-dd-MM"); final
	 * DateFormat formater = new SimpleDateFormat("MM-dd-yyyy"); java.util.Date
	 * parsedUtilDate; parsedUtilDate = formater.parse(formater.format(date));
	 * sqltDate = new java.sql.Timestamp(parsedUtilDate.getTime()); //
	 * LOG.info(ApplicationConstants.METHODEND + methodName); return sqltDate; }
	 * 
	 * public static Date getFormattedCurrentDate() {
	 * 
	 * final Date date = new Date(); Date parsedUtilDate = null; try { final
	 * DateFormat formater = new SimpleDateFormat("MM-dd-yyyy"); parsedUtilDate
	 * = formater.parse(formater.format(date)); } catch (ParseException
	 * exception) { LOG.error("Exception in convertDBdate method" +
	 * exception.getMessage()); // return ApplicationConstants.NOTAPPLICABLE; }
	 * 
	 * // LOG.info(ApplicationConstants.METHODEND + methodName); return
	 * parsedUtilDate; }
	 * 
	 * public static String getCurrentDate() { String currentDate = null; try {
	 * DateFormat df = new SimpleDateFormat("MMMM dd, yyyy"); Date date1 = new
	 * Date(); currentDate = df.format(date1); } catch (Exception e) {
	 * e.printStackTrace(); } return currentDate; }
	 * 
	 * public static String formattedDate(String enteredDate) throws
	 * java.text.ParseException { LOG.info("Inside the method formattedDate");
	 * String cDate = null; if (null != enteredDate && !"".equals(enteredDate))
	 * { // dd-MMM-yyyy DateFormat oldFormatter = new SimpleDateFormat(
	 * "E MMM dd HH:mm:ss Z yyyy"); DateFormat formatter = new
	 * SimpleDateFormat("MM/dd/yyyy");
	 * 
	 * Date convertedDate = (Date) oldFormatter.parse(enteredDate); cDate =
	 * formatter.format(convertedDate); } LOG.info("Exit method formattedDate");
	 * return cDate; }
	 */

	public static String getMP4RssFeedVideo(long videoId) {
		LOG.info("Inside method getMP4RssFeedVideo : " + videoId);

		String strVideo = null;
		// Fetch MP4 video by passing video id to below URL

		String url = "http://api.brightcove.com/services/library?command=find_video_by_id&video_fields=name,length,FLVURL&media_delivery=http&token=x95LXczyNI5-G9kX0cjsHM9edPFzaKFTE4PANJ7L2rQfuF-swGUxJg.."
				+ "&video_id=" + videoId;

		try {

			InputStream is = new URL(url).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,
					Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONObject json = new JSONObject(jsonText);

			strVideo = json.getString("FLVURL");
		} catch (Exception e) {

			LOG.error("Inside Utility : getMP4RssFeedVideo :  "
					+ e.getMessage());
		}
		LOG.info("Exit Utility : getMP4RssFeedVideo : " + videoId);
		return strVideo;
	}

	public static String emailBody(List<Item> newsItemList) {
		LOG.info("Inside Utility : emailBody ");

		StringBuilder emailBody = new StringBuilder();
		emailBody.append("Hi,");
		emailBody.append("</br></br>");

		if (null != newsItemList && !newsItemList.isEmpty()) {
			emailBody
					.append(PropertiesReader
							.getPropertyValue(ApplicationConstants.MAIL_CONTENT_FIRST_LINE));
			emailBody.append("</br></br>");
			emailBody.append("For the Time Stamp  &nbsp;<b>: &nbsp;&nbsp;"
					+ Utility.getCurrentDateandTime() + "</b>.\n");
			emailBody.append("</br></br>");
			emailBody
					.append("<table cellspacing='0' cellpadding='0' border='1'><tr bgcolor='#FFFF99'><th>&nbsp;Sl No.&nbsp;</th><th>&nbsp;HubCiti(s)&nbsp;</th><th>&nbsp;NewsType(s)&nbsp;</th><th>&nbsp;Reason&nbsp;</th>");
			emailBody.append("</tr>");

			for (int i = 0; i < newsItemList.size(); i++) {
				Item objItem = newsItemList.get(i);

				emailBody.append("<tr>");

				emailBody.append("<td align=\"center\">");
				emailBody.append(objItem.getId());
				emailBody.append("</td>");

				emailBody.append("<td align=\"center\">");
				emailBody.append(objItem.getHubCitiName());
				emailBody.append("</td>");

				emailBody.append("<td align=\"center\">");
				emailBody.append(objItem.getFeedType());
				emailBody.append("</td>");

				emailBody.append("<td align=\"left\">");
				emailBody.append(objItem.getMessage());
				emailBody.append("</td>");

				emailBody.append("</tr>");
			}
			emailBody.append("</table>");
		} /*
		 * else {
		 * 
		 * emailBody.append("News  are not available for the date  " +
		 * Utility.getCurrentDateandTime() + ".\n");
		 * emailBody.append("</br></br>");
		 * 
		 * }
		 */

		emailBody.append("</br></br>");
		emailBody.append("Regards,</br>");
		// emailBody.append("ScanSee Team");
		emailBody.append("HubCiti Team");

		LOG.info("Inside Utility : emailBody : " + emailBody.toString());
		return emailBody.toString();
	}

	/**
	 * @return Current Date & Time (Jul 24, 2015 4:00 AM.)
	 */
	public static String getCurrentDateandTime() {
		// Make a new Date object. It will be initialized to the current time.
		Date now = new Date();
		String currentDate = null;
		try {

			currentDate = DateFormat.getDateTimeInstance(DateFormat.MEDIUM,
					DateFormat.SHORT).format(now);

		} catch (Exception e) {
			LOG.error("Inside Utility :  getCurrentDateandTime : "
					+ e.getMessage());
		}
		return currentDate;
	}

	public static String getDate(String s) {
		String currentDate = null;
		try {
			DateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
			Date date1 = df.parse(s);
			currentDate = df.format(date1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return currentDate;

	}

	public static void getAustinList(String name, String hubCitiID) {
		LOG.info("Inside Utility : getAustinList ");

		List<Item> items = new ArrayList<Item>();
		final ApplicationContext context = new ClassPathXmlApplicationContext(
				"feeds-service.xml");
		final FeedsServiceImpl feedsService = (FeedsServiceImpl) context
				.getBean("feedsService");

		URL url = null;

		try {
			/*
			 * switch (name) { case "citywide": url = new
			 * URL(CommonConstants.CITYWIDE_NEWS_URL); break;
			 * 
			 * case "imagineaustin": url = new
			 * URL(CommonConstants.IMAGINEAUSTIN_NEWS_URL); break;
			 * 
			 * case "abia": url = new URL(CommonConstants.ABIA_NEWS_URL); break;
			 * 
			 * case "animalservices": url = new
			 * URL(CommonConstants.ANIMALSERVICES_NEWS_URL); break;
			 * 
			 * case "medicalservices": url = new
			 * URL(CommonConstants.MEDICALSERVICES_NEWS_URL); break;
			 * 
			 * case "fire": url = new URL(CommonConstants.FIRE_NEWS_URL); break;
			 * 
			 * case "humanservices": url = new
			 * URL(CommonConstants.HUMANSERVICES_NEWS_URL); break;
			 * 
			 * case "sustainability": url = new
			 * URL(CommonConstants.SUSTAINABILITY_NEWS_URL); break;
			 * 
			 * case "parks": url = new URL(CommonConstants.PARKS_NEWS_URL);
			 * break;
			 * 
			 * case "police": url = new URL(CommonConstants.POLICE_NEWS_URL);
			 * break;
			 * 
			 * default: url = null; }
			 */

			if (name.equalsIgnoreCase("citywide")) {
				url = new URL(CommonConstants.CITYWIDE_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("imagineaustin")) {
				url = new URL(CommonConstants.IMAGINEAUSTIN_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("abia")) {
				url = new URL(CommonConstants.ABIA_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("animalservices")) {
				url = new URL(CommonConstants.ANIMALSERVICES_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("medicalservices")) {
				url = new URL(CommonConstants.MEDICALSERVICES_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("fire")) {
				url = new URL(CommonConstants.FIRE_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("humanservices")) {
				url = new URL(CommonConstants.HUMANSERVICES_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("sustainability")) {
				url = new URL(CommonConstants.SUSTAINABILITY_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("parks")) {
				url = new URL(CommonConstants.PARKS_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("police")) {
				url = new URL(CommonConstants.POLICE_NEWS_URL);
			}

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setCoalescing(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			if (url.openStream().available() > 0) {
				Document doc = db.parse(new InputSource(url.openStream()));
				doc.getDocumentElement().normalize();
				NodeList nodeList = doc.getElementsByTagName("item");

				for (int i = 0; i < nodeList.getLength(); i++) {

					String title = null;
					String lDescription = null;
					String image = null;
					String link = null;
					String date = null;
					String sDescription = null;

					Node node = nodeList.item(i);

					Element fstElmnt = (Element) node;

					try {

						if (fstElmnt.getElementsByTagName("media:content") != null) {

							NodeList media = fstElmnt
									.getElementsByTagName("media:content");
							if (media.item(0) != null
									&& media.item(0).getAttributes() != null
									&& media.item(0).getAttributes()
											.getNamedItem("url") != null) {
								String mediaurl = media.item(0).getAttributes()
										.getNamedItem("url").getNodeValue();

								image = mediaurl;
							}

						} else {
							image = null;
						}

					} catch (Exception e) {
						LOG.error("Inside Utility : getAustinList :  "
								+ e.getMessage());

					}

					try {

						if (fstElmnt.getElementsByTagName("link") != null) {

							NodeList linkList = fstElmnt
									.getElementsByTagName("link");
							Element linkElement = (Element) linkList.item(0);
							linkList = linkElement.getChildNodes();

							link = ((Node) linkList.item(0)).getNodeValue();

						} else {
							link = null;
						}

					} catch (Exception e) {
						LOG.error("Inside Utility : getAustinList :  "
								+ e.getMessage());

					}
					try {
						if (fstElmnt.getElementsByTagName("title") != null) {

							NodeList titleList = fstElmnt
									.getElementsByTagName("title");
							Element titleElement = (Element) titleList.item(0);
							titleList = titleElement.getChildNodes();

							title = ((Node) titleList.item(0)).getNodeValue()

							.replaceAll("([^\\w\\s\'.,-:&\"<>])", "")
									.replaceAll("&rsquo", "'")
									.replaceAll("&lsquo", "'")
									.replaceAll("&ldquo", "\"")
									.replaceAll("&rdquo", "\"")
									.replaceAll("&mdash", "--")
									.replaceAll("&ndash", "-")
									.replaceAll("&8211", "-")
									.replaceAll("&8216", "'") // left single
																// quotation
																// mark
									.replaceAll("&8217", "'") // right single
																// quotation
																// mark
									.replaceAll("&8230", "...") // horizontal
																// ellipsis
									.replaceAll("&38", "&") // for ampersand.
									.replaceAll("&8220", "\"")// left double
																// quotation
																// mark
									.replaceAll("&8221", "\"") // right double
																// quotation
																// mark
 .replaceAll("&hellip", "...")
									.replaceAll(":", "");

							// title = StringEscapeUtils.unescapeHtml(title);
						} else {
							title = null;
						}

					} catch (Exception e) {
						LOG.error("Inside Utility : getAustinList :  "
								+ e.getMessage());

					}

					try {
						if (fstElmnt.getElementsByTagName("pubDate") != null) {

							NodeList dateList = fstElmnt
									.getElementsByTagName("pubDate");
							Element dateElement = (Element) dateList.item(0);
							dateList = dateElement.getChildNodes();
							String datee = ((Node) dateList.item(0))
									.getNodeValue();
							// date=getDate(datee);
							String dates[] = datee.split(" ");
							date = dates[2] + " " + dates[1] + " " + " , "
									+ dates[3];

						} else {
							date = null;
						}

					} catch (Exception e) {
						LOG.error("Inside Utility : getAustinList :  "
								+ e.getMessage());

					}

					NodeList websiteList = fstElmnt
							.getElementsByTagName("description");

					Element websiteElement = (Element) websiteList.item(0);
					websiteList = websiteElement.getChildNodes();
					if (null != websiteList.item(0)) {

						if (websiteList.item(0).toString() != null) {
							if (((Node) websiteList.item(0)).getNodeValue()
									.length() > 75) {
								sDescription = ((Node) websiteList.item(0))
										.getNodeValue()
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"").replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replaceAll("&mdash", "--")
										.replaceAll("&ndash", "-")
										.replaceAll("&39", "")
										.replaceAll("&8211", ".")
										 .replaceAll("&hellip", "...")
										.replaceAll("&8216", "'") // left single
																	// quotation
																	// mark
										.replaceAll("&8217", "'") // right
																	// single
																	// quotation
																	// mark
										.replaceAll("&8230", "...") // horizontal
																	// ellipsis
										.replaceAll("&38", "&") // for
																// ampersand.
										.replaceAll("&8220", "\"")// left double
																	// quotation
																	// mark
										.replaceAll("&8221", "\"") // right
																	// double
																	// quotation
																	// mark
										.substring(0, 75)
										+ "...";
								// sDescription = StringEscapeUtils
								// .unescapeHtml(sDescription);
							} else {

								sDescription = ((Node) websiteList.item(0))
										.getNodeValue()
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"").replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replaceAll("&mdash", "--")
										.replaceAll("&ndash", "-")
										.replaceAll("&8211", ".")
										.replaceAll("&8216", "'")
										// left single quotation mark
										.replaceAll("&8217", "'")
										// right single quotation mark
										.replaceAll("&8230", "...")
										// horizontal ellipsis
										.replaceAll("&39", "")
										 .replaceAll("&hellip", "...")
										.replaceAll("&38", "&") // for
																// ampersand.
										.replaceAll("&8220", "\"")// left double
																	// quotation
																	// mark
										.replaceAll("&8221", "\"") // right
																	// double
																	// quotation
																	// mark
										+ "...";
								// sDescription = StringEscapeUtils
								// .unescapeHtml(sDescription);
							}
							lDescription = websiteList.item(0).toString();

							if (null != lDescription
									&& !"".equals(lDescription)) {
								lDescription = lDescription.replace("[#text:",
										"");
								lDescription = lDescription
										.substring(0, lDescription.length() - 1)
										.trim()
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"")
										.replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replace("&39", "'")
										.replaceAll("&8211", ".")
										.replaceAll("&8216", "'")
										// left single quotation mark
										.replaceAll("&8217", "'")
										 .replaceAll("&hellip", "...")
										// right single quotation mark
										.replaceAll("&8230", "...")
										// horizontal ellipsis
										.replaceAll("&mdash", "--")
										.replaceAll("&ndash", "-")
										.replaceAll("&38", "&") // for
																// ampersand.
										.replaceAll("&8220", "\"")// left double
																	// quotation
																	// mark
										.replaceAll("&8221", "\""); // right
																	// double
																	// quotation
																	// mark
								// lDescription = StringEscapeUtils
								// .unescapeHtml(lDescription);
							}

							Item item = new Item(title, lDescription, image,
									link, date, sDescription, null,null);

							item.setId(i + 1);
							items.add(item);
						}
					}
				}

				feedsService.processDatabaseOperation(items, name, hubCitiID);

			}

		} catch (Exception e) {
			LOG.error("Inside Utility : getRockwallList :  " + e.getMessage());
			Item item = new Item(null, null, null, null, null, null,null,
					e.getMessage());
			items.add(item);

			try {
				feedsService.processDatabaseOperation(items, name, hubCitiID);
			} catch (RssFeedBatchProcessException e1) {
				LOG.error("Inside Utility : getAustinList :  " + e.getMessage());
			}
		}

		LOG.info("Exit Utility : getAustinList ");

	}

	public static void getKilleenList(String name, String hubCitiID) {

		LOG.info("Inside Utility : getKilleenlist ");

		List<Item> items = new ArrayList<Item>();
		final ApplicationContext context = new ClassPathXmlApplicationContext(
				"feeds-service.xml");
		final FeedsServiceImpl feedsService = (FeedsServiceImpl) context
				.getBean("feedsService");
		URL url = null;

		try {

			/*
			 * switch (name) { case "topstories": url = new
			 * URL(CommonConstants.TOPSTORIES_NEWS_URL); break;
			 * 
			 * case "Weather": url = new
			 * URL(CommonConstants.WEATHERKILEEN_NEWS_URL); break;
			 * 
			 * case "Business": url = new
			 * URL(CommonConstants.BUSINESSKILLEEN_NEWS_URL); break;
			 * 
			 * case "Traffic": url = new URL(CommonConstants.TRAFFIC_NEWS_URL);
			 * break;
			 * 
			 * case "Crime": url = new URL(CommonConstants.CRIME_NEWS_URL);
			 * break;
			 * 
			 * case "Opinion": url = new
			 * URL(CommonConstants.OPINIONKILLEEN_NEWS_URL); break;
			 * 
			 * case "obits": url = new URL(CommonConstants.OBITS_NEWS_URL);
			 * break;
			 * 
			 * case "worldnews": url = new
			 * URL(CommonConstants.WORLDNEWS_NEWS_URL); break;
			 * 
			 * default: url = null; }
			 */

			if (name.equalsIgnoreCase("topstories")) {
				url = new URL(CommonConstants.TOPSTORIES_NEWS_URL);
			}

			if (name.equalsIgnoreCase("Weather")) {
				url = new URL(CommonConstants.WEATHERKILEEN_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("Business")) {
				url = new URL(CommonConstants.BUSINESSKILLEEN_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("Traffic")) {
				url = new URL(CommonConstants.TRAFFIC_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("Crime")) {
				url = new URL(CommonConstants.CRIME_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("Opinion")) {
				url = new URL(CommonConstants.OPINIONKILLEEN_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("obits")) {
				url = new URL(CommonConstants.OBITS_NEWS_URL);
			}

			else if (name.equalsIgnoreCase("worldnews")) {
				url = new URL(CommonConstants.WORLDNEWS_NEWS_URL);
			}

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setCoalescing(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			if (url.openStream().available() > 0) {
				Document doc = db.parse(new InputSource(url.openStream()));
				doc.getDocumentElement().normalize();
				NodeList nodeList = doc.getElementsByTagName("item");

				for (int i = 0; i < nodeList.getLength(); i++) {

					String title = null;
					String lDescription = null;
					String image = null;
					String link = null;
					String date = null;
					String sDescription = null;

					Node node = nodeList.item(i);

					Element fstElmnt = (Element) node;

					try {

						if (fstElmnt.getElementsByTagName("enclosure") != null) {

							NodeList media = fstElmnt
									.getElementsByTagName("enclosure");
							if (media.item(0) != null
									&& media.item(0).getAttributes() != null
									&& media.item(0).getAttributes()
											.getNamedItem("url") != null) {
								String mediaurl = media.item(0).getAttributes()
										.getNamedItem("url").getNodeValue();

								image = mediaurl;
							}

						} else {
							image = null;
						}

					} catch (Exception e) {
						LOG.error("Inside Utility : getKilleenList Exception:  "
								+ e.getMessage());

					}

					try {

						if (fstElmnt.getElementsByTagName("link") != null) {

							NodeList linkList = fstElmnt
									.getElementsByTagName("link");
							Element linkElement = (Element) linkList.item(0);
							linkList = linkElement.getChildNodes();

							link = ((Node) linkList.item(0)).getNodeValue();

						} else {
							link = null;
						}

					} catch (Exception e) {
						LOG.error("Inside Utility : getKillenList :  "
								+ e.getMessage());

					}
					try {
						if (fstElmnt.getElementsByTagName("title") != null) {

							NodeList titleList = fstElmnt
									.getElementsByTagName("title");
							Element titleElement = (Element) titleList.item(0);
							titleList = titleElement.getChildNodes();

							title = ((Node) titleList.item(0)).getNodeValue()

							.replaceAll("([^\\w\\s\'.,-:&\"<>])", "")
									.replaceAll("&rsquo", "'")
									.replaceAll("&lsquo", "'")
									.replaceAll("&ldquo", "\"")
									.replaceAll("&rdquo", "\"")
									.replaceAll("&mdash", "--")
									 .replaceAll("&hellip", "...")
									.replaceAll("&ndash", "-")
									.replaceAll("&8211", "-")
									.replaceAll("&8216", "'") // left single
																// quotation
																// mark
									.replaceAll("&8217", "'") // right single
																// quotation
																// mark
									.replaceAll("&8230", "...") // horizontal
																// ellipsis
									.replaceAll("&38", "&") // for ampersand.
									.replaceAll("&8220", "\"")// left double
																// quotation
																// mark
									.replaceAll("&8221", "\"") // right double
																// quotation
																// mark

									.replaceAll(":", "");

							// title = StringEscapeUtils.unescapeHtml(title);
						} else {
							title = null;
						}

					} catch (Exception e) {
						LOG.error("Inside Utility : getKilleenList :  "
								+ e.getMessage());

					}

					try {
						if (fstElmnt.getElementsByTagName("pubDate") != null) {

							NodeList dateList = fstElmnt
									.getElementsByTagName("pubDate");
							Element dateElement = (Element) dateList.item(0);
							dateList = dateElement.getChildNodes();
							String datee = ((Node) dateList.item(0))
									.getNodeValue();
							// date=getDate(datee);
							String dates[] = datee.split(" ");
							date = dates[2] + " " + dates[1] + " " + " , "
									+ dates[3];

						} else {
							date = null;
						}

					} catch (Exception e) {
						LOG.error("Inside Utility : getKilleenList :  "
								+ e.getMessage());

					}

					NodeList websiteList = fstElmnt
							.getElementsByTagName("description");

					Element websiteElement = (Element) websiteList.item(0);
					websiteList = websiteElement.getChildNodes();
					if (null != websiteList.item(0)) {

						if (websiteList.item(0).toString() != null) {
							if (((Node) websiteList.item(0)).getNodeValue()
									.length() > 75) {
								sDescription = ((Node) websiteList.item(0))
										.getNodeValue()
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"").replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replaceAll("&mdash", "--")
										 .replaceAll("&hellip", "...")
										.replaceAll("&ndash", "-")
										.replaceAll("&39", "")
										.replaceAll("&8211", ".")
										.replaceAll("&8216", "'") // left single
																	// quotation
																	// mark
										.replaceAll("&8217", "'") // right
																	// single
																	// quotation
																	// mark
										.replaceAll("&8230", "...") // horizontal
																	// ellipsis
										.replaceAll("&38", "&") // for
																// ampersand.
										.replaceAll("&8220", "\"")// left double
																	// quotation
																	// mark
										.replaceAll("&8221", "\"") // right
																	// double
																	// quotation
																	// mark
										.substring(0, 75)
										+ "...";
								// sDescription = StringEscapeUtils
								// .unescapeHtml(sDescription);
							} else {

								sDescription = ((Node) websiteList.item(0))
										.getNodeValue()
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"").replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replaceAll("&mdash", "--")
										.replaceAll("&ndash", "-")
										 .replaceAll("&hellip", "...")
										.replaceAll("&8211", ".")
										.replaceAll("&8216", "'")
										// left single quotation mark
										.replaceAll("&8217", "'")
										// right single quotation mark
										.replaceAll("&8230", "...")
										// horizontal ellipsis
										.replaceAll("&39", "")
										.replaceAll("&38", "&") // for
																// ampersand.
										.replaceAll("&8220", "\"")// left double
																	// quotation
																	// mark
										.replaceAll("&8221", "\"") // right
																	// double
																	// quotation
																	// mark
										+ "...";
								// sDescription = StringEscapeUtils
								// .unescapeHtml(sDescription);
							}
							lDescription = websiteList.item(0).toString();

							if (null != lDescription
									&& !"".equals(lDescription)) {
								lDescription = lDescription.replace("[#text:",
										"");
								lDescription = lDescription
										.substring(0, lDescription.length() - 1)
										.trim()
										.replaceAll("([^\\w\\s\'.,-:&\"<>])",
												"")
										.replaceAll("&rsquo", "'")
										.replaceAll("&lsquo", "'")
										.replaceAll("&ldquo", "\"")
										.replaceAll("&rdquo", "\"")
										.replace("&39", "'")
										.replaceAll("&8211", ".")
										 .replaceAll("&hellip", "...")
										.replaceAll("&8216", "'")
										// left single quotation mark
										.replaceAll("&8217", "'")
										// right single quotation mark
										.replaceAll("&8230", "...")
										// horizontal ellipsis
										.replaceAll("&mdash", "--")
										.replaceAll("&ndash", "-")
										.replaceAll("&38", "&") // for
																// ampersand.
										.replaceAll("&8220", "\"")// left double
																	// quotation
																	// mark
										.replaceAll("&8221", "\""); // right
																	// double
																	// quotation
																	// mark
								// lDescription = StringEscapeUtils
								// .unescapeHtml(lDescription);
							}

							Item item = new Item(title, lDescription, image,
									link, date, sDescription,null, null);

							item.setId(i + 1);
							items.add(item);
						}
					}
				}

				feedsService.processDatabaseOperation(items, name, hubCitiID);

			}

		} catch (Exception e) {
			LOG.error("Inside Utility : getKillenList :  " + e.getMessage());
			Item item = new Item(null, null, null, null, null, null,null,
					e.getMessage());
			items.add(item);

			try {
				feedsService.processDatabaseOperation(items, name, hubCitiID);
			} catch (RssFeedBatchProcessException e1) {
				LOG.error("Inside Utility : getKillenList :  " + e.getMessage());
			}
		}

		LOG.info("Exit Utility : getKillenList ");

	}

}
