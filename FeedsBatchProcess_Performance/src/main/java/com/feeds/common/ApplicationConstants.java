package com.feeds.common;

public class ApplicationConstants {

	/**
	 * MethodStart declared as String for logger messages.
	 */
	public static final String METHODSTART = "In side method >>> ";
	/**
	 * MethodEnd declared as String for logger messages.
	 */
	public static final String METHODEND = " Exiting method >>> ";
	/**
	 * ExceptionOccurred declared as String for logger messages.
	 */
	public static final String EXCEPTIONOCCURRED = "Exception Occurred in  >>> ";

	public static final String SUCCESSTEXT = "Success";

	public static final String FAILURETEXT = "Failure";
	public static final String NOTAPPLICABLE = "N/A";

	public static final String SCHEMANAME = "dbo";

	/**
	 * This constant for retrieving Database error message.
	 */
	public static final String ERRORMESSAGE = "ErrorMessage";
	/**
	 * This constant for retrieving Database error code.
	 */
	public static final String ERRORNUMBER = "ErrorNumber";
	/**
	 * SuccessCode declared as String for getting success response code.
	 */
	public static final String FEEDS = "Feeds";

	public static final String SMTPHOST = "SMTP_Host";

	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String SMTPPORT = "SMTP_Port";

	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String EMAIL = "BatchProcessEmailNotification";

	public static final String EMAILCONFIG = "Email";

	public static final String SMTP_PORT = "smtp_port";
	public static final String SMTP_SERVER = "smtp_server";
	public static final String EMAIL_SUBJECT = "subject";
	public static final String SENDER_TO_LIST = "sender_toList";
	public static final String FROM_MAIL = "from_emailId";
	public static final String BATCH_PROGRAM = "batch_program";
	public static final String HUBCITI_ID = "hubciti_id";
	public static final String MAIL_CONTENT_FIRST_LINE = "mail_content_first_line";
	public static final String EMPTYLIST = "EmptyList";
	public static final String TYLER_HUBCITI_ID = "tyler_hubcitiId";
	public static final String ROCKWALL_HUBCITI_ID = "rockwall_hubCitiId";

}
