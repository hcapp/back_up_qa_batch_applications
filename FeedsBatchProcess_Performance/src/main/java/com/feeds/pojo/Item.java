package com.feeds.pojo;

import java.util.List;

public class Item {
	private int id;
	private String title;
	private String image;
	private String link;
	private String feedType;
	

	public String getVideoLink() {
		return VideoLink;
	}

	public void setVideoLink(String videoLink) {
		VideoLink = videoLink;
	}

	private String HcHubCitiID;
	private String HubCitiName;
	private String VideoLink;

	private String classification;
	private String section;

	private String adcopy;

	public String getClassification() {
		return classification;
	}

	public Item(String title, String webUri) {
		this.title = title;
		this.link = webUri;

	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getAdcopy() {
		return adcopy;
	}

	public void setAdcopy(String adcopy) {
		this.adcopy = adcopy;
	}

	public String getHcHubCitiID() {
		return HcHubCitiID;
	}

	public void setHcHubCitiID(String hcHubCitiID) {
		HcHubCitiID = hcHubCitiID;
	}

	private String date;
	private String shortDesc;
	private String apiPatnerName;
	private String reason;

	private Integer rowCount;
	private Integer status;

	public String getApiPatnerName() {
		return apiPatnerName;
	}

	public void setApiPatnerName(String apiPatnerName) {
		this.apiPatnerName = apiPatnerName;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getRowCount() {
		return rowCount;
	}

	public void setRowCount(Integer rowCount) {
		this.rowCount = rowCount;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getFeedType() {
		return feedType;
	}

	public void setFeedType(String feedType) {
		this.feedType = feedType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	private String description;

	private List<Item> items;

	private String message;

	public Item() {

	}

	public Item(String tit, String description, String image, String link,
			String date, String shortDesc,String mediaLink, String message) {
		this.title = tit;
		this.description = description;
		this.image = image;
		this.link = link;
		this.date = date;
		this.shortDesc = shortDesc;
		this.VideoLink= mediaLink;
		this.message = message;
	}

	public Item(String tit, String description, String image, String link,
			String date, String shortDesc, String message, String adcopy,
			String section, String classification) {
		this.title = tit;
		this.description = description;
		this.image = image;
		this.link = link;
		this.date = date;
		this.shortDesc = shortDesc;
		this.message = message;
		this.adcopy = adcopy;
		this.section = section;
		this.classification = classification;
	}

	public Item(String tit, String description, String image, String link,
			String date, String shortDesc,String mediaLink) {
		this.title = tit;
		this.description = description;
		this.image = image;
		this.link = link;
		this.date = date;
		this.shortDesc = shortDesc;
		this.VideoLink=mediaLink;
	}

	public Item(String tit, String description, String image, String link) {
		this.title = tit;
		this.description = description;
		this.image = image;
		this.link = link;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		if (title != null) {
			this.title = title;
		} else {
			this.title = null;
		}
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getHubCitiName() {
		return HubCitiName;
	}

	public void setHubCitiName(String hubCitiName) {
		this.HubCitiName = hubCitiName;
	}

}
