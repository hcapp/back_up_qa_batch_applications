package com.feeds.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.feeds.exception.RssFeedBatchProcessException;
import com.feeds.pojo.Item;

public interface FeedsDao {

	String insertData(List<Item> items, String newsType, String hubcitiId)
			throws RssFeedBatchProcessException;

	void deleteData() throws RssFeedBatchProcessException;

	String NewsFeedPorting() throws RssFeedBatchProcessException;

	/**
	 * The DAO method for sending list of items with empty information email
	 * details to database.
	 * 
	 * @return List of items with empty information.
	 * @throws RssFeedBatchProcessException
	 *             throws if exception occurs.
	 */
	List<Item> emptyNewsListByEmail() throws RssFeedBatchProcessException;

	public ArrayList<Item> getHubCitiId() throws RssFeedBatchProcessException;

	public String deleteFeedData() throws RssFeedBatchProcessException;
	
	public void batchInsertRecordsIntoTable(List<Item> itemList, String newsType, String hubcitiId) throws SQLException, RssFeedBatchProcessException;
}
